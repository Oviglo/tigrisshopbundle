config.shop: .icon-shopping-bag E commerce
shop:
  menu:
    shop: .icon-shopping-bag Shop
    eshop: .icon-shopping-bag E commerce
    categories: .icon-tags Catégories
    products: .icon-box-full Produits
    taxes: .icon-sack-dollar Taxes
    orders: .icon-file-invoice Commandes
    user_orders: .icon-file-invoice Mes commandes
    transports: .icon-shipping-fast Transports
    discounts: .icon-badge Réductions

  config:
    click_collect: .icon-store Click&Collect
    click_infos: Informations pour le client
    click_infos_help: Informations envoyées par mail au client tel que les horaires de disponibilités
    click_address: Adresse du lieu de retrait
    pay_at_shop: Autoriser les acheteurs à payer en boutique
    pay_at_shop_help: La commande sera validée mais considérée comme non payée

    gift_options: .icon-gift Emballage cadeau
    gift_options_enable: Activer l'option "Emballage cadeau"
    gift_options_price: Prix TTC
    gift_options_tax: Tax
  title: E-commerce

  action:
    add_to_basket: .icon-cart-plus Ajouter au panier

  category:
    category: Catégorie de produit
    title: Catégories de produit
    parent: Catégorie parente
    parent_help: Permet de créer un hierarchie
    image: Image
    public: Publiée
    description: Description
    add:
      title: Ajouter une catégorie de produit
      success: La catégorie a bien été ajoutée
    edit:
      title: Modifier une catégorie de produit
      success: La catégorie a bien été modifiée
    remove:
      title: Supprimer une catégorie de produit
      success: La catégorie a bien été supprimée
      message: Confirmer la suppression de la catégorie
    sort:
      title: Modifier l'ordre
      success: L'ordre des catégorie a bien été modifié
      
  tax:
    category: Taxe
    title: Taxes
    value: Valeur (%)
    add:
      title: Ajouter une taxe
      success: La taxe a bien été ajoutée
    edit:
      title: Modifier une taxe
      success: La taxe a bien été modifiée
    delete:
      title: Supprimer une taxe
      success: La taxe a bien été supprimée
    remove:
      title: Supprimer une taxe
      message: Confirmer la suppression de la taxe
      success: La taxe a bien été supprimée

  discount:
    title: .icon-badge Réductions
    new_price: Prix
    start_date: Début
    end_date: Fin
    valid_dates: Validité
    discount: Réduction
    old_price: Ancien prix
    use_count: Nb. utilisations
    description: Description
    dates: Dates de validité
    add:
      title: Ajouter une réduction
      success: La réduction a bien été ajoutée
    edit:
      title: Modifier une réduction
      success: La réduction a bien été modifiée
    remove:
      title: Supprimer une réduction
      message: Confirmer la suppression de la réduction
      success: La réduction a bien été supprimée
    type:
      label: Type de réduction
      percent: Poucentage
      value: Valeur
      new_price: Nouveau prix
    value: Réduction
    name: Libéllé
    products: Produits
    enabled: Active
    products_help: La réduction sera appliquée seulement sur les produits sélectionnés
    user_groups: Groupes d'utilisateur
    user_groups_help: Seul les utilisateurs des ces groupes peuvent bénéficier de cette réduction
    users: Utilisateurs
    users_help: Seul les utilisateurs de cette liste peuvent bénéficier de cette réduction
    item_type:
      label: Réduction sur
      products: Produits
      categories: Catégories
      all_products: Tous les produits
      order: La commande
    min_price: Prix minimum d'achat 
    categories: Catégories
    categories_help: La réduction sera appliquée seulement sur les catégories sélectionnées
    code: Code promo
    code_help: Si renseigné, l'utilisateur doit entrer ce code lors de l'achat pour activer la réduction
    use_count_limit: Nombre d'utilisations
    use_count_limit_help: Nombre d'activations possible, mettre 0 pour une utilisation illimitée
    unvalid_code: Le code est invalide
    valid_code: Code validé
    code_message: Entrez ce code promo lors de la validation de votre commande
    print_code: Imprimer le code
    fieldset:
      global_settings: .icon-pencil Informations
      actions: .icon-percent Réduction
      rules: .icon-scale-balanced Règles d'application
      dates: .icon-calendar Dates
    validity:
      expired: Expiré
      valid: Valide
      futur: Prochainement
  
  product:
    products: Produits
    title: Produits de la boutique
    public: Publier
    reference: Référence
    top: Top liste
    top_help: Si coché le produit s'affichera parmi les premiers dans la liste
    sale_count: Nb. ventes
    add:
      title: Ajouter un produit
      success: Le produit a bien été ajouté
    edit:
      title: Modifier un produit
      success: Le produit a bien été modifié
    delete:
      title: Supprimer un produit
      message: Confirmer la suppression du produit
      success: Le produit a bien été supprimé
    delete_group:
      title: Supprimer des produits
      message: Confirmer la suppression de ces produits
      success: Les produits ont bien été supprimés
    copy:
      title: Dupliquer
      message: Confirmer la duplication du produit "%entity%"
      success: Le produit a bien été dupliqué
    change_category:
      title: Modifier la categories de produits
      success: Les catégories des produits ont bien été modifiées
    price: Prix
    original_price: Prix de base
    original_price_help: Ce prix sera affiché barré
    priceIT: Prix TTC
    priceET: Prix HT
    tax: Taxe
    stock: Stock
    quantity: Quantité
    max_quantity: Quantité maximum par commande
    enable_max_quantity: Activer la quantité maximum par commande
    category: Catégorie
    categories: Catégories
    description: Description
    type: Type de produit
    type_standard: Produit standard
    type_virtual: Produit virtuel
    width: Longueur
    height: Largeur
    depth: Profondeur
    weight: Poids
    delivery_added_price: Frais de port supplémentaires
    disable_delivery_price_on_free_shipping: Pas de frais supplémentaires si la livraison est gratuite
    sizes: .icon-box Dimensions & poids
    min_price: Prix mini
    max_price: Prix max
    unavailable: Indisponible
    use_transports: Transports à exclure
    use_transports_help: Les transports sélectionnés ne seront pas proposés pour ce produit
    pre_order_date: Date de début de pré-commande
    available_date: Date de disponibilité
    available_status:
      label: Disponibilité
      available: Disponible
      unavailable: Non disponible
      available_at: Disponible à partir du...
      pre_order: En précommande
    status:
      label: Disponibilité
      all: Tous les statuts
      available: Disponible
      unavailable: Non disponible
      available_at: Disponible à partir du...
      pre_order: En précommande
    fieldset:
      global_settings: .icon-pencil Informations
      categories: .icon-tags Catégories
      price: .icon-money-bill-wave Prix
      images: .icon-images Galerie de photos
      stock: .icon-boxes Stock
      sizes: .icon-box Dimensions & poids
      delivery: .icon-truck Livraison
      options: .icon-tasks-alt Options
      slots: .icon-tachometer-slow Tranches de prix
      distance: .icon-map-marked-alt Distance
      availability: .icon-box-check Disponibilité
      files: .icon-file-download Fichiers
    change_quantity:
      title: Modifier la quantité
      success: La quantité a bien été modifiée
    no_product: Aucun produit trouvé
    pre_order: Précommande
    pre_order_date_message: "Produit en précommande, disponible le %date%"
    sort_by:
      label: Trier par
      name: Nom A-Z
      last: Derniers ajoutés
      price_asc: Prix croissant
      price_desc: Prix décroissant
      sales: Meilleurs ventes
    category_edit:
      title: Modifier la catégorie
    selected: Produits seléctionnés
    change_part:
      success: Le produit a bien été mis à jour
    created_at: Ajouté le
  
  product_file:
    help: Permet de fournir des fichiers à télécharger tel qu'une notice d'utilisation
    file: Fichier
    files: Fichiers
    description: Description

  product_image:
    position: Position
    position_help: La position de l'image dans la galerie

  address:
    country: Pays
    postal: Code postal
    city: Ville
    choose: Changer d'adresse
    line1: Numéro et voie
    firstname: Prénom
    name: Nom
    civility: Civilité
    mister: M
    miss: Mm
    search: Rechercher une adresse...
    phone: Numéro de téléphone
    add:
      title: Ajouter une adresse
    delete:
      title: Supprimer une adresse
      success: L'adresse a bien été supprimée
      message: Confirmer la suppression de cette adresse
  
  basket:
    product_added: Le produit a bien été ajouté au panier
    title: Mon panier
    empty: Il n'y a aucun produit dans votre panier
    quantity: Quantité
    refresh_quantity: .icon-sync Actualitser
    total: Total
    buy: Passer la commande
    delete_product: Supprimer
    change_quantity:
      success: La quantité a bien été modifiée
    error:
      cannot_buy: Vous ne pouvez pas acheter ce produit
      cannot_add_to_basket: Votre commande dans votre panier a atteint la quantité maximale pour ce produit
    delete:
      success: Le produit a bien été supprimé

  order:
    title: Commandes
    orders: Commandes
    order: Commande
    user_title: Mes commandes
    price: Prix
    date: Effectuée le
    total: Total
    quantity: Quantité
    name: Acheteur
    payment_status: État paiement 
    shipping_status: État livraison 
    number: Commande numéro %n%
    show: .icon-eye Détails de la commande
    invoices: Factures
    invoice: Facture %n%
    transport: Transport
    shipp:
      title: Marquer comme envoyée
      success: La commande est maintenant marquée comme envoyée
    available:
      title: Marquer comme disponible pour retrait
      success: La commande est maintenant marquée comme disponible pour retrait
      message: Confirmé la disponibilité de la commande.
    shipping_service: Service de transport
    tracking_number: Numéro de suivis
    status:
      label: Etat
      all: Tous
      new: En attente
      finished: Terminée
      canceled: Annulée
      completed: Terminée
      paid: Payée
      awaiting: En attente
      ready: Prête
      shipped: Envoyée
      available: Disponible
      shipping: En préparation
      unpaid: En attente
      open: En cours
      confirmed: Confirmé
      statuses: Status de la commande
      order: Commande
      payment: Paiement
      delivery: Livraison
      history: Voir l'historique des status

      open_help: Le client n'a pas confirmé sa commande
      confirmed_help: Commande confirmée par le client 
      canceled_help: Commande annulée
    paid:
      title: Modifier le statut du paiement
      check: Commande payée
      method: Mode de paiement
      credit_card: Carte de crédit
      bank_check: Chèque
      cash: Argent liquide
      create_invoice: Créer une facture
    cancel:
      title: Annuler
      message: Confirmer l'annulation de cette commande
      success: La commande a bien été annulée
    valid:
      title: Valider
      message: Confirmer la validation de cette commande
      success: La commande a bien été validée


  payment:
    payment_redirection: Vous allez être redirigé vers le service de paiement

  purchase:
    address: Adresse
    shipping_address: Adresse de livraison
    empty_basket: Votre panier est vide
    summary: Récapitulatif de votre commande
    empty_address: Aucune adresse de livraison
    finished:
      title: .icon-check Commande validée
      success: Votre commande a bien été validée, merci. 
    delivery:
      title: Livraison
      mondial_relay:
        title: Livraison en point relais
    error:
      title: Une erreur est survenue
      message: Une erreur est survenue lors du paiement, veuillez nous contacter si le problème persiste.
    cancel:
      title: Votre commande a été annulée
      message: Le paiement a été annulé, veuillez nous contacter si vous rencontrez un problème.

  button:
    paypal: .brand-paypal Payer avec PayPal
    payplug: .icon-credit-card Payer avec PayPlug
    monetico: .icon-credit-card Payer avec Monetico
    pay_at_shop: .icon-store Payer en boutique

  transport:
    title: Transports
    name: Libellé
    maxWidth: Longueur max.
    maxHeight: Largeur max.
    maxDepth: Profondeur max.
    maxWeight: Poids max.
    countries: Pays
    price: Prix
    free: Gratuit
    slots: Tranches de prix
    description: Description 
    free_at_price: Gratuit à partir d'un prix d'achat
    free_at_price_help: Mettre la valeur à 0 pour ne pas appliquer de gratuité
    max_distance: Distance maximum (en Km)
    address: Adresse postale
    lat: Latitude
    lng: Longitude
    min_price: Prix minimum d'achat
    min_price_help: Le prix total du panier doit être au minimum à cette valeur pour utiliser ce transport
    add:
      title: Ajouter un transport
      success: Le transport a bien été ajouté
    edit:
      title: Modifier un transport
      success: Le transport a bien été modifié
    remove:
      title: Supprimer un transport
      message: Confirmer la suppression du transport "%entity%"
      success: Le transport a bien été supprimé
    type:
      label: Type de livraison
      shipping: Envoi 
      collect: Collecte (l'acheteur vient chercher la commande)
    service:
      label: Service
      no_service: Aucun
      mondial_relay: Mondial Relay
      colissimo: Colissimo
      la_poste: La Poste
    tracing:
      btn: .icon-truck-fast Suivre le colis 

  pay_at_shop:
    title: Réglement de la commande au retrait
    message: Votre commande et sur le point d'être validée, vous devrez régler <strong>%amount%</strong> lors du retrait de vos articles.

  gift:
    gift_wrapping: .icon-gift Emballage cadeau (%price%)
    gift_options: .icon-gift Emballage cadeau

  order_message:
    messages: Messages 
    add:
      success: Votre message a bien été envoyer
      error: Une erreure est survenue lors de l'envoi du message, veuillez ré-essayer ou nous contacter
    delete:
      delete: Supprimer un message
      confirm: Confirmer la suppression du message
      success: Le message a bien été supprimé
    notification:
      title: Nouveau message pour une commande
      content: Un nouveau message a été envoyé pour une commande
      btn: Voir le message

data:
  importer:
    type:
      shop_category: .icon-tags Catégories e-commerce
      shop_product: .icon-box-full Produits e-commerce
    import:
      shop_category: .icon-tags Importer des catégories e-commerce
      shop_product: .icon-box-full Importer des produits e-commerce

email:
  order:
    title: Merci pour votre achat !
    content: Votre commande a bien été validée, elle va être traitée dans les meilleurs délais.
    button: Récapitulatif de votre commande
  admin_order:
    title: Nouvelle commande
    content: Une nouvelle commande a été validée
    button: Récapitulatif de la commande
  shipped:
    title: Votre commande est en chemin !
    content: Bonjour, votre commande N°%n% a été envoyée.
    button: Suivre ma commande
  available:
    title: Votre commande est disponible
    content: Bonjour, votre commande N°%n% est disponible dés maintenant, vous pouvez venir la chercher.
    button: Voir ma commande

dashboard:
  title.shop: Boutique
  valid_orders: Commandes validées
  best_sale_products: .icon-trophy Meilleures ventes
  total_sale_price: Total des ventes

data.exporter.product: Produits de la boutique
data.exporter.type.product: Produits de la boutique