<?php

namespace Tigris\ShopBundle\DataImporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\DataImporter\AbstractImporter;
use Tigris\BaseBundle\Entity\FileFolder;
use Tigris\BaseBundle\Repository\FileFolderRepository;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Repository\CategoryRepository;
use Tigris\ShopBundle\Repository\ProductRepository;
use Tigris\ShopBundle\Repository\TaxRepository;

class ProductImporter extends AbstractImporter
{
    private $currentEntity;

    public function __construct(
        private $shopConfig,
        private readonly ProductRepository $productRepository,
        private readonly CategoryRepository $categoryRepository,
        protected readonly TokenStorageInterface $token,
        protected readonly EntityManagerInterface $entityManager,
        private readonly FileFolderRepository $fileFolderRepository,
        private readonly TaxRepository $taxRepository
    ) {
    }

    public function createEntity(array $data): null|object
    {
        if (!isset($data['name'])) {
            return null;
        }

        $name = $data['name'];

        // If product name exist update infos
        $entity = $this->productRepository->findOneByName($name);
        if (null === $entity) {
            $entity = new Product();
        }

        $tax = $this->taxRepository->findOneByName('TVA');
        if (null !== $tax) {
            $entity->setTax($tax);
        }

        if (isset($data['categories'])) {
            $categories = explode(',', (string) $data['categories']);

            foreach ($categories as $category) {
                $categoryEntity = $this->categoryRepository->findOneByName($category);
                if (null == $categoryEntity) {
                    $categoryEntity = (new Category())
                        ->setName($category);
                    $this->entityManager->persist($categoryEntity);
                }

                $entity->addCategory($categoryEntity);
            }
            $this->entityManager->flush();
            unset($data['categories']);
        }

        $this->currentEntity = $entity;

        $this->hydrateEntity($entity, $data);

        return $entity;
    }

    public function getEntity(string $primaryKey, mixed $primaryKeyValue, array $data): ?object
    {
        try {
            return $this->productRepository->findOneBy([$primaryKey => $primaryKeyValue]);
        } catch (\Exception) {
            return null;
        }
    }

    public function getMapping(): array
    {
        $map = [
            'name' => 'string',
            'description' => 'string',
            'reference' => 'string',
            'price' => 'float',
            'quantity' => 'int',
            'weight' => 'float',
            'image' => 'file_url',
            'categories' => 'string',
        ];

        $productField = $this->shopConfig['product_options'];
        foreach ($productField as $name => $field) {
            switch ($field['type']) {
                case 'wysiwyg':
                case 'text':
                case 'textarea':
                case 'color':
                    $map[$name] = 'string';
                    break;
                case 'language':
                    $map[$name] = 'lower_string';
                    break;
                case 'number':
                    $map[$name] = 'int';
                    break;
            }
        }

        return $map;
    }

    public function getFileFolder(): FileFolder
    {
        // Create all product folder in "Products" parent folder
        $productFolder = $this->fileFolderRepository->findOneBySlug('products');
        if (null === $productFolder) {
            $productFolder = (new FileFolder())
                ->setName('Products');

            $this->entityManager->persist($productFolder);
        }

        $folder = (new FileFolder())
            ->setName($this->currentEntity->getName())
            ->setParent($productFolder);
        $this->entityManager->persist($folder);
        $this->entityManager->flush();

        return $folder;
    }
}
