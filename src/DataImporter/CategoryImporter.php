<?php

namespace Tigris\ShopBundle\DataImporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\DataImporter\AbstractImporter;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Repository\CategoryRepository;

class CategoryImporter extends AbstractImporter
{
    public function __construct(protected readonly TokenStorageInterface $token, protected readonly EntityManagerInterface $entityManager, private readonly CategoryRepository $categoryRepository)
    {
    }

    public function createEntity(array $data): null|object
    {
        return (new Category())
            ->setName($data['name']);
    }

    public function getEntity(string $primaryKey, mixed $primaryKeyValue, array $data): ?object
    {
        try {
            return $this->categoryRepository->findOneBy([$primaryKey => $primaryKeyValue]);
        } catch (\Exception) {
            return null;
        }
    }

    public function getMapping(): array
    {
        return [
            'name' => 'string',
        ];
    }
}
