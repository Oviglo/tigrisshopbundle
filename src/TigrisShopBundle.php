<?php

namespace Tigris\ShopBundle;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Tigris\ShopBundle\DependencyInjection\TransportServicePass;

class TigrisShopBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $parameters = $container->parameters()
            ->set('tigris_shop', $config)
        ;

        foreach ($config as $key => $value) {
            $parameters->set("tigris_shop.$key", $value);
        }

        $container->import(__DIR__.'/../config/services.yaml');
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TransportServicePass());
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->arrayNode('payment_system')
                    ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                        ->scalarNode('client_id')->end()
                        ->scalarNode('client_secret')->end()
                        ->booleanNode('test')->end()
                        ->scalarNode('secret_key')->end()
                        ->scalarNode('security_key')->end()
                        ->scalarNode('company_code')->end()
                        ->scalarNode('ept_code')->end()
                        ->end()
                    ->end()
                ->end()

                ->booleanNode('enable_transport')->defaultTrue()->end()
                ->booleanNode('enable_gift_options')->defaultFalse()->end()
                ->scalarNode('mondial_relay_site_id')->defaultNull()->end()
                ->scalarNode('mondial_relay_site_key')->defaultNull()->end()
                ->scalarNode('la_poste_api_key')->defaultNull()->end()

                ->arrayNode('product_options')
                    ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('type')->end()
                            ->scalarNode('label')->end()
                            ->booleanNode('required')->defaultFalse()->end()
                            ->booleanNode('translatable')->defaultFalse()->end()
                            ->booleanNode('expanded')->end()
                            ->scalarNode('class')->end()
                            ->booleanNode('multiple')->end()
                            ->arrayNode('choices')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('value')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('options')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('value')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('attr')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('value')
                                ->prototype('scalar')->end()
                            ->end()

                        ->end()
                    ->end()
                ->end()

            ->end()
        ;
    }
}
