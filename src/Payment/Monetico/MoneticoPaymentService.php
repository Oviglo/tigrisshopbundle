<?php

namespace Tigris\ShopBundle\Payment\Monetico;

use Tigris\ShopBundle\Entity\Transport;
use DansMaCulotte\Monetico\Monetico;
use DansMaCulotte\Monetico\Receipts\PurchaseReceipt;
use DansMaCulotte\Monetico\Requests\PurchaseRequest;
use DansMaCulotte\Monetico\Resources\BillingAddressResource;
use DansMaCulotte\Monetico\Resources\ClientResource;
use DansMaCulotte\Monetico\Resources\ShippingAddressResource;
use DansMaCulotte\Monetico\Responses\PurchaseResponse;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Payment\PaymentResponse;
use Tigris\ShopBundle\Payment\PaymentServiceInterface;

class MoneticoPaymentService implements PaymentServiceInterface
{
    private readonly Monetico $monetico;

    final public const CURRENCY = 'EUR';

    public function __construct(
        string $eptCode,
        string $securityKey,
        string $companyCode,
        private readonly bool $sandbox,
        private readonly Security $token
    ) {
        $this->monetico = new Monetico($eptCode, $securityKey, $companyCode);
    }

    public function sendOrderRequest(Order $order, string $responseUrl, string $finishedUrl, string $canceledUrl): array
    {
        $user = $this->token->getUser();
        if (!$user instanceof User) {
            throw new \Exception('You need to login');
        }

        $orderAddress = $order->getOrderAddress();

        $purchase = new PurchaseRequest([
            'reference' => $order->getId(),
            'description' => '',
            'language' => 'FR',
            'email' => $user->getEmail(),
            'amount' => $order->getPrice(),
            'dateTime' => new \DateTime(),
            'currency' => self::CURRENCY,
            'errorUrl' => $canceledUrl,
            'successUrl' => $finishedUrl,
        ]);

        $billingAddress = new BillingAddressResource([
            'name' => $orderAddress->getFullName(),
            'addressLine1' => $orderAddress->getLine1(),
            'city' => $orderAddress->getCity(),
            'postalCode' => $orderAddress->getPostal(),
            'country' => $orderAddress->getCountry(),
        ]);
        $purchase->setBillingAddress($billingAddress);

        if (($transport = $order->getTransport()) instanceof Transport) {
            $shippingAddress = new ShippingAddressResource([
                'name' => $orderAddress->getFullName(),
                'addressLine1' => $orderAddress->getLine1(),
                'city' => $orderAddress->getCity(),
                'postalCode' => $orderAddress->getPostal(),
                'country' => $orderAddress->getCountry(),
            ]);
            $purchase->setShippingAddress($shippingAddress);
        }

        $client = new ClientResource([
            'civility' => $orderAddress->getCivility(),
            'firstName' => $orderAddress->getFirstname(),
            'lastName' => $orderAddress->getName(),
        ]);
        $purchase->setClient($client);

        $url = PurchaseRequest::getUrl($this->sandbox);
        $fields = $this->monetico->getFields($purchase);

        return ['url' => $url, 'fields' => $fields, 'method' => 'POST'];
    }

    public function getOrderResponse(Request $request): PaymentResponse
    {
        if ('POST' === $request->getMethod()) {
            $response = new PurchaseResponse($request->request->all());
            $responseArray = $response->toArray();

            $result = $this->monetico->validate($response);

            $receipt = new PurchaseReceipt($result);

            return (new PaymentResponse())
                ->setApiResponse($response->toArray())
                ->setApproved(in_array($responseArray['code-retour'], ['paiement', 'payetest']))
                ->setReturnValue($receipt)
                ->setOrderId($request->request->get('reference'))
                ->setTest($this->sandbox);
        }

        throw new \Exception('Error post request required', 1);
    }
}
