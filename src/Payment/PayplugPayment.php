<?php

namespace Tigris\ShopBundle\Payment;

use Payplug\Payment;
use Payplug\Payplug;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Tigris\ShopBundle\Entity\Order;

class PayplugPayment implements PaymentServiceInterface
{
    private string $secretKey;

    final public const CURRENCY = 'EUR';

    public function __construct(string $secretKey, private readonly Security $token)
    {
        $this->$secretKey = $secretKey;
        Payplug::init(['secretKey' => $secretKey]);
    }

    public function sendOrderRequest(Order $order, string $redirectUrl, string $finishedUrl, string $canceledUrl)
    {
        $basket = null;
        $user = $this->token->getUser();
        $basketOrderAddress = $basket->getOrderAddress();

        $payment = Payment::create([
            'amount' => $basket->getPrice() * 100,
            'currency' => self::CURRENCY,
            'save_card' => false,
            'billing' => [
                'title' => $basketOrderAddress->getCivility(),
                'first_name' => $basketOrderAddress->getFirstname(),
                'last_name' => $basketOrderAddress->getName(),
                'email' => $user->getEmail(),
                'address1' => $basketOrderAddress->getLine1(),
                'postcode' => $basketOrderAddress->getPostal(),
                'city' => $basketOrderAddress->getCity(),
                'country' => $basketOrderAddress->getCountry(),
                'language' => 'fr',
            ],
            'shipping' => false,
            'hosted_payment' => [
                'return_url' => "$redirectUrl?success=true",
                'cancel_url' => "$redirectUrl?success=false",
            ],
            'notification_url' => "$redirectUrl?success=false",
            'metadata' => [
                'customer_id' => $user->getId(),
            ],
        ]);

        return $payment->hosted_payment->payment_url;
    }

    public function getOrderResponse(Request $request): PaymentResponse
    {
        return new PaymentResponse();
    }
}
