<?php

namespace Tigris\ShopBundle\Payment;

use Tigris\ShopBundle\Entity\Address;

class PaymentResponse
{
    private bool $approved = false;
    private $apiResponse = [];
    private string $returnValue = '';
    private Address|null $orderAddress = null;
    private int|null $orderId = null;
    private bool $test = true;
    private string|null $paymentId = null;

    public function isApproved(): bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    public function getApiResponse(): array
    {
        return $this->apiResponse;
    }

    public function setApiResponse(string|array $apiResponse): self
    {
        $this->apiResponse = is_string($apiResponse) ? json_decode($apiResponse, true, 512, JSON_THROW_ON_ERROR) : $apiResponse;

        return $this;
    }

    public function getOrderAddress(): ?Address
    {
        return $this->orderAddress;
    }

    public function setOrderAddress(Address $orderAddress = null): self
    {
        $this->orderAddress = $orderAddress;

        return $this;
    }

    public function getReturnValue()
    {
        return $this->returnValue;
    }

    public function setReturnValue($returnValue)
    {
        $this->returnValue = $returnValue;

        return $this;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function isTest(): bool
    {
        return $this->test;
    }

    public function setTest(bool $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getPaymentId(): string|null
    {
        return $this->paymentId;
    }

    public function setPaymentId(string|null $paymentId): self
    {
        $this->paymentId = $paymentId;

        return $this;
    }
}
