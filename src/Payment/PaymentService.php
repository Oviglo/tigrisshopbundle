<?php

namespace Tigris\ShopBundle\Payment;

use Psr\Log\LoggerInterface;
use Tigris\ShopBundle\Payment\Monetico\MoneticoPaymentService;
use Tigris\ShopBundle\Payment\Paypal\PaypalPaymentService;
use Symfony\Bundle\SecurityBundle\Security;

class PaymentService
{
    private PaypalPaymentService|PayplugPayment|MoneticoPaymentService|null $service = null;

    public function __construct(private array $config, private readonly Security $token, private readonly LoggerInterface $logger)
    {
    }

    public function getPaymentService($name = 'paypal'): PaymentServiceInterface|null
    {
        $name = trim(strtolower((string) $name));
        if (null === $this->service && isset($this->config['payment_system'][$name])) {
            switch ($name) {
                case 'paypal':
                    $this->service = new PaypalPaymentService(
                        $this->config['payment_system'][$name]['client_id'],
                        $this->config['payment_system'][$name]['client_secret'],
                        $this->config['payment_system'][$name]['test'],
                        $this->logger
                    );
                    break;

                case 'payplug':
                    $this->service = new PayplugPayment(
                        $this->config['payment_system'][$name]['secret_key'],
                        $this->token,
                        $this->logger
                    );
                    break;

                case 'monetico':
                    $this->service = new MoneticoPaymentService(
                        $this->config['payment_system'][$name]['ept_code'],
                        $this->config['payment_system'][$name]['security_key'],
                        $this->config['payment_system'][$name]['company_code'],
                        $this->config['payment_system'][$name]['test'],
                        $this->token,
                        $this->logger
                    );
                    break;
            }
        }

        return $this->service;
    }
}
