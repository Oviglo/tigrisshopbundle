<?php

namespace Tigris\ShopBundle\Payment\Paypal;

use PayPalCheckoutSdk\Core\PayPalEnvironment;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Uid\Uuid;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Payment\PaymentRequest;
use Tigris\ShopBundle\Payment\PaymentResponse;
use Tigris\ShopBundle\Payment\PaymentServiceInterface;

class PaypalPaymentService implements PaymentServiceInterface
{
    private ?PayPalEnvironment $env = null;
    private readonly PayPalHttpClient $client;

    public function __construct(
        string $clientId,
        string $clientSecret,
        private readonly int $sandbox,
        private readonly LoggerInterface $logger
    ) {
        if (0 !== $sandbox) {
            $this->env = new SandboxEnvironment($clientId, $clientSecret);
        } else {
            $this->env = new ProductionEnvironment($clientId, $clientSecret);
        }

        $this->client = new PayPalHttpClient($this->env);
    }

    public function sendOrderRequest(Order $order, string $responseUrl, string $finishedUrl, string $canceledUrl)
    {
        $paymentRequest = new PaymentRequest($order, $responseUrl);
        $paypalRequest = new PaypalRequest($paymentRequest);

        $request = new OrdersCreateRequest();
        $request->headers['PayPal-Request-Id'] = (string) Uuid::v4();
        $request->prefer('return=representation');
        $request->body = $paypalRequest->getRequest();

        try {
            // Call API with your client and get a response for your call
            $response = $this->client->execute($request);

            return $response->result->links[1]->href;
        } catch (\Exception $ex) {
            $this->logger->critical($ex->getMessage());

            return "{$responseUrl}?success=false";
        }
    }

    public function getOrderResponse(Request $request): PaymentResponse
    {
        $paymentId = $request->get('token');
        $request = new OrdersCaptureRequest($paymentId);
        $request->prefer('return=representation');
        $response = new PaymentResponse();

        try {
            $result = $this->client->execute($request);
            $orderAddress = (new Address());
            // If call returns body in response, you can get the deserialized version from the result attribute of the response

            $response
                ->setApproved('COMPLETED' === $result->result->status)
                ->setApiResponse((array) $result)
                ->setOrderAddress($orderAddress)
                ->setTest($this->sandbox)
                ->setPaymentId($paymentId)
            ;

            return $response;
            // Call API with your client and get a response for your call
        } catch (\Exception $ex) {
            $response->setApproved(false)
                ->setApiResponse(['message' => $ex->getMessage()])
            ;
            $this->logger->critical($ex->getMessage());

            return $response;
        }
    }

    public function getRedirectionType(): string
    {
        return self::REDIRECTION_AUTO;
    }
}
