<?php

namespace Tigris\ShopBundle\Payment\Paypal;

use Tigris\BaseBundle\Utils\Address;
use Tigris\ShopBundle\Payment\PaymentRequest;

class PaypalRequest
{
    private array $requestInfos = [];
    private array $breakdown = [];
    private float $totalTax = 0.0;
    private float $totalProduct = 0.0;

    public function __construct(private readonly PaymentRequest $request)
    {
        $this->processConvertion();
    }

    public function getRequest(): array
    {
        return $this->requestInfos;
    }

    private function processConvertion(): void
    {
        $this->requestInfos['intent'] = 'CAPTURE';

        $this->generatePaymentSource();

        $items = [];

        foreach ($this->request->getProductWithQuantity() as $pq) {
            $product = $pq['product'];
            $item = [
                'name' => $product->getName(),
                'description' => '',
                'unit_amount' => [
                    'currency_code' => $this->request->getCurrency(),
                    'value' => number_format($product->getETPrice(true, $this->request->getOrderUser()), 2),
                ],
                'quantity' => $pq['quantity'],
            ];

            $this->totalProduct += $pq['quantity'] * number_format($product->getETPrice(true, $this->request->getOrderUser()), 2);

            if (null !== $pq['tax']) {
                $taxAmount = $pq['tax']['amount'];
                $item['tax'] = [
                    'currency_code' => $this->request->getCurrency(),
                    'value' => number_format($taxAmount, 2),
                ];

                $this->totalTax += $pq['quantity'] * number_format($pq['tax']['amount'], 2);
            }

            $items[] = $item;

            if ($pq['wrappingOption']['active']) {
                $items[] = [
                    'name' => 'Emballage cadeau',
                    'description' => '',
                    'unit_amount' => [
                        'currency_code' => $this->request->getCurrency(),
                        'value' => number_format($pq['wrappingOption']['price'], 2),
                    ],
                    'quantity' => 1,
                ];
            }
        }

        $this->generateBreakdown();

        $purchaseUnit = [
            'reference_id' => $this->request->getReference(),
            'amount' => [
                'value' => number_format($this->request->getTotalPrice(), 2),
                'currency_code' => $this->request->getCurrency(),
                'breakdown' => $this->breakdown,
            ],
            'items' => $items,
        ];

        if (null !== ($transport = $this->request->getTransportInfos()) && $this->request->getShippingAddress() instanceof Address) {
            $purchaseUnit['shipping'] = $this->getShippingAddress($transport['name']);
        }

        $this->requestInfos['purchase_units'] = [$purchaseUnit];
    }

    private function generatePaymentSource(): void
    {
        $this->requestInfos['payment_source'] = [
            'paypal' => [
                'experience_context' => [
                    'payment_method_preference' => 'IMMEDIATE_PAYMENT_REQUIRED',
                    'payment_method_selected' => 'PAYPAL',
                    'shipping_preference' => ($this->request->hasDelivery() ? 'SET_PROVIDED_ADDRESS' : 'NO_SHIPPING'),
                    'user_action' => 'PAY_NOW',
                    'cancel_url' => $this->request->getResponseUrl() . '?success=false',
                    'return_url' => $this->request->getResponseUrl() . '?success=true',
                ],
            ],
        ];
    }

    /**
     * The breakdown of the amount.
     * Breakdown provides details such as total item amount, total tax amount, shipping, handling, insurance, and discounts, if any.
     */
    private function generateBreakdown(): void
    {
        $this->breakdown = [
            'item_total' => [
                'currency_code' => $this->request->getCurrency(),
                'value' => round($this->totalProduct, 2),
            ],
        ];

        if ($this->request->getOrderDiscounts() !== []) {
            $this->breakdown['discount'] = [
                'currency_code' => $this->request->getCurrency(),
                'value' => 0,
            ];
            // Add order discounts
            foreach ($this->request->getOrderDiscounts() as $orderDiscount) {
                $discount = $orderDiscount->getDiscount();
                if ($discount->isOrderValid($this->request->getOrder())) {
                    $this->breakdown['discount']['value'] += $orderDiscount->getPriceValue();
                }
            }
        }

        if ($this->totalTax > 0) {
            $this->breakdown['tax_total'] = [
                'currency_code' => $this->request->getCurrency(),
                'value' => number_format($this->totalTax, 2),
            ];
        }

        if (null !== $transport = $this->request->getTransportInfos()) {
            $this->breakdown['shipping'] = [
                'currency_code' => $this->request->getCurrency(),
                'value' => number_format($transport['price'], 2),
            ];
        }
    }

    public function getShippingAddress(string $method): array
    {
        $address = $this->request->getShippingAddress();

        return [
            'method' => $method,
            'name' => [
                'full_name' => $address->getFullName(),
            ],
            'address' => [
                'address_line_1' => $address->line1,
                'address_line_2' => $address->line2,
                'admin_area_1' => '',
                'admin_area_2' => $address->city,
                'postal_code' => $address->zipCode,
                'country_code' => $address->countryCode,
            ],
        ];
    }
}
