<?php

namespace Tigris\ShopBundle\Payment;

use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Utils\Address;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Entity\Transport;

class PaymentRequest
{
    public function __construct(private readonly Order $order, private readonly string $responseUrl, private readonly string $currency = 'EUR') {}

    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return array<Product, int>
     */
    public function getProductWithQuantity(): array
    {
        $products = [];
        foreach ($this->order->getOrderProducts() as $orderProduct) {
            $product = $orderProduct->getProduct();
            $data = [
                'product' => $product,
                'quantity' => $orderProduct->getQuantity(),
                'tax' => null,
                'wrappingOption' => [
                    'active' => $orderProduct->hasGiftWrappingOption(),
                    'price' => $orderProduct->getGiftWrappingPrice(),
                ],
            ];

            if (null !== $tax = $product->getTax()) {
                $data['tax'] = [
                    'value' => $tax->getValue(),
                    'amount' => $product->getTaxPrice(true, $this->order->getUser()),
                ];
            }

            $products[] = $data;
        }

        return $products;
    }

    public function getTotalPrice(): float
    {
        return $this->order->getPrice();
    }

    public function getPriceWhithoutTransport(): float
    {
        return $this->order->getPrice(withTransport: false);
    }

    public function getItemsPrice(): float
    {
        return $this->order->getItemsETPrice();
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getOrderUser(): User
    {
        return $this->order->getUser();
    }

    public function getTransportInfos(): ?array
    {
        $transport = $this->order->getTransport();

        if (!$transport instanceof Transport) {
            return null;
        }

        return [
            'type' => $transport->getType(),
            'name' => $transport->getName(),
            'price' => $this->order->getTransportPrice(),
        ];
    }

    public function getShippingAddress(): ?Address
    {
        if (!$this->order->getShippingAddress() instanceof \Tigris\ShopBundle\Entity\Address && !$this->order->getOrderAddress() instanceof \Tigris\ShopBundle\Entity\Address) {
            return null;
        }

        $shippingAddress = new Address();
        $orderShippingAddress = $this->order->getShippingAddress() ?? $this->order->getOrderAddress();

        $shippingAddress->countryCode = $orderShippingAddress->getCountry();
        $shippingAddress->zipCode = $orderShippingAddress->getPostal();
        $shippingAddress->city = $orderShippingAddress->getCity();
        $shippingAddress->line1 = $orderShippingAddress->getLine1();
        $shippingAddress->line2 = $orderShippingAddress->getComplement();
        $shippingAddress->firstName = $orderShippingAddress->getFirstname();
        $shippingAddress->name = $orderShippingAddress->getName();

        return $shippingAddress;
    }

    public function hasDelivery(): bool
    {
        return $this->order->hasDelivery();
    }

    public function getResponseUrl(): string
    {
        return $this->responseUrl;
    }

    public function getReference(): string
    {
        return $this->order->getId();
    }

    public function getOrderDiscounts(): array
    {
        return $this->order->getDiscounts()->toArray();
    }
}
