<?php

namespace Tigris\ShopBundle\Payment;

use Symfony\Component\HttpFoundation\Request;
use Tigris\ShopBundle\Entity\Order;

interface PaymentServiceInterface
{
    public const REDIRECTION_AUTO = 'auto';
    public const REDIRECTION_MANUAL = 'manual';

    /**
     * Send payment request.
     */
    public function sendOrderRequest(Order $order, string $responseUrl, string $finishedUrl, string $canceledUrl);

    /**
     * Get payment response after service redirection.
     */
    public function getOrderResponse(Request $request): PaymentResponse;

    /*
    TODO: send refund

    public function refundOrderRequest(Order $order)
    */
}
