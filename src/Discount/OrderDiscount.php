<?php

namespace Tigris\ShopBundle\Discount;

class OrderDiscount
{
    public function __construct(
        private readonly string $name,
        private readonly float $value,
        private readonly int $quantity = 1
    ){
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'value' => $this->value,
            'quantity' => $this->quantity
        ];
    }
}
