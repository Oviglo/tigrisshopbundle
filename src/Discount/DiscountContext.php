<?php

namespace Tigris\ShopBundle\Discount;

use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;

class DiscountContext
{
    public function __construct(
        private readonly User|null $user,
        private readonly Product|null $product,
        private readonly Order|null $order = null
    ){
    }

    public function getUser(): User|null
    {
        return $this->user;
    }

    public function setUser(User|null $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOrder(): Order|null
    {
        return $this->order;
    }

    public function setOrder(Order|null $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getProduct(): Product|null
    {
        return $this->product;
    }

    public function setProduct(Product|null $product): self
    {
        $this->product = $product;

        return $this;
    }
}
