<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class TypeApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        $order = $context->getOrder();
        return match ($discount->getItemType()) {
            Discount::ITEM_TYPE_ORDER => $order instanceof Order,
            default => true,
        };
    }
}
