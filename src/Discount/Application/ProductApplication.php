<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class ProductApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        if ($discount->getItemType() !== Discount::ITEM_TYPE_PRODUCTS) {
            return true;
        }

        $product = $context->getProduct();

        return
            $discount->getProducts()->isEmpty() ||
            ($product instanceof Product && $discount->getProducts()->contains($product))
        ;
    }
}
