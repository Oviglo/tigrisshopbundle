<?php

namespace Tigris\ShopBundle\Discount\Application;

use Doctrine\DBAL\Query\QueryBuilder;
use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class EnabledApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        return $discount->isEnabled();
    }

    public function queryFilter(QueryBuilder $qb): void
    {
        $qb->andWhere('d.enabled = true');
    }
}
