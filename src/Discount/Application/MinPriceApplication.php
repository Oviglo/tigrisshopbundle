<?php

namespace Tigris\ShopBundle\Discount\Application;

use Doctrine\DBAL\Query\QueryBuilder;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class MinPriceApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        $user = $context->getUser();
        $order = $context->getOrder();

        if ($discount->getItemType() !== Discount::ITEM_TYPE_ORDER || !$order instanceof Order) {
            return true;
        }

        return $discount->getMinPrice() <= $order->getPrice(false, true, $user, false);
    }
}
