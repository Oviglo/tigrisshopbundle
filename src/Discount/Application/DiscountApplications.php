<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class DiscountApplications
{
    private array $applications = [];
    private string|null $whichReturnedFalse = null;

    public function __construct()
    {
        $this
            ->addApplication(EnabledApplication::class)
            ->addApplication(DateRangeApplication::class)
            ->addApplication(TypeApplication::class)
            ->addApplication(UseCountApplication::class)
            ->addApplication(MinPriceApplication::class)
            ->addApplication(CodeApplication::class)
            ->addApplication(ProductApplication::class)
            ->addApplication(CategoriesApplication::class)
            ->addApplication(UserApplication::class)
        ;
    }

    public function addApplication(string $class): self
    {
        if (!isset($this->applications[$class])) {
            $this->applications[] = $class;
        }

        return $this;
    }

    public function isValidate(Discount $discount, DiscountContext $context): bool
    {
        foreach ($this->applications as $applicationClass) {
            $application = new $applicationClass();

            if ($application instanceof DiscountApplicationInterface && !$application->isApplicable($discount, $context)) {
                $this->whichReturnedFalse = $applicationClass;
                return false;
            }
        }

        return true;
    }

    public function getWhichReturnedFalse(): string|null
    {
        return $this->whichReturnedFalse;
    }
}
