<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class UseCountApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        return $discount->getUseCountLimit() === 0 || $discount->getUseCountLimit() > $discount->getUseCount();
    }
}
