<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class CodeApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        $user = $context->getUser();

        return
            null === $discount->getCode() ||
            ($discount->haveCodeUser($user) && $discount->getCodeUsedCount($user) === 0)
        ;
    }
}
