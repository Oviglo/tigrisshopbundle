<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class UserApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        if ($discount->getUsers()->isEmpty() && $discount->getUserGroups()->isEmpty()) {
            return true;
        }

        $user = $context->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if ($discount->getUsers()->contains($user)) {
            return true;
        }

        foreach ($discount->getUserGroups() as $group) {
            if ($user->hasGroup($group)) {
                return true;
            }
        }

        return false;
    }
}
