<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

class DateRangeApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        $now = new \DateTime();

        return
            (!$discount->getStartDate() instanceof \DateTimeInterface || $discount->getStartDate() <= $now) &&
            (!$discount->getEndDate() instanceof \DateTimeInterface || $discount->getEndDate() > $now)
        ;
    }
}
