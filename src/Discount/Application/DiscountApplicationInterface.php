<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;

interface DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool;
}
