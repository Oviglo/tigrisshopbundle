<?php

namespace Tigris\ShopBundle\Discount\Application;

use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Entity\Product;

class CategoriesApplication implements DiscountApplicationInterface
{
    public function isApplicable(Discount $discount, DiscountContext $context): bool
    {
        $product = $context->getProduct();
        if ($discount->getItemType() !== Discount::ITEM_TYPE_CATEGORIES || $discount->getCategories()->isEmpty() || !$product instanceof Product) {
            return true;
        }

        $hasCategory = false;

        foreach ($product->getCategories() as $category) {
            if ($discount->getCategories()->contains($category)) {
                $hasCategory = true;
                break;
            }
        }

        return $hasCategory;
    }
}
