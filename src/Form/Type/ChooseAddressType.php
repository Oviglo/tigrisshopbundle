<?php

namespace Tigris\ShopBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\ShopBundle\Entity\Address;

class ChooseAddressType extends AbstractType
{
    public function __construct(private readonly TokenStorageInterface $tokenStorage, private readonly TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $builder
            ->add('addresses', AddressEntityType::class, [
                'label' => false,
                'class' => Address::class,
                'expanded' => true,
                'required' => false,
                'placeholder' => $this->translator->trans('shop.address.add.title'),
                'choice_label' => 'toHtml',
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('a')
                    ->where('a.user = :user')
                    ->setParameter(':user', $user)
                    ->orderBy('a.isDefault', 'ASC'),
            ])
            ->add('address', AddressType::class, [
                'label' => 'shop.address.add.title',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'validation_groups' => function (FormInterface $form) {
                // Valid new address only if "new address" radio is checked
                $formData = $form->getData();

                return (null !== $formData['addresses']) ? false : ['Default'];
            },
        ]);
    }
}
