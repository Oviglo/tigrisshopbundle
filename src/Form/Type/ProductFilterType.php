<?php

namespace Tigris\ShopBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Repository\ProductRepository;

class ProductFilterType extends AbstractType
{
    public function __construct(private readonly ProductRepository $productRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $maxPrice = $this->productRepository->findMaxPrice();
        $minPrice = $this->productRepository->findMinPrice();

        $builder
            ->add('minPrice', RangeType::class, [
                'label' => 'shop.product.min_price',
                'data' => 0,
                'attr' => [
                    'min' => (int) $minPrice,
                    'max' => (int) $maxPrice - 5,
                    'step' => 1,
                ],
            ])
            ->add('maxPrice', RangeType::class, [
                'label' => 'shop.product.max_price',
                'data' => $maxPrice,
                'attr' => [
                    'min' => (int) $minPrice,
                    'max' => (int) $maxPrice + 1,
                    'step' => 1,
                ],
            ])

            ->add('sortBy', ChoiceType::class, [
                'label' => 'shop.product.sort_by.label',
                'choices' => [
                    'shop.product.sort_by.last' => 'id,desc',
                    'shop.product.sort_by.sales' => 'saleCount,desc',
                    'shop.product.sort_by.name' => 'name,asc',
                    'shop.product.sort_by.price_asc' => 'price,asc',
                    'shop.product.sort_by.price_desc' => 'price,desc',
                ],
                'attr' => [
                    'class' => 'auto-submit',
                ],
            ])

            ->add('submitAction', SubmitType::class, [
                'label' => 'button.filter',
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();
            $parent = $data['category'] ?? null;

            $form->add('categories', EntityType::class, [
                'label' => 'shop.product.categories',
                'class' => Category::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) use ($parent) {
                    $qb = $er->createQueryBuilder('c')
                        ->where('c.public = true')
                        ->orderBy('c.left', 'ASC')
                        ->addOrderBy('c.position', 'ASC')
                    ;

                    if ($parent instanceof Category) {
                        $qb
                            ->andWhere('c.parent = :parent')
                            ->setParameter(':parent', $parent)
                        ;
                    }

                    return $qb;
                },
                'choice_value' => fn(?Category $entity) => $entity instanceof Category ? $entity->getSlug() : '',
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'name' => 'product-filter',
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
