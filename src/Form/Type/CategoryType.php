<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Tigris\BaseBundle\Form\Type\UploadFileType;
use Tigris\ShopBundle\Entity\Category;

class CategoryType extends AbstractType
{
    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

            ->add('public', CheckboxType::class, [
                'label' => 'shop.category.public',
                'required' => false,
            ])

            ->add('image', UploadFileType::class, [
                'label' => 'shop.category.image',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'shop.category.description',
                'required' => false,
            ])
        ;

        if ($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $builder
                ->add('namePrefix', null, [
                    'label' => 'name_prefix',
                ])
            ;
        }

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $category = $event->getData();

                $event->getForm()->add('parent', EntityType::class, [
                    'class' => Category::class,
                    'multiple' => false,
                    'required' => false,
                    'label' => 'shop.category.parent',
                    'empty_data' => null,
                    'query_builder' => function ($er) use ($category) {
                        $er = $er->createQueryBuilder('c');
                        if (is_object($category) && $category->getId()) {
                            $er = $er->andWhere('c.id != '.$category->getId())
                            ->andWhere('c.root != :root')
                            ->setParameter(':root', $category);
                        }

                        return $er->orderBy('c.left', 'ASC');
                    },
                    'help' => 'shop.category.parent_help',
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
