<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Form\Type\StaticType;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Transport;

class ShippType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('shippingService', TextType::class, [
                'label' => 'shop.order.shipping_service',
            ])
            ->add('trackingNumber', TextType::class, [
                'label' => 'shop.order.tracking_number',
                'required' => false,
            ])
        ;

        $translator = $this->translator;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($translator) {
            $order = $event->getData();
            $form = $event->getForm();

            if (!$order) {
                return;
            }

            $transport = $order->getTransport();
            if (!$transport instanceof Transport) {
                return;
            }

            $serviceName = $transport->getService();

            if (empty($serviceName)) {
                return;
            }

            $order->setShippingService($translator->trans('shop.transport.service.'.$serviceName));

            $form->remove('shippingService');
            $form->add('shippingService', StaticType::class, [
                'label' => 'shop.order.shipping_service',
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
