<?php

namespace Tigris\ShopBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\TableCollectionType;
use Tigris\BaseBundle\Form\Type\WysiwygType;
use Tigris\BaseBundle\Service\ArrayToFormService;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Entity\Tax;
use Tigris\ShopBundle\Entity\Transport;

class ProductType extends AbstractType
{
    private $options;

    public function __construct(array $options, private readonly ArrayToFormService $arrayToForm)
    {
        $this->options = $options['product_options'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $optionsForm = $this->arrayToForm->createFormBuilder($this->options, 'options');
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

            ->add('reference', null, [
                'label' => 'shop.product.reference',
            ])

            ->add('public', CheckboxType::class, [
                'label' => 'shop.product.public',
                'required' => false,
            ])

            ->add('type', ChoiceType::class, [
                'label' => 'shop.product.type',
                'expanded' => true,
                'choices' => [
                    'shop.product.type_standard' => Product::TYPE_STANDARD,
                    'shop.product.type_virtual' => Product::TYPE_VIRTUAL,
                ],
            ])

            ->add('categories', EntityType::class, [
                'label' => false,
                'class' => Category::class,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function (EntityRepository $er) {
                    $er = $er->createQueryBuilder('c');

                    return $er->orderBy('c.left', 'ASC');
                },
            ])

            ->add('images', TableCollectionType::class, [
                'label' => false,
                'entry_type' => ProductImageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])

            ->add('productFiles', TableCollectionType::class, [
                'label' => 'shop.product_file.files',
                'entry_type' => ProductFileType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'help' => 'shop.product_file.help',
                'by_reference' => false,
            ])

            ->add('description', WysiwygType::class, [
                'label' => 'shop.product.description',
                'tools' => 'maxi',
            ])

            ->add('tax', EntityType::class, [
                'label' => 'shop.product.tax',
                'class' => Tax::class,
            ])

            ->add('price', MoneyType::class, [
                'label' => 'shop.product.price',
                'scale' => 2,
                'attr' => [
                    'data-decimals' => 2,
                    'step' => 0.10,
                    'min' => 0,
                ],
            ])

            ->add('originalPrice', MoneyType::class, [
                'label' => 'shop.product.original_price',
                'scale' => 2,
                'help' => 'shop.product.original_price_help',
                'attr' => [
                    'data-decimals' => 2,
                    'step' => 0.10,
                    'min' => 0,
                ],
            ])

            ->add('quantity', NumberType::class, [
                'label' => 'shop.product.quantity',
                'attr' => [
                    'min' => 0,
                ],
            ])

            ->add('enableMaxQuantity', CheckboxType::class, [
                'label' => 'shop.product.enable_max_quantity',
                'required' => false,
            ])

            ->add('maxQuantity', NumberType::class, [
                'label' => 'shop.product.max_quantity',
                'attr' => [
                    'min' => 1,
                ],
            ])

            /*->add('width', NumberType::class, [
                'label' => 'shop.product.width',
                'required' => false,
                'attr' => [
                    'data-decimals' => 3,
                    'step' => 0.100,
                    'min' => 0,
                ],
            ])

            ->add('height', NumberType::class, [
                'label' => 'shop.product.height',
                'required' => false,

                'attr' => [
                    'data-decimals' => 3,
                    'step' => 0.100,
                    'min' => 0,
                ],
            ])

            ->add('depth', NumberType::class, [
                'label' => 'shop.product.depth',
                'required' => false,
                'attr' => [
                    'data-decimals' => 3,
                    'step' => 0.100,
                    'min' => 0,
                ],
            ])*/

            ->add('weight', NumberType::class, [
                'label' => 'shop.product.weight',
                'required' => false,
                'attr' => [
                    'data-decimals' => 3,
                    'step' => 0.100,
                    'min' => 0,
                ],
            ])

            ->add('deliveryAddedPrice', MoneyType::class, [
                'label' => 'shop.product.delivery_added_price',
                'required' => false,
            ])

            ->add('disableDeliveryPriceOnFreeShipping', CheckboxType::class, [
                'label' => 'shop.product.disable_delivery_price_on_free_shipping',
                'required' => false,
            ])

            ->add('useTransports', EntityType::class, [
                'label' => 'shop.product.use_transports',
                'help' => 'shop.product.use_transports_help',
                'class' => Transport::class,
                'multiple' => true,
                'expanded' => true,
            ])

            ->add('availableStatus', ChoiceType::class, [
                'label' => 'shop.product.available_status.label',
                'choices' => [
                    'shop.product.available_status.available' => Product::STATUS_AVAILABLE,
                    'shop.product.available_status.unavailable' => Product::STATUS_UNAVAILABLE,
                    'shop.product.available_status.available_at' => Product::STATUS_AVAILABLE_AT,
                    'shop.product.available_status.pre_order' => Product::STATUS_PRE_ORDER,
                ],
            ])

            ->add('availableDate', DateTimeType::class, [
                'label' => 'shop.product.available_date',
            ])

            ->add('preOrderDate', DateTimeType::class, [
                'label' => 'shop.product.pre_order_date',
            ])

            ->add($optionsForm)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
