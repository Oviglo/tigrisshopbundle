<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\TableCollectionType;
use Tigris\ShopBundle\Entity\Transport;

class TransportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'shop.transport.name',
            ])

            ->add('countries', CountryType::class, [
                'label' => 'shop.transport.countries',
                'multiple' => true,
                // 'preferred_choices' => ['FR', 'BE', 'LU', 'IT', 'ES', 'DE'],
            ])

            ->add('slots', TableCollectionType::class, [
                'label' => false,
                'entry_type' => TransportSlotType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false,
            ])

            ->add('freeAtPrice', MoneyType::class, [
                'label' => 'shop.transport.free_at_price',
                'help' => 'shop.transport.free_at_price_help',
            ])

            ->add('maxDistance', NumberType::class, [
                'label' => 'shop.transport.max_distance',
            ])

            ->add('address', TextType::class, [
                'label' => 'shop.transport.address',
                'attr' => [
                    'placeholder' => 'shop.address.search',
                    'class' => 'address-autocomplete',
                ],
            ])

            ->add('lat', TextType::class, [
                'label' => 'shop.transport.lat',
            ])

            ->add('lng', TextType::class, [
                'label' => 'shop.transport.lng',
            ])

            ->add('description', TextType::class, [
                'label' => 'shop.transport.description',
                'required' => false,
            ])

            ->add('minPrice', MoneyType::class, [
                'label' => 'shop.transport.min_price',
                'required' => true,
                'help' => 'shop.transport.min_price_help',
            ])

            ->add('type', ChoiceType::class, [
                'label' => 'shop.transport.type.label',
                'required' => true,
                'expanded' => true,
                'choices' => [
                    'shop.transport.type.shipping' => Transport::TYPE_SHIPPING,
                    'shop.transport.type.collect' => Transport::TYPE_COLLECT,
                ],
            ])

            ->add('service', ChoiceType::class, [
                'label' => 'shop.transport.service.label',
                'choices' => Transport::SERVICES_CHOICES,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Transport::class,
        ]);
    }
}
