<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MondialRelayChoiceType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'addresses' => [],
            'expanded' => true,
            'choices' => [],
        ]);

        $resolver->setAllowedTypes('addresses', ['array']);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $choices = [];
        foreach ($options['addresses'] as $address) {
            $choices[$address->name] = $address;
        }

        $view->vars['choices'] = $choices;
        $options['choices'] = $choices;
        $view->vars['addresses'] = $options['addresses'];
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'mondialRelayChoice';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): ?string
    {
        return ChoiceType::class;
    }
}
