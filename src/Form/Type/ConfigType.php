<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ConfigType extends AbstractType
{
    public function __construct(private readonly bool $enableGiftOptions)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('clickcollect', ClickCollectConfigType::class, [
                'label' => 'shop.config.click_collect',
            ])
        ;

        if ($this->enableGiftOptions) {
            $builder
                ->add('giftoptions', GiftOptionsConfigType::class, [
                    'label' => 'shop.config.gift_options',
                ])
            ;
        }
    }
}
