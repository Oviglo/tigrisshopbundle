<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Tigris\BaseBundle\Form\Type\WysiwygType;

class ClickCollectConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('address', TextType::class, [
                'label' => 'shop.config.click_address',
                'attr' => [
                    'class' => 'address-autocomplete',
                ],
            ])
            ->add('infos', WysiwygType::class, [
                'label' => 'shop.config.click_infos',
                'required' => false,
                'help' => 'shop.config.click_infos_help',
                'tools' => 'mini',
            ])

            ->add('payAtShop', CheckboxType::class, [
                'label' => 'shop.config.pay_at_shop',
                'required' => false,
                'help' => 'shop.config.pay_at_shop_help',
            ])

        ;
    }
}
