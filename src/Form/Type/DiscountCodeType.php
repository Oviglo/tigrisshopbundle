<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DiscountCodeType extends AbstractType
{
    public function getParent(): ?string
    {
        return TextType::class;
    }
}
