<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Tigris\ShopBundle\Entity\Tax;
use Tigris\ShopBundle\Repository\TaxRepository;

class GiftOptionsConfigType extends AbstractType
{
    public function __construct(private readonly TaxRepository $taxRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('enabled', CheckboxType::class, [
                'label' => 'shop.config.gift_options_enable',
            ])

            ->add('price', MoneyType::class, [
                'label' => 'shop.config.gift_options_price',
                'scale' => 2,
                'attr' => [
                    'data-decimals' => 2,
                    'step' => 0.10,
                    'min' => 0,
                ],
            ])

            ->add('tax', EntityType::class, [
                'label' => 'shop.config.gift_options_tax',
                'class' => Tax::class,
                'required' => false,
            ])
        ;

        $builder->get('enabled')
            ->addModelTransformer(new CallbackTransformer(
                fn($valueAsString) => 1 == (int) $valueAsString,
                fn($valueAsBoolean) => $valueAsBoolean
            ))
        ;

        $taxRepository = $this->taxRepository;

        $builder->get('tax')
            ->addModelTransformer(new CallbackTransformer(
                fn($valueAsInt) => $taxRepository->findOneById($valueAsInt),
                fn($valueAsObject) => $valueAsObject
            ))
        ;
    }
}
