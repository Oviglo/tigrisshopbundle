<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Intl\Countries;
use Tigris\ShopBundle\TransportService\MondialRelay;

class MondialRelayType extends AbstractType
{
    public function __construct(private readonly MondialRelay $mondialRelay)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $mondialRelay = $this->mondialRelay;

        $postalCityForm = $builder
            ->create('postal_city', FormType::class, [
                'label' => false,
            ])
            ->add('postal', TextType::class, [
                'label' => false,
            ])
            ->add('city', TextType::class, [
                'label' => false,
                'required' => false,
            ])
            ->add('country', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    Countries::getName('FR') => 'FR',
                    Countries::getName('BE') => 'BE',
                    Countries::getName('ES') => 'ES',
                    Countries::getName('PT') => 'PT',
                    Countries::getName('NL') => 'NL',
                ],
            ])
        ;

        $builder
            ->add($postalCityForm)
        ;

        $formModifier = function (FormInterface $form, array $data = null) use ($mondialRelay) {
            $city = $data['city'] ?? null;
            $postal = $data['postal'] ?? null;
            $country = $data['country'] ?? 'FR';
            $results = [];
            if (!empty($city) && !empty($postal)) {
                $results = $mondialRelay->getDeliveryChoice($postal, $city, $country);
            }

            $choices = [];
            foreach ($results as $result) {
                $choices[$result->name] = $result->id;
            }

            $form->add('relay', MondialRelayChoiceType::class, [
                'label' => false,
                'choices' => $choices,
                'addresses' => $results,
            ]);
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formModifier) {
            $data = $event->getData();
            $form = $event->getForm();

            $formModifier($form, $data['postal_city']);
        });

        $builder->get('postal_city')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formModifier) {
            $form = $event->getForm();
            $formModifier($form->getParent(), $form->getData());
        });
    }
}
