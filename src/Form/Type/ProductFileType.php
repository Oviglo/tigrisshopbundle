<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\UploadFileType;
use Tigris\ShopBundle\Entity\ProductFile;

class ProductFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', UploadFileType::class, [
                'label' => 'shop.product_file.file',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'shop.product_file.description',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductFile::class,
        ]);
    }
}
