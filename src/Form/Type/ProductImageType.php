<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Form\Type\UploadFileType;
use Tigris\ShopBundle\Entity\ProductImage;

class ProductImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('image', UploadFileType::class, [
                'label' => false,
                'class' => File::class,
                'multiple' => false,
            ])
            ->add('position', NumberType::class, [
                'label' => 'shop.product_image.position',
                'help' => 'shop.product_image.position_help',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductImage::class,
        ]);
    }
}
