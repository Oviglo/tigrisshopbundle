<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\ShopBundle\Entity\Address;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('postal', TextType::class, [
                'label' => 'shop.address.postal',
            ])
            ->add('city', TextType::class, [
                'label' => 'shop.address.city',
            ])
            ->add('country', CountryType::class, [
                'label' => 'shop.address.country',
                'preferred_choices' => ['FR', 'BE', 'DE', 'IT', 'ES', 'LU'],
            ])
            ->add('line1', TextType::class, [
                'label' => 'shop.address.line1',
            ])
            ->add('name', TextType::class, [
                'label' => 'shop.address.name',
            ])
            ->add('phone', TelType::class, [
                'label' => 'shop.address.phone',
            ])
            ->add('civility', ChoiceType::class, [
                'label' => 'shop.address.civility',
                'choices' => [
                    'shop.address.mister' => 'mr',
                    'shop.address.miss' => 'ms',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'shop.address.firstname',
            ])

            ->add('lat', HiddenType::class)
            ->add('lng', HiddenType::class)

            ->add('search', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'shop.address.search',
                    'class' => 'address-autocomplete',
                    'autocomplete' => 'chrome-off',
                ],
                'mapped' => false,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
