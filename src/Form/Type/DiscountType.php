<?php

namespace Tigris\ShopBundle\Form\Type;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Entity\Discount;

class DiscountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'shop.discount.name',
            ])

            ->add('description', TextareaType::class, [
                'label' => 'shop.discount.description',
                'required' => false,
                'attr' => [
                    'rows' => 5,
                ],
            ])

            ->add('type', ChoiceType::class, [
                'label' => 'shop.discount.type.label',
                'choices' => [
                    'shop.discount.type.percent' => Discount::DISCOUNT_TYPE_PERCENT,
                    'shop.discount.type.value' => Discount::DISCOUNT_TYPE_VALUE,
                    'shop.discount.type.new_price' => Discount::DISCOUNT_TYPE_NEW_PRICE,
                ],
            ])

            ->add('value', NumberType::class, [
                'label' => 'shop.discount.value',
                'html5' => true,
            ])

            ->add('itemType', ChoiceType::class, [
                'label' => 'shop.discount.item_type.label',
                'choices' => [
                    'shop.discount.item_type.products' => Discount::ITEM_TYPE_PRODUCTS,
                    'shop.discount.item_type.categories' => Discount::ITEM_TYPE_CATEGORIES,
                    'shop.discount.item_type.order' => Discount::ITEM_TYPE_ORDER,
                    // 'shop.discount.item_type.all_products' => Discount::ITEM_TYPE_ALL_PRODUCTS,
                ],
            ])

            ->add('products', ProductSelectType::class, [
                'label' => 'shop.discount.products',
                'multiple' => true,
                'help' => 'shop.discount.products_help',
                'required' => false,
            ])

            ->add('categories', EntityType::class, [
                'label' => 'shop.discount.categories',
                'multiple' => true,
                'class' => Category::class,
                'help' => 'shop.discount.categories_help',
                'required' => false,
            ])

            ->add('userGroups', EntityType::class, [
                'label' => 'shop.discount.user_groups',
                'multiple' => true,
                'class' => Group::class,
                'help' => 'shop.discount.user_groups_help',
                'required' => false,
            ])

            ->add('users', EntityType::class, [
                'label' => 'shop.discount.users',
                'multiple' => true,
                'class' => User::class,
                'help' => 'shop.discount.users_help',
                'required' => false,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
                    ->where('u.enabled = true')
                    ->orderBy('u.fullName', 'ASC'),
            ])

            ->add('enabled', CheckboxType::class, [
                'label' => 'shop.discount.enabled',
                'required' => false,
            ])

            ->add('code', DiscountCodeType::class, [
                'label' => 'shop.discount.code',
                'help' => 'shop.discount.code_help',
                'required' => false,
            ])

            ->add('useCountLimit', NumberType::class, [
                'label' => 'shop.discount.use_count_limit',
                'help' => 'shop.discount.use_count_limit_help',
                'required' => false,
            ])

            ->add('minPrice', MoneyType::class, [
                'label' => 'shop.discount.min_price',
                'scale' => 2,
                'attr' => [
                    'data-decimals' => 2,
                    'step' => 0.10,
                    'min' => 0,
                ],
            ])

            ->add('startDate', DateTimeType::class, [
                'label' => 'shop.discount.start_date',
            ])

            ->add('endDate', DateTimeType::class, [
                'label' => 'shop.discount.end_date',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Discount::class,
        ]);
    }
}
