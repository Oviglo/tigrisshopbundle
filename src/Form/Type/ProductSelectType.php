<?php

namespace Tigris\ShopBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Tigris\BaseBundle\Form\Type\DynamicListSelectType;
use Tigris\ShopBundle\Entity\Product;

class ProductSelectType extends DynamicListSelectType
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, Product::class);
    }

    public function getBlockPrefix(): string
    {
        return 'productSelect';
    }
}
