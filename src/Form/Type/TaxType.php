<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\ShopBundle\Entity\Tax;

class TaxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

            ->add('value', NumberType::class, [
                'label' => 'shop.tax.value',
                'scale' => 2,
                'html5' => true,
                'attr' => [
                    'data-decimals' => 2,
                    'step' => 0.10,
                    'data-suffix' => '%',
                    'min' => 0,
                    'max' => 100,
                ],
            ])

            /*->add('localization', CountryType::class, array(
                'label' => 'shop.tax.localization',
                'preferred_choices' => ['fr', 'de', 'be', 'lu', 'es', 'it'],
            ))*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tax::class,
        ]);
    }
}
