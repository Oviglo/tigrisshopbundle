<?php

namespace Tigris\ShopBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\ShopBundle\Entity\TransportSlot;

class TransportSlotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('maxWeight', NumberType::class, [
                'label' => 'shop.transport.maxWeight',
                'required' => false,
                'attr' => [
                    'data-decimals' => 3,
                    'step' => 0.100,
                    'min' => 0,
                ],
            ])

            ->add('price', MoneyType::class, [
                'label' => 'shop.transport.price',
                'required' => false,
                'attr' => [
                    'data-decimals' => 2,
                    'step' => 0.10,
                    'min' => 0,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TransportSlot::class,
        ]);
    }
}
