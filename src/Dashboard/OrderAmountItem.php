<?php

namespace Tigris\ShopBundle\Dashboard;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Tigris\BaseBundle\Contracts\Dashboard\DashboardItemInterface;
use Tigris\BaseBundle\Dashboard\ItemConfigBuilder;
use Tigris\BaseBundle\Dashboard\ItemDataBuilder;
use Tigris\BaseBundle\Dashboard\Location;
use Tigris\BaseBundle\Dashboard\Type;
use Tigris\BaseBundle\Dashboard\ValueFormat;
use Tigris\ShopBundle\Repository\OrderRepository;

#[AutoconfigureTag('tigris_base.dashboard_item')]
class OrderAmountItem implements DashboardItemInterface
{
    public function __construct(private readonly OrderRepository $orderRepository) {}

    public function configure(): array
    {
        return (new ItemConfigBuilder())
            ->id('total-sale-price')
            ->icon('sack')
            ->title('dashboard.total_sale_price')
            ->route('tigris_shop_admin_order_index')
            ->placement(Location::HEADER, 3)
            ->type(Type::ICON_COUNTER)
            ->toArray()
        ;
    }

    public function data(): array
    {
        return (new ItemDataBuilder())
            ->value($this->orderRepository->getValidTotalPrice(), ValueFormat::CURRENCY)
            ->toArray()
        ;
    }
}