<?php

namespace Tigris\ShopBundle\Dashboard;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Tigris\BaseBundle\Contracts\Dashboard\DashboardItemInterface;
use Tigris\BaseBundle\Dashboard\ItemConfigBuilder;
use Tigris\BaseBundle\Dashboard\ItemDataBuilder;
use Tigris\BaseBundle\Dashboard\Location;
use Tigris\BaseBundle\Dashboard\Type;
use Tigris\ShopBundle\Entity\OrderStatus;
use Tigris\ShopBundle\Repository\OrderRepository;

#[AutoconfigureTag('tigris_base.dashboard_item')]
class OrderCounterItem implements DashboardItemInterface
{
    public function __construct(private readonly OrderRepository $orderRepository) {}

    public function configure(): array
    {
        return (new ItemConfigBuilder)
            ->id('valid-orders')
            ->icon('file-invoice')
            ->title('dashboard.valid_orders')
            ->route('tigris_shop_admin_order_index')
            ->placement(Location::HEADER, 2)
            ->type(Type::ICON_COUNTER)
            ->toArray()
        ;
    }

    public function data(): array
    {
        $orderCount = $this->orderRepository->count(['status' => OrderStatus::COMPLETED]);
        return (new ItemDataBuilder)
            ->value($orderCount)
            ->toArray()
        ;
    }
}