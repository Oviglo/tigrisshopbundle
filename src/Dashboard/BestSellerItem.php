<?php

namespace Tigris\ShopBundle\Dashboard;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Tigris\BaseBundle\Contracts\Dashboard\DashboardItemInterface;
use Tigris\BaseBundle\Dashboard\ItemConfigBuilder;
use Tigris\BaseBundle\Dashboard\ItemDataBuilder;
use Tigris\BaseBundle\Dashboard\Location;
use Tigris\BaseBundle\Dashboard\Type;
use Tigris\ShopBundle\Repository\ProductRepository;

#[AutoconfigureTag('tigris_base.dashboard_item')]
class BestSellerItem implements DashboardItemInterface
{
    public function __construct(private readonly ProductRepository $productRepository) {}

    public function configure(): array
    {
        return (new ItemConfigBuilder)
            ->id('best-sales-products')
            ->title('dashboard.best_sale_products')
            ->route('tigris_shop_admin_product_index')
            ->placement(Location::SECTION_1, 1)
            ->type(Type::LIST)
            ->toArray()
        ;
    }

    public function data(): array
    {
        $bestSellers = $this->productRepository->findData(['count' => 10, 'orders' => ['saleCount' => 'DESC', 'id' => 'DESC'], 'minSaleCount' => 1]);

        $idb = new ItemDataBuilder();

        foreach ($bestSellers as $bestSeller) {
            $idb->addListItem($bestSeller->getName(), (string) $bestSeller->getSaleCount());
        }

        return $idb->toArray();
    }
}