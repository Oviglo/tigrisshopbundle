<?php

namespace Tigris\ShopBundle\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostLoadEventArgs;
use Doctrine\ORM\Events;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\ShopBundle\Entity\OrderStatus;

#[AsDoctrineListener(event: Events::postLoad)]
class DoctrineListener
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function postLoad(PostLoadEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof OrderStatus) {
            $entity->setTranslator($this->translator);
        }
    }
}
