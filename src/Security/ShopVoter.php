<?php

namespace Tigris\ShopBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Manager\ConfigManager;

class ShopVoter extends Voter
{
    final public const GIFT_WRAPPING_OPTION = 'gift_wrapping_option';

    public function __construct(
        private readonly bool $enableGiftOptions,
        private readonly ConfigManager $configManager
    ) {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return self::GIFT_WRAPPING_OPTION === $attribute;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        if (self::GIFT_WRAPPING_OPTION === $attribute) {
            $config = $this->configManager->getGroupValue('TigrisShopBundle.giftoptions');

            return $this->enableGiftOptions && (bool) ($config['enabled'] ?? false);
        }

        return false;
    }
}
