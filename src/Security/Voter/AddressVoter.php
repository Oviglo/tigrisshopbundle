<?php

namespace Tigris\ShopBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Address;

class AddressVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $subject instanceof Address && in_array($attribute, ['view', 'edit']);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        return match ($attribute) {
            'view' => $this->authorizationChecker->isGranted('ROLE_ADMIN') || $subject->getUser()->getId() == $user->getId(),
            'edit' => $this->authorizationChecker->isGranted('ROLE_ADMIN') || $subject->getUser()->getId() == $user->getId(),
            default => false,
        };
    }
}
