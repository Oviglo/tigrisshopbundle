<?php

namespace Tigris\ShopBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Order;

class OrderVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';
    public const CANCEL = 'cancel';

    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker) {}

    protected function supports(string $attribute, $subject): bool
    {
        return $subject instanceof Order && in_array($attribute, [self::VIEW, self::EDIT, self::CANCEL]);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');

        switch ($attribute) {
            case self::VIEW:
                return $subject->getUser()->getId() == $user->getId() || $isAdmin;

            case self::EDIT:
                if ($isAdmin) {
                    return true;
                }

                return !$subject->getInvoices()->isEmpty();

            case self::CANCEL:
                if ($isAdmin) {
                    return true;
                }
                
                if (!$subject->getInvoices()->isEmpty()) {
                    return false;
                }

                return $subject->getUser()->getId() == $user->getId() || $isAdmin;
        }

        return false;
    }
}
