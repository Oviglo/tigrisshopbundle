<?php

namespace Tigris\ShopBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Product;

class ProductVoter extends Voter
{
    final public const VIEW = 'view';
    final public const EDIT = 'edit';
    final public const BUY = 'buy';

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::BUY])) {
            return false;
        }

        return $subject instanceof Product;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::VIEW => $this->canView($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            self::BUY => $this->canBuy($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    public function canView($subject, $user): bool
    {
        if (!$user instanceof User) {
            return $subject->isPublic();
        }

        return $subject->isPublic() || $user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMIN');
    }

    public function canEdit($subject, $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        return $user->hasRole('ROLE_SUPER_ADMIN') || $user->hasRole('ROLE_ADMIN') || $user->hasRole('ROLE_SHOP_ADMIN');
    }

    public function canBuy(Product $subject, $user): bool
    {
        $available = false;
        if (Product::STATUS_AVAILABLE === $subject->getAvailableStatus()) {
            $available = true;
        } elseif (Product::STATUS_AVAILABLE_AT === $subject->getAvailableStatus() && $subject->getAvailableDate() < new \DateTime()) {
            $available = true;
        } elseif (Product::STATUS_PRE_ORDER === $subject->getAvailableStatus() && $subject->getPreOrderDate() < new \DateTime()) {
            $available = true;
        }

        return $subject->isPublic() && ($subject->getQuantity() > 0 || Product::TYPE_VIRTUAL == $subject->getType()) && $available;
    }
}
