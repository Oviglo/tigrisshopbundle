<?php

namespace Tigris\ShopBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\ShopBundle\Entity\BasketProduct;
use Tigris\ShopBundle\Entity\Product;

class BasketProductVoter extends Voter
{
    final public const ADD_PRODUCT = 'add_product';

    protected function supports(string $attribute, $subject): bool
    {
        if (self::ADD_PRODUCT !== $attribute) {
            return false;
        }

        return $subject instanceof BasketProduct;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::ADD_PRODUCT => $this->canAddProduct($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    public function canAddProduct(BasketProduct $subject, $user): bool
    {
        if (0 === $subject->getProduct()->getQuantity() && Product::TYPE_VIRTUAL !== $subject->getProduct()->getType()) {
            return false;
        }

        if (0 === $subject->getQuantity()) {
            return true;
        }

        return !$subject->getProduct()->isEnableMaxQuantity() || ($subject->getQuantity() <= $subject->getProduct()->getMaxQuantity());
    }
}
