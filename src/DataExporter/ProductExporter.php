<?php

namespace Tigris\ShopBundle\DataExporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\DataExporter\AbstractExporter;
use Tigris\BaseBundle\Writer\FacebookMarketWriter;
use Tigris\ShopBundle\Repository\ProductRepository;

class ProductExporter extends AbstractExporter
{
    public function __construct(EntityManagerInterface $entityManager, array $configs, private readonly ProductRepository $productRepository, TranslatorInterface $translator, private readonly RouterInterface $router)
    {
        parent::__construct($entityManager, $configs, $translator);
    }

    public function format(array $criteria = [], string $writerName = null): array
    {
        $entities = $this->productRepository->findData($criteria);
        $data = [];
        if ($writerName === FacebookMarketWriter::getName()) {
            $data[] = $this->generateHeader(['id', 'title', 'description', 'availability', 'condition', 'price', 'link', 'image_link', 'brand']);
        } else {
            $data[] = $this->generateHeader(['shop.product.name', 'shop.product.quantity', 'shop.product.price', 'shop.product.description']);
        }

        foreach ($entities as $entity) {
            if ($writerName === FacebookMarketWriter::getName()) {
                $description = substr(strip_tags((string) $entity->getDescription()), 0, 9999);
                if (strlen($description) < 30) {
                    continue;
                }

                $data[] = [
                    ['value' => $entity->getId()],
                    ['value' => $entity->getName()],
                    ['value' => $description],
                    ['value' => $entity->getQuantity() > 0 ? 'in stock' : 'out of stock'],
                    ['value' => 'new'],
                    ['value' => str_replace(',', '.', (string) $entity->getPrice()).' EUR'],
                    ['value' => $this->router->generate('tigris_shop_product_show', ['slug' => $entity->getSlug()], RouterInterface::ABSOLUTE_URL)],
                    ['value' => $entity->getImage() !== null ? $entity->getImage()->getAbsoluteWebPath() : ''],
                    ['value' => $entity->getEditor()],
                ];
            } else {
                $data[] = $this->generateRow(['name', 'quantity', 'price', 'description'], $entity);
            }
        }

        return $data;
    }
}
