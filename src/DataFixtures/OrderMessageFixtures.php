<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderMessage;

class OrderMessageFixtures extends Fixture implements DependentFixtureInterface
{
    private function data(): \Generator
    {
        yield [
            'order' => 1,
            'content' => 'Message',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $order = $this->getReference(OrderFixtures::REFERENCE.$data['order'], Order::class);
            $entity = (new OrderMessage())
                ->setOrder($order)
                ->setContent($data['content'])
            ;

            $entity->setAuthor($order->getUser());

            $manager->persist($entity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            OrderFixtures::class,
        ];
    }
}
