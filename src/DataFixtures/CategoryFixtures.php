<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ShopBundle\Entity\Category;

class CategoryFixtures extends Fixture
{
    private array $categories = [
        [
            'name' => 'Livre',
            'public' => true,
            'parent' => null,
        ],
        [
            'name' => 'Jeux Vidéo',
            'public' => true,
            'parent' => null,
        ],
        [
            'name' => 'K7 vidéo',
            'public' => false,
            'parent' => null,
        ],
        [
            'name' => 'Plateforme',
            'public' => true,
            'parent' => 'jeux-video',
        ],
        [
            'name' => 'Promotions',
            'public' => true,
            'parent' => null,
        ],
        [
            'name' => 'DVD - Blu-Ray',
            'public' => true,
            'parent' => null,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->categories as $category) {
            $entity = (new Category())
                ->setName($category['name'])
                ->setPublic($category['public'])
            ;

            if (null !== $category['parent'] && $this->hasReference('shop-category-'.$category['parent'], Category::class)) {
                $parent = $this->getReference('shop-category-'.$category['parent'], Category::class);
                $entity->setParent($parent);
            }

            $manager->persist($entity);

            $this->addReference('shop-category-'.Utils::slugify($entity->getName()), $entity);
        }

        $manager->flush();
    }
}
