<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Entity\Product;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    final public const REFERENCE = 'shop-product-';

    private array $data = [
        // Jeux vidéo
        [
            'name' => 'Super Mario Odyssey',
            'reference' => 'SMO',
            'categories' => ['plateforme'],
            'price' => 44.90,
            'quantity' => 15,
            'description' => 'Rejoignez Mario dans une vaste aventure en 3D à travers le globe et utilisez ses nouveaux pouvoirs pour collecter des lunes afin d\'alimenter votre vaisseau, l\'Odyssée, et sauver la princesse Peach qui a été enlevée par Bowser !',
        ],
        [
            'name' => 'Super Mario 3D World + Bowser\'s Fury',
            'reference' => 'SM3DW',
            'categories' => ['plateforme'],
            'price' => 42.90,
            'quantity' => 27,
            'description' => 'Rejoignez Mario, Luigi, Peach et Toad dans une aventure en 3D pour sauver les Sprixies du roi Bowser dans Super Mario 3D World + Bowser\'s Fury sur Nintendo Switch !',
        ],
        [
            'name' => 'Mario Party Superstars',
            'reference' => 'MPS',
            'categories' => ['jeux-video'],
            'price' => 44.90,
            'quantity' => 10,
            'description' => 'Mario Party Superstars est un jeu de société sur Nintendo Switch. Retrouvez les meilleurs mini-jeux de la série Mario Party, jouables en ligne ou en local.',
        ],
        [
            'name' => 'Mario + The Lapins Crétins Sparks of Hope',
            'reference' => 'MTLCSOH',
            'categories' => ['jeux-video'],
            'price' => 34.90,
            'quantity' => 9,
            'description' => 'Mario + The Lapins Crétins Sparks of Hope est un jeu de stratégie sur Nintendo Switch. Mario, Luigi, Peach et leurs amis s\'associent aux Lapins Crétins pour sauver les étoiles de l\'univers des griffes de Cursa, une entité maléfique.',
        ],
        [
            'name' => 'The Legend Of Zelda Breath Of The Wild',
            'reference' => 'TLOZBOTW',
            'categories' => ['jeux-video'],
            'price' => 51.90,
            'quantity' => 15,
            'description' => 'The Legend of Zelda : Breath of the Wild est un jeu d\'action/aventure. Link se réveille d\'un sommeil de 100 ans dans un royaume d\'Hyrule dévasté. Il lui faudra percer les mystères du passé et vaincre Ganon, le fléau.',
        ],
        [
            'name' => 'The Legend Of Zelda Tears Of The Kingdom',
            'reference' => 'TLOZTOTK',
            'categories' => ['jeux-video'],
            'price' => 44.99,
            'quantity' => 5,
            'description' => 'The Legend of Zelda : Tears of the Kingdom est un jeu d\'action/aventure. Link se réveille d\'un sommeil de 100 ans dans un royaume d\'Hyrule dévasté. Il lui faudra percer les mystères du passé et vaincre Ganon, le fléau.',
        ],
        [
            'name' => 'Stardew Valley',
            'reference' => 'STVA',
            'categories' => ['jeux-video'],
            'price' => 22.50,
            'quantity' => 2,
            'description' => 'Stardew Valley est un jeu de simulation de ferme. Vous héritez de la vieille ferme de votre grand-père dans la vallée de Stardew. Armé de quelques outils et de quelques pièces, vous commencez votre nouvelle vie. Pouvez-vous apprendre à vivre de la terre et à transformer ces champs en un foyer prospère ?',
        ],
        [
            'name' => 'Disney Illusion Island',
            'reference' => 'DII',
            'categories' => ['jeux-video'],
            'price' => 34.99,
            'quantity' => 13,
            'description' => 'Disney Illusion Island est un jeu d\'aventure sur Nintendo Switch. Mickey et ses amis se retrouvent piégés dans un monde de dessin animé. Ils devront résoudre des énigmes et combattre des ennemis pour retrouver leur liberté.',
        ],
        [
            'name' => '51 Worldwide Games',
            'reference' => '51WWG',
            'categories' => ['jeux-video'],
            'price' => 15.99,
            'quantity' => 1,
            'description' => '51 Worldwide Games est une compilation de jeux de société sur Nintendo Switch. Découvrez 51 jeux du monde entier, jouables en solo ou en multijoueur.',
        ],
        [
            'name' => 'Sonic Mania Plus',
            'reference' => 'SMP',
            'categories' => ['jeux-video'],
            'price' => 19.99,
            'quantity' => 6,
            'description' => 'Sonic Mania Plus est un jeu de plateforme. Rejoignez Sonic, Tails et Knuckles dans une aventure rétro à travers des niveaux familiers et inédits.',
        ],
        [
            'name' => 'Cuphead',
            'reference' => 'CH',
            'categories' => ['jeux-video'],
            'price' => 22.50,
            'quantity' => 5,
            'description' => 'Cuphead est un jeu de plateforme. Cuphead et Mugman ont perdu une partie de leur âme au Diable. Ils devront parcourir des mondes étranges et affronter des boss redoutables pour les récupérer.',
        ],
        [
            'name' => 'Rayman Legends : Definitive Edition',
            'reference' => 'RAYMANLDE',
            'categories' => ['jeux-video'],
            'price' => 32.50,
            'quantity' => 2,
            'description' => 'Rayman Legends : Definitive Edition est un jeu de plateforme. Rayman, Globox et les Ptizêtres se lancent dans une nouvelle aventure pour sauver les Lums et libérer les 4 mondes légendaires.',
        ],
        [
            'name' => 'Fire Emblem Three Houses',
            'reference' => 'FETH',
            'categories' => ['jeux-video'],
            'price' => 48.49,
            'quantity' => 8,
            'description' => 'Fire Emblem : Three Houses est un jeu de rôle tactique. Choisissez votre maison et menez vos élèves au combat pour protéger Fódlan des forces obscures qui menacent de le détruire.',
        ],
        [
            'name' => 'Xenoblade Chronicles: Definitive Edition',
            'reference' => 'XCDE',
            'categories' => ['jeux-video'],
            'price' => 42.49,
            'quantity' => 15,
            'description' => 'Xenoblade Chronicles : Definitive Edition est un jeu de rôle. Shulk part en quête de vengeance après que sa ville a été attaquée par des machines. Il découvrira que le monde est bien plus vaste et mystérieux qu\'il ne l\'imaginait.',
        ],
        [
            'name' => 'Octopath Traveler',
            'reference' => 'OCTOTRAV',
            'categories' => ['jeux-video'],
            'price' => 99.49,
            'quantity' => 2,
            'description' => 'Octopath Traveler est un jeu de rôle. Huit aventuriers se lancent dans des quêtes personnelles à travers le continent d\'Orsterra. Chacun a ses propres compétences et motivations, mais tous sont liés par le destin.',
        ],
        // Livres
        [
            'name' => 'Astérix le gaulois',
            'reference' => 'ALG',
            'categories' => ['livre', 'promotions'],
            'price' => 9.99,
            'quantity' => 0,
            'description' => 'Astérix le Gaulois est le premier album de la série de bande dessinée Astérix, publié en 1961. Il a été écrit par René Goscinny et illustré par Albert Uderzo.',
        ],
        [
            'name' => "Kaamelott, Tome 1 : L'Armée Du Nécromant",
            'reference' => 'KT1',
            'categories' => ['livre'],
            'price' => 13.95,
            'quantity' => 1,
            'description' => 'Kaamelott, Tome 1 : L\'Armée Du Nécromant est le premier tome de la série de bande dessinée Kaamelott, publié en 2019. Il a été écrit par Alexandre Astier et Steven Dupré.',
        ],

        // DVD
        [
            'name' => 'Super Mario Bros. Le Film',
            'reference' => 'SMBM',
            'categories' => ['dvd-blu-ray'],
            'price' => 19.95,
            'quantity' => 30,
            'description' => 'Super Mario Bros. Le Film est un film de science-fiction réalisé par Rocky Morton et Annabel Jankel, sorti en 1993. Il met en scène Bob Hoskins et John Leguizamo dans les rôles de Mario et Luigi.',
        ],
        [
            'name' => 'The Dark Knight Trilogy',
            'reference' => 'TDKT',
            'categories' => ['dvd-blu-ray'],
            'price' => 27.49,
            'quantity' => 15,
            'description' => 'The Dark Knight Trilogy est une trilogie de films de super-héros réalisée par Christopher Nolan. Elle met en scène Christian Bale dans le rôle de Batman.',
        ],
        [
            'name' => 'Maléfique',
            'reference' => 'MLF',
            'categories' => ['dvd-blu-ray'],
            'price' => 14.99,
            'quantity' => 8,
            'description' => 'Maléfique est un film fantastique réalisé par Robert Stromberg, sorti en 2014. Il met en scène Angelina Jolie dans le rôle de Maléfique.',
        ],
        [
            'name' => 'Friends Intégrale',
            'reference' => 'FI',
            'categories' => ['dvd-blu-ray'],
            'price' => 85.99,
            'quantity' => 2,
            'description' => 'Friends est une série télévisée américaine créée par David Crane et Marta Kauffman, diffusée de 1994 à 2004. Elle met en scène Jennifer Aniston, Courteney Cox, Lisa Kudrow, Matt LeBlanc, Matthew Perry et David Schwimmer.',
        ],
        [
            'name' => 'Les Goonies',
            'reference' => 'LGOO',
            'categories' => ['dvd-blu-ray'],
            'price' => 11.99,
            'quantity' => 4,
            'description' => 'Les Goonies est un film d\'aventure réalisé par Richard Donner, sorti en 1985. Il met en scène Sean Astin, Josh Brolin, Jeff Cohen, Corey Feldman, Kerri Green, Martha Plimpton et Jonathan Ke Quan.',
        ],
        [
            'name' => 'Robocop',
            'reference' => 'ROBOCOP',
            'categories' => ['dvd-blu-ray'],
            'price' => 11.99,
            'quantity' => 18,
            'description' => 'Robocop est un film de science-fiction réalisé par Paul Verhoeven, sorti en 1987. Il met en scène Peter Weller dans le rôle d\'Alex Murphy / Robocop.',
        ],
        [
            'name' => 'Rocky',
            'reference' => 'ROCKY',
            'categories' => ['dvd-blu-ray'],
            'price' => 10.99,
            'quantity' => 13,
            'description' => 'Rocky est un film de boxe réalisé par John G. Avildsen, sorti en 1976. Il met en scène Sylvester Stallone dans le rôle de Rocky Balboa.',
        ],
        [
            'name' => 'Rambo III',
            'reference' => 'RAMBO3',
            'categories' => ['dvd-blu-ray'],
            'price' => 2.99,
            'quantity' => 1,
            'description' => 'Rambo III est un film d\'action réalisé par Peter MacDonald, sorti en 1988. Il met en scène Sylvester Stallone dans le rôle de John Rambo.',
        ],
    ];

    public function getDependencies(): array
    {
        return [
            CategoryFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data as $data) {
            $entity = new Product();
            $entity->setName($data['name'])
                ->setReference($data['reference'])
                ->setPrice($data['price'])
                ->setQuantity($data['quantity'])
            ;

            if (isset($data['description'])) {
                $entity->setDescription($data['description']);
            }

            foreach ($data['categories'] as $category) {
                if ($this->hasReference('shop-category-'.$category, Category::class)) {
                    $entity->addCategory($this->getReference('shop-category-'.$category, Category::class));
                }
            }

            $manager->persist($entity);

            $this->addReference(static::REFERENCE.Utils::slugify($entity->getName()), $entity);
        }

        $manager->flush();
    }
}
