<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Tigris\ShopBundle\Entity\Address;

class AddressFixtures extends Fixture
{
    private function data(): \Generator
    {
        yield [
            'user' => 'asterix',
            'name' => 'Doe',
            'firstname' => 'John',
            'line1' => 'Adresse',
            'postal' => '88100',
            'city' => 'Saint-Dié-Des-Vosges',
            'phone' => '0600000000',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new Address())
                ->setUser($this->getReference('user-'.$data['user'], User::class))
                ->setName($data['name'])
                ->setFirstname($data['firstname'])
                ->setPostal($data['postal'])
                ->setCity($data['city'])
                ->setLine1($data['line1'])
                ->setPhone($data['phone'])
            ;

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
