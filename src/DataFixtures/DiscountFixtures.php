<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\DataFixtures\UserFixtures;
use App\Entity\User;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Entity\Product;

class DiscountFixtures extends Fixture implements DependentFixtureInterface
{
    private array $data = [
        [
            'name' => 'Super Discount',
            'type' => Discount::DISCOUNT_TYPE_PERCENT,
            'value' => 30,
            'products' => ['super-mario-odyssey'],
            'users' => [],
        ],
        [
            'name' => 'Super Discount Plus',
            'type' => Discount::DISCOUNT_TYPE_VALUE,
            'value' => 10,
            'products' => ['super-mario-odyssey'],
            'users' => [],
        ],
        [
            'name' => 'Categories Discount',
            'type' => Discount::DISCOUNT_TYPE_VALUE,
            'value' => 2,
            'products' => [],
            'users' => [],
            'categories' => ['livre', 'promotions'],
        ],
        [
            'name' => 'Happy Code',
            'type' => Discount::DISCOUNT_TYPE_NEW_PRICE,
            'value' => 5,
            'products' => ['the-legend-of-zelda-breath-of-the-wild'],
            'users' => [],
            'code' => 'HAPPYCODE',
        ],
        [
            'name' => 'Deprecated Code',
            'type' => Discount::DISCOUNT_TYPE_NEW_PRICE,
            'value' => 10,
            'products' => ['the-legend-of-zelda-breath-of-the-wild'],
            'users' => [],
            'code' => 'DEPRECATED',
            'endDate' => '1 week ago',
        ],
        [
            'name' => 'Discount code for order',
            'type' => Discount::DISCOUNT_TYPE_VALUE,
            'value' => 15,
            'products' => [],
            'users' => [],
            'code' => 'ORDERCODE',
            'itemType' => Discount::ITEM_TYPE_ORDER,
        ],
        [
            'name' => 'Deprecated discount code for order',
            'type' => Discount::DISCOUNT_TYPE_VALUE,
            'value' => 15,
            'products' => [],
            'users' => [],
            'code' => 'DEPRECATEDORDERCODE',
            'itemType' => Discount::ITEM_TYPE_ORDER,
            'endDate' => '1 week ago',
        ],
        [
            'name' => 'Discount code for order with min price',
            'type' => Discount::DISCOUNT_TYPE_VALUE,
            'value' => 15,
            'products' => [],
            'users' => [],
            'code' => 'MINPRICEORDERCODE',
            'itemType' => Discount::ITEM_TYPE_ORDER,
            'minPrice' => 20,
        ],
        [
            'name' => 'Code for Asterix',
            'type' => Discount::DISCOUNT_TYPE_VALUE,
            'value' => 4,
            'products' => [],
            'users' => ['asterix'],
            'code' => 'CODEFORASTERIX',
            'itemType' => Discount::ITEM_TYPE_ORDER,
        ],
    ];

    public function getDependencies(): array
    {
        return [
            ProductFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data as $data) {
            $entity = (new Discount())
                ->setName($data['name'])
                ->setType($data['type'])
                ->setValue($data['value'])
            ;

            if (isset($data['code'])) {
                $entity->setCode($data['code']);
            }

            if (isset($data['endDate'])) {
                $entity->setEndDate(new \DateTime($data['endDate']));
            }

            foreach ($data['products'] as $product) {
                if ($this->hasReference('shop-product-'.$product, Product::class)) {
                    $entity->addProduct($this->getReference('shop-product-'.$product, Product::class));
                }
            }

            foreach ($data['users'] as $user) {
                if ($this->hasReference(UserFixtures::REFERENCE.$user, User::class)) {
                    $entity->addUser($this->getReference(UserFixtures::REFERENCE.$user, User::class));
                }
            }

            if (isset($data['categories'])) {
                foreach ($data['categories'] as $category) {
                    $entity->addCategory($this->getReference('shop-category-'.$category, Category::class));
                }
            }

            if (isset($data['itemType'])) {
                $entity->setItemType($data['itemType']);
            }

            if (isset($data['minPrice'])) {
                $entity->setMinPrice($data['minPrice']);
            }

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
