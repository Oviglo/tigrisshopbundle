<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Entity\TransportSlot;

class TransportFixtures extends Fixture implements DependentFixtureInterface
{
    private array $data = [
        [
            'name' => 'Mondial Relay',
            'service' => Transport::SERVICE_MONDIAL_RELAY,
            'slots' => [
                [
                    'maxWeight' => 20,
                    'price' => 6.00,
                ],
            ],
            'countries' => ['FR'],
        ],
        [
            'name' => 'La Poste',
            'service' => Transport::SERVICE_LAPOSTE,
            'slots' => [
                [
                    'maxWeight' => 20,
                    'price' => 6.20,
                ],
            ],
            'countries' => ['FR'],
        ],
    ];

    public function getDependencies(): array
    {
        return [
            CategoryFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data as $data) {
            $entity = (new Transport())
                ->setName($data['name'])
            ;

            if (isset($data['service'])) {
                $entity->setService($data['service']);
            }

            foreach ($data['slots'] as $slotData) {
                $slot = (new TransportSlot())
                    ->setMaxWeight($slotData['maxWeight'])
                    ->setPrice($slotData['price'])
                ;

                $entity->addSlot($slot);
            }

            $entity->setCountries($data['countries']);

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
