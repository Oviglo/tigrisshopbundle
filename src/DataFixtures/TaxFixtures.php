<?php

namespace Tigris\ShopBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\ShopBundle\Entity\Tax;

class TaxFixtures extends Fixture
{
    private array $data = [
        [
            'name' => 'TVA 20',
            'value' => 20,
        ],
        [
            'name' => 'TVA 5',
            'value' => 5,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data as $data) {
            $entity = (new Tax())
                ->setName($data['name'])
                ->setValue($data['value'])
            ;

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
