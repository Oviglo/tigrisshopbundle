<?php

namespace Tigris\ShopBundle\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\DataFixtures\UserFixtures;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    final public const REFERENCE = 'shop-order-';

    private array $data = [
        [
            'reference' => 1,
            'user' => 'asterix',
            'createdAt' => '1 month ago',
            'products' => [
                ['product' => 'super-mario-odyssey', 'quantity' => 1],
            ],
            'address' => [
                'civility' => 'mr',
                'firstname' => 'Jean',
                'name' => 'Némarre',
                'city' => 'Vieux-Moulin',
                'postal' => '88210',
                'line1' => '17 rue du Senneçon',
                'lat' => 48.3976861,
                'lng' => 6.9959598,
            ],
        ],
        [
            'reference' => 2,
            'user' => 'obelix',
            'createdAt' => '2 months ago',
            'products' => [
                ['product' => 'super-mario-odyssey', 'quantity' => 2],
            ],
            'address' => [
                'civility' => 'mr',
                'firstname' => 'Jean',
                'name' => 'Némarre',
                'city' => 'Vieux-Moulin',
                'postal' => '88210',
                'line1' => '17 rue du Senneçon',
                'lat' => 48.3976861,
                'lng' => 6.9959598,
            ],
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data as $data) {
            $entity = (new Order())
                ->setUser($this->getReference('user-'.$data['user'], User::class))
                ->setCreatedAt(new \DateTime($data['createdAt']))
            ;

            foreach ($data['products'] as $orderProduct) {
                $entity->addProduct($this->getReference(ProductFixtures::REFERENCE.$orderProduct['product'], Product::class), $orderProduct['quantity']);
            }

            if (isset($data['address'])) {
                $address = (new Address())
                    ->setCivility($data['address']['civility'])
                    ->setName($data['address']['name'])
                    ->setFirstname($data['address']['firstname'])
                    ->setCity($data['address']['city'])
                    ->setPostal($data['address']['postal'])
                    ->setLine1($data['address']['line1'])
                    ->setLat($data['address']['lat'])
                    ->setLng($data['address']['lng'])
                ;

                $manager->persist($address);

                $entity->setOrderAddress($address);
            }

            $this->addReference(static::REFERENCE.$data['reference'], $entity);

            $manager->persist($entity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            ProductFixtures::class,
            TransportFixtures::class,
        ];
    }
}
