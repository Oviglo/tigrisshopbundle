<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Repository\PaymentLogRepository;

#[ORM\Entity(repositoryClass: PaymentLogRepository::class)]
#[ORM\Table(name: 'shop_payment_log')]
class PaymentLog
{
    final public const TYPE_SUCCESS = 0;
    final public const TYPE_ERROR = 1;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $createdAt;

    #[ORM\Column]
    private int $type = self::TYPE_SUCCESS;

    #[ORM\Column(type: Types::JSON)]
    private array $data = [];

    #[ORM\ManyToOne]
    private Order|null $order = null;

    #[ORM\ManyToOne]
    private User $user;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
