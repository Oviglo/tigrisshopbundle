<?php

namespace Tigris\ShopBundle\Entity;

use App\Entity\Group;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Intl\Currencies;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Discount\Application\DiscountApplications;
use Tigris\ShopBundle\Discount\DiscountContext;
use Tigris\ShopBundle\Repository\DiscountRepository;
use Tigris\ShopBundle\Utils\Calculator\Calculator;

#[ORM\Entity(repositoryClass: DiscountRepository::class)]
#[ORM\Table('shop_discount')]
class Discount
{
    use TimestampableEntity;

    final public const DISCOUNT_TYPE_PERCENT = 'percent';
    final public const DISCOUNT_TYPE_VALUE = 'value';
    final public const DISCOUNT_TYPE_NEW_PRICE = 'new price';

    final public const ITEM_TYPE_PRODUCTS = 'products';
    final public const ITEM_TYPE_CATEGORIES = 'categories';
    final public const ITEM_TYPE_ORDER = 'order';
    final public const ITEM_TYPE_ALL_PRODUCTS = 'all_products';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    private string $name;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'discounts')]
    #[ORM\JoinTable(name: 'shop_discount_product')]
    #[JMS\Exclude]
    private Collection $products;

    #[ORM\ManyToMany(targetEntity: Group::class)]
    #[ORM\JoinTable(name: 'shop_discount_usergroup')]
    private Collection $userGroups;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'discounts')]
    #[ORM\JoinTable(name: 'shop_discount_category')]
    private Collection $categories;

    #[ORM\Column(length: 20)]
    private string $itemType = self::ITEM_TYPE_PRODUCTS;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: 'shop_discount_user')]
    #[JMS\Exclude]
    private Collection $users;

    #[ORM\Column(length: 20)]
    private string $type = self::DISCOUNT_TYPE_PERCENT;

    #[ORM\Column(type: Types::DECIMAL, precision: 6, scale: 2)]
    private string $value = '0';

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $startDate;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endDate = null;

    #[ORM\Column]
    private bool $enabled = true;

    #[ORM\Column(length: 40, nullable: true)]
    private ?string $code = null;

    #[ORM\Column]
    private int $useCount = 0;

    #[ORM\Column]
    private int $useCountLimit = 1;

    #[ORM\OneToMany(targetEntity: DiscountUserCode::class, mappedBy: 'discount', cascade: ['persist', 'refresh', 'remove'], orphanRemoval: true)]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Collection $codeUsers;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private string $minPrice = '0.0';

    #[ORM\Column(type: Types::TEXT)]
    private string $description = '';

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->userGroups = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->startDate = new \DateTime();
        $this->categories = new ArrayCollection();
        $this->codeUsers = new ArrayCollection();
    }

    public function __sleep(): array
    {
        return ['id', 'name', 'code', 'startDate', 'endDate', 'description', 'minPrice', 'value', 'type'];
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'description' => $this->description,
            'minPrice' => $this->minPrice,
            'value' => $this->value,
            'type' => $this->type,
        ];
    }

    public function fromArray(array $data): self
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getValue(): float
    {
        return (float) $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = (string) $value;

        return $this;
    }

    public function getDiscount(Product $product, ?User $user = null): float
    {
        return Calculator::discountProductValue($this, $product, $user);
    }

    public function getOrderDiscount(Order $order, ?User $user = null): float
    {
        if ($this->itemType !== static::ITEM_TYPE_ORDER) {
            return 0;
        }

        if (!$this->isOrderValid($order)) {
            return 0;
        }

        // Without transport
        $price = $order->getPrice(false, true, $user, false);

        return Calculator::discountValue($this, $price);
    }

    public function getNewPrice(Product $product, ?User $user = null): float
    {
        $price = $product->getPrice();

        return $price - $this->getDiscount($product, $user);
    }

    public function getNewOrderPrice(Order $order, ?User $user = null): float
    {
        $price = $order->getPrice(false, true, $user, false);

        return $price - $this->getOrderDiscount($order, $user);
    }

    /**
     * Check if user can have the discount on a product.
     */
    public function isValid(?Product $product = null, ?User $user = null, ?Order $order = null): bool
    {
        $applications = new DiscountApplications();
        $context = new DiscountContext($user, $product, $order);

        return $applications->isValidate($this, $context);
    }

    public function isOrderValid(Order $order): bool
    {
        $user = $order->getUser();
        $applications = new DiscountApplications();
        $context = new DiscountContext($user, null, $order);

        return $applications->isValidate($this, $context);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUserGroups(): Collection
    {
        return $this->userGroups;
    }

    public function setUserGroups(Collection $userGroups): self
    {
        $this->userGroups = $userGroups;

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function setCategories(Collection $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function getItemType(): string
    {
        return $this->itemType;
    }

    public function setItemType(string $itemType): self
    {
        $this->itemType = $itemType;

        return $this;
    }

    public function getEnabled(): bool
    {
        return $this->enabled ?? true;
    }

    public function isEnabled(): bool
    {
        return $this->enabled ?? true;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getFormattedValue(): string
    {
        return match ($this->type) {
            self::DISCOUNT_TYPE_NEW_PRICE => $this->value.Currencies::getSymbol('EUR'),
            self::DISCOUNT_TYPE_PERCENT => '-'.$this->value.'%',
            self::DISCOUNT_TYPE_VALUE => '-'.$this->value.Currencies::getSymbol('EUR'),
            default => '',
        };
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code = null): self
    {
        $this->code = null === $code || '' === $code ? null : trim($code);

        return $this;
    }

    public function getUseCount(): int
    {
        return $this->useCount ?? 0;
    }

    public function setUseCount(int $useCount): self
    {
        $this->useCount = $useCount;

        return $this;
    }

    public function getUseCountLimit(): int
    {
        return $this->useCountLimit ?? 0;
    }

    public function setUseCountLimit(int $useCountLimit): self
    {
        $this->useCountLimit = $useCountLimit;

        return $this;
    }

    public function getCodeUsers(): Collection
    {
        return $this->codeUsers;
    }

    public function setCodeUsers(Collection $codeUsers): self
    {
        $this->codeUsers = $codeUsers;

        return $this;
    }

    public function addCodeUser(User $user): self
    {
        foreach ($this->codeUsers as $codeUser) {
            if ($codeUser->getUser()->isEqualTo($user)) {
                // User has allready actived this code
                return $this;
            }
        }

        $newUserCode = (new DiscountUserCode())
            ->setDiscount($this)
            ->setUser($user)
        ;

        $this->codeUsers[] = $newUserCode;

        return $this;
    }

    public function removeCodeUser(User $user): self
    {
        foreach ($this->codeUsers as $codeUser) {
            if ($codeUser->getUser()->isEqualTo($user)) {
                $this->codeUsers->removeElement($codeUser);
            }
        }

        return $this;
    }

    public function haveCodeUser(?User $user = null): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        foreach ($this->codeUsers as $codeUser) {
            if ($codeUser->getUser()->isEqualTo($user)) {
                return true;
            }
        }

        return false;
    }

    public function getCodeUsedCount(?User $user = null): int
    {
        if (!$user instanceof User) {
            return 0;
        }

        $count = 0;

        foreach ($this->codeUsers as $codeUser) {
            if ($codeUser->getUser()->isEqualTo($user) && $codeUser->isUsed()) {
                ++$count;
            }
        }

        return $count;
    }

    public function getCodeUser(User $user): ?DiscountUserCode
    {
        foreach ($this->codeUsers as $codeUser) {
            if ($user->isEqualTo($codeUser->getUser())) {
                return $codeUser;
            }
        }

        return null;
    }

    public function getMinPrice(): float
    {
        return (float) $this->minPrice ?? 0.0;
    }

    public function setMinPrice(float $minPrice): self
    {
        $this->minPrice = (string) $minPrice;

        return $this;
    }

    public function useCode(User $user): void
    {
        foreach ($this->codeUsers as $codeUser) {
            if ($user->isEqualTo($codeUser->getUser())) {
                $codeUser->use();
                $this->addUse();
            }
        }
    }

    public function addUse(): self
    {
        ++$this->useCount;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description ?? '';

        return $this;
    }
}
