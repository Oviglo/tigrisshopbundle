<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Tigris\ShopBundle\Utils\Calculator\Calculator;

#[ORM\Entity]
#[ORM\Table(name: 'shop_order_discount')]
class OrderDiscount
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'discounts')]
    private Order $order;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    #[JMS\Exclude]
    private ?Discount $discount = null;

    #[ORM\Column(type: Types::JSON)]
    #[JMS\Exclude]
    private array $discountData = [];

    #[ORM\Column(type: Types::DECIMAL, precision: 6, scale: 2)]
    private string $value;

    #[ORM\Column(length: 20)]
    private string $type;

    #[ORM\Column(length: 180)]
    private string $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getDiscount(): Discount
    {
        return (new Discount())->fromArray($this->discountData);
    }

    public function setDiscount(Discount $discount): self
    {
        $this->discount = $discount;
        $this->discountData = $discount->toArray();
        $this->value = $discount->getValue();
        $this->type = $discount->getType();
        $this->name = $discount->getName();

        return $this;
    }

    public function setDiscountData(array $data): self
    {
        $this->discountData = $data;

        return $this;
    }

    public function getDiscountData(): array
    {
        return $this->discountData;
    }

    public function getValue(): float
    {
        return (float) ($this->value ?? 0);
    }

    public function setValue(float $value): self
    {
        $this->value = (string) $value;

        return $this;
    }

    public function getPriceValue(): float
    {
        $price = 0.0;
        $orderPrice = $this->order->getPrice(false, true, null, false);

        return match ($this->type) {
            Discount::DISCOUNT_TYPE_PERCENT => Calculator::percent($orderPrice, $this->value),
            Discount::DISCOUNT_TYPE_VALUE => min($orderPrice, $this->value),
            Discount::DISCOUNT_TYPE_NEW_PRICE => $price - $this->value,
            default => $price,
        };
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNewOrderPrice(): float
    {
        $price = $this->order->getPrice(false, true, null, false);

        return match ($this->type) {
            Discount::DISCOUNT_TYPE_PERCENT => max($price - Calculator::percent($price, $this->value), 0),
            Discount::DISCOUNT_TYPE_VALUE => max(0, $price - $this->value),
            Discount::DISCOUNT_TYPE_NEW_PRICE => $price - ($price - $this->value),
            default => $price,
        };
    }
}
