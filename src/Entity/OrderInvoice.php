<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tigris\InvoiceBundle\Entity\Invoice;

#[ORM\Entity]
#[ORM\Table(name: 'shop_order_invoice')]
class OrderInvoice
{
    #[ORM\Id]
    #[ORM\ManyToOne]
    private Invoice $invoice;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'orderInvoices')]
    private Order $order;

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    public function setInvoice(Invoice $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }
}
