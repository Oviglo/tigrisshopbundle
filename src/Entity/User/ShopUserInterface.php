<?php

namespace Tigris\ShopBundle\Entity\User;

use Doctrine\Common\Collections\Collection;
use Tigris\ShopBundle\Entity\Address;

interface ShopUserInterface
{
    public function getAddresses(): Collection;

    public function setAddresses(Collection $addresses): self;

    public function addAddress(Address $address): self;

    public function getMainAddress(): Address|null;

    public function removeAddress(Address $address): self;

    public function getOrders(): Collection;

    public function setOrders(Collection $orders): self;
}
