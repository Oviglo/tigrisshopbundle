<?php

namespace Tigris\ShopBundle\Entity\User;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\DiscountUserCode;
use Tigris\ShopBundle\Entity\Order;

trait ShopUserTrait
{
    #[ORM\OneToMany(targetEntity: Address::class, mappedBy: 'user', cascade: ['all'])]
    #[JMS\Groups(['Admin'])]
    private Collection $addresses;

    #[ORM\OneToMany(targetEntity: Order::class, mappedBy: 'user', cascade: ['all'])]
    #[JMS\Exclude]
    private Collection $orders;

    #[ORM\OneToMany(targetEntity: DiscountUserCode::class, mappedBy: 'user')]
    #[JMS\Exclude]
    private Collection $discountCodes;

    public function initShopValues(): void
    {
        $this->addresses = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->discountCodes = new ArrayCollection();
    }

    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function getMainAddress(): Address|null
    {
        foreach ($this->addresses as $address) {
            if ($address->isDefault()) {
                return $address;
            }
        }

        return null;
    }

    public function setAddresses(Collection $addresses): self
    {
        $this->addresses = $addresses;

        return $this;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $address->setUser($this);
            $this->addresses[] = $address;
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        // set the owning side to null (unless already changed)
        if ($this->addresses->removeElement($address) && $address->getUser() === $this) {
            $address->setUser(null);
        }

        return $this;
    }

    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function setOrders(Collection $orders): self
    {
        $this->orders = $orders;

        return $this;
    }

    public function getDiscountCodes(): Collection
    {
        return $this->discountCodes;
    }
}
