<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\Model\User;

#[ORM\Entity]
#[ORM\Table(name: 'shop_basket_product')]
class BasketProduct
{
    #[ORM\Id]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Product $product;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'basketProducts')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Basket $basket;

    #[ORM\Column]
    private int $quantity = 1;

    #[ORM\Column]
    private bool $giftWrappingOption = false;

    #[ORM\Column(type: Types::DECIMAL, scale: 2, precision: 5)]
    private float $giftWrappingPrice = 0;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private Tax|null $giftWrappingTax = null;

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getBasket()
    {
        return $this->basket;
    }

    public function setBasket($basket)
    {
        $this->basket = $basket;

        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(bool $useDiscount = false, User $user = null, bool $withGiftOption = false): float
    {
        $giftPrice = 0;
        if ($withGiftOption && $this->giftWrappingOption) {
            $giftPrice = $this->getGiftWrappingPrice() * $this->getQuantity();
        }

        return ($this->product->getPrice($useDiscount, $user) * $this->getQuantity()) + $giftPrice;
    }

    public function getWeight()
    {
        return $this->quantity * $this->product->getWeight();
    }

    public function hasGiftWrappingOption(): bool
    {
        return $this->giftWrappingOption ?? false;
    }

    public function setGiftWrappingOption(bool $giftWrappingOption): self
    {
        $this->giftWrappingOption = $giftWrappingOption;

        return $this;
    }

    public function getGiftWrappingPrice(): float
    {
        return $this->giftWrappingPrice ?? 0.0;
    }

    public function setGiftWrappingPrice(float $giftWrappingPrice): self
    {
        $this->giftWrappingPrice = $giftWrappingPrice;

        return $this;
    }

    public function getGiftWrappingTax(): ?Tax
    {
        return $this->giftWrappingTax;
    }

    public function setGiftWrappingTax(Tax $giftWrappingTax): self
    {
        $this->giftWrappingTax = $giftWrappingTax;

        return $this;
    }
}
