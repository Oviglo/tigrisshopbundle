<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\Model\User;

#[ORM\Entity]
#[ORM\Table(name: 'shop_discount_user_code')]
class DiscountUserCode
{
    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'discountCodes')]
    private User $user;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'codeUsers')]
    private Discount $discount;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTimeInterface|null $usedAt = null;

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setDiscount(Discount $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscount(): Discount
    {
        return $this->discount;
    }

    public function isUsed(): bool
    {
        return $this->usedAt instanceof \DateTimeInterface;
    }

    public function getUsedAt(): ?\DateTimeInterface
    {
        return $this->usedAt;
    }

    public function setUsedAt(\DateTimeInterface $usedAt = null): self
    {
        $this->usedAt = $usedAt;

        return $this;
    }

    public function use(): self
    {
        $this->usedAt = new \DateTime();

        return $this;
    }
}
