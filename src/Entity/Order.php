<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\InvoiceBundle\Invoice\InvoiceItem;
use Tigris\ShopBundle\Entity\User\ShopUserInterface;
use Tigris\ShopBundle\Repository\OrderRepository;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: 'shop_order')]
#[ORM\HasLifecycleCallbacks]
class Order
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    final public const STATUS_NEW = 'new';
    final public const STATUS_AWAITING = 'awaiting';
    final public const STATUS_FINISHED = 'finished';
    final public const STATUS_SHIPPED = 'shipped';
    final public const STATUS_CANCELED = 'canceled';
    final public const STATUS_AVAILABLE = 'available';

    final public const PAYMENT_STATUS_CANCELED = 'canceled';
    final public const PAYMENT_STATUS_AWAITING_PAYMENT = 'awaiting_payment';
    final public const PAYMENT_STATUS_PAID = 'paid';
    final public const PAYMENT_STATUS_REFUNDED = 'refunded';

    final public const METHOD_BANK_CHECK = 'bank_check';
    final public const METHOD_CASH = 'cash';
    final public const METHOD_CREDIT_CARD = 'credit_card';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?User $user = null;

    /**
     * @var Collection<OrderProduct>
     */
    #[ORM\OneToMany(targetEntity: OrderProduct::class, mappedBy: 'order', cascade: ['all'], orphanRemoval: true)]
    #[JMS\Exclude]
    private Collection $orderProducts;

    #[ORM\Column(length: 100)]
    private string $firstname = '';

    #[ORM\Column(length: 100)]
    private string $name = '';

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private ?array $orderAddressData = [];

    #[ORM\Column(type: Types::JSON, nullable: true)]
    private ?array $shippingAddressData = [];

    #[ORM\OneToMany(targetEntity: OrderInvoice::class, mappedBy: 'order', cascade: ['all'])]
    #[JMS\Exclude]
    private Collection $orderInvoices;

    #[ORM\Column(type: Types::JSON, nullable: true)]
    #[JMS\Exclude]
    private ?array $apiResponse = [];

    #[ORM\Column(nullable: true)]
    private ?string $paymentMethod = null;

    #[ORM\Column(type: Types::JSON)]
    #[JMS\SerializedName('transport')]
    private array $transportData = [];

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private string $transportPrice = '0';

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private string $totalPrice = '0';

    #[ORM\Column(type: Types::JSON, nullable: true)]
    #[JMS\Exclude]
    private array $transportServiceData = [];

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $trackingNumber = null;

    #[ORM\Column(length: 80, nullable: true)]
    private ?string $shippingService = null;

    #[ORM\OneToMany(targetEntity: OrderStatus::class, mappedBy: 'order', cascade: ['all'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[ORM\OrderBy(['createdAt' => 'DESC', 'id' => 'DESC'])]
    private Collection $statuses;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?OrderStatus $paymentStatus = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?OrderStatus $deliveryStatus = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?OrderStatus $status = null;

    #[ORM\OneToMany(targetEntity: OrderDiscount::class, mappedBy: 'order', cascade: ['persist'])]
    private Collection $discounts;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->orderInvoices = new ArrayCollection();
        $this->statuses = new ArrayCollection();
        $this->discounts = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function prePersist()
    {
        $payStatus = (new OrderStatus())
            ->setType(OrderStatus::PAYMENT)
            ->setStatus(OrderStatus::UNPAID)
            ->setOrder($this)
        ;

        $this->statuses[] = $payStatus;
        $this->paymentStatus = $payStatus;

        $deliStatus = (new OrderStatus())
            ->setType(OrderStatus::DELIVERY)
            ->setStatus(OrderStatus::SHIPPING)
            ->setOrder($this)
        ;

        $this->statuses[] = $deliStatus;
        $this->deliveryStatus = $deliStatus;

        $status = (new OrderStatus())
            ->setType(OrderStatus::ORDER)
            ->setStatus(OrderStatus::OPEN)
            ->setOrder($this)
        ;

        $this->statuses[] = $status;
        $this->status = $status;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        if ($user instanceof ShopUserInterface) {
            $address = $user->getMainAddress();

            if ($address instanceof Address) {
                $this->setOrderAddress($address);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<OrderProduct>
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function setOrderProducts(Collection $orderProducts): self
    {
        $this->orderProducts = $orderProducts;

        return $this;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $orderProduct->setOrder($this);
            $this->orderProducts->add($orderProduct);
        }

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function addProduct(Product $product, int $quantity = 1): self
    {
        foreach ($this->orderProducts as $orderProduct) {
            if ($orderProduct->getProduct()->getId() == $product->getId()) {
                $orderProduct->setQuantity($orderProduct->getQuantity() + $quantity);

                return $this;
            }
        }

        $orderProduct = (new OrderProduct())
            ->setOrder($this)
            ->setProduct($product)
            ->setQuantity($quantity)
        ;

        $this->orderProducts[] = $orderProduct;

        return $this;
    }

    public function setProductGiftWrappingOption(Product $product, bool $enable, float $price = 0, ?Tax $tax = null): self
    {
        foreach ($this->orderProducts as $orderProduct) {
            if ($orderProduct->getProduct()->getId() == $product->getId()) {
                $orderProduct
                    ->setGiftWrappingOption($enable)
                    ->setGiftWrappingPrice($price)
                    ->setGiftWrappingTaxValue($tax->getValue())
                ;

                return $this;
            }
        }

        return $this;
    }

    public function resetProducts(): self
    {
        $this->orderProducts = new ArrayCollection();

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getPrice(bool $withTransport = true, bool $withDiscount = true, ?User $user = null, bool $withOrderDiscount = true, bool $withGiftOption = true): float
    {
        if (!$user instanceof User) {
            $user = $this->getUser();
        }

        $price = 0.0;
        foreach ($this->orderProducts as $orderProduct) {
            $productPrice = $orderProduct->getPrice($withDiscount, $withGiftOption);
            $price += $productPrice * $orderProduct->getQuantity();
        }

        // Order discounts
        if ($withOrderDiscount) {
            foreach ($this->discounts as $orderDiscount) {
                $price -= $orderDiscount->getPriceValue();
            }
        }

        // Transport allways add at the end
        if ($this->getTransport() instanceof Transport && $withTransport) {
            $price += $this->getTransportPrice();
        }

        return round($price, 2);
    }

    public function getItemsETPrice(): float
    {
        $price = 0.0;
        foreach ($this->orderProducts as $orderProduct) {
            $productPrice = $orderProduct->getETPrice();
            $price += $productPrice * $orderProduct->getQuantity();
        }

        return $price;
    }

    public function getOrderAddress(): ?Address
    {
        if ($this->orderAddressData === null || $this->orderAddressData === []) {
            return null;
        }

        return (new Address())
            ->setName($this->name)
            ->setFirstname($this->firstname)
            ->fromArray($this->orderAddressData)
        ;
    }

    public function setOrderAddress(Address $orderAddress): self
    {
        $this->name = $orderAddress->getName();
        $this->firstname = $orderAddress->getFirstname();

        $this->orderAddressData = $orderAddress->toArray();

        return $this;
    }

    public function getShippingAddress(): ?Address
    {
        if ($this->shippingAddressData === null || $this->shippingAddressData === []) {
            return $this->getOrderAddress();
        }

        return (new Address())
            ->setName($this->name)
            ->setFirstname($this->firstname)
            ->fromArray($this->shippingAddressData)
        ;
    }

    public function setShippingAddress(Address $shippingAddress): self
    {
        $this->shippingAddressData = $shippingAddress->toArray();

        return $this;
    }

    public function addInvoice(Invoice $invoice): self
    {
        foreach ($this->orderProducts as $orderProduct) {
            $tax = $orderProduct->getProduct()->getTax();
            $taxItem = [];
            if (null !== $tax) {
                $taxItem[$tax->getName()] = $tax->getValue();
            }
            $invoiceItem = new InvoiceItem($orderProduct->getProduct()->getName(), $orderProduct->getQuantity(), $orderProduct->getProduct()->getPrice(), $taxItem);
            $invoice->addItem($invoiceItem);

            // Add discount items
            foreach ($orderProduct->getDiscounts() as $discount) {
                $invoiceItem = new InvoiceItem($discount['name'], $discount['quantity'], $discount['value'] * -1, $taxItem);
                $invoice->addItem($invoiceItem);
            }

            // Gift options
            if ($orderProduct->hasGiftWrappingOption()) {
                $giftTax = [];
                if ($orderProduct->getGiftWrappingTax() instanceof Tax) {
                    $tax = $orderProduct->getGiftWrappingTax();
                    $giftTax[$tax->getName()] = $tax->getValue();
                }
                $invoiceItem = new InvoiceItem('Emballage cadeau', 1, $orderProduct->getGiftWrappingPrice(), $giftTax);
                $invoice->addItem($invoiceItem);
            }
        }

        // Order discount
        foreach ($this->discounts as $discount) {
            $invoiceItem = new InvoiceItem($discount->getName(), 1, $discount->getPriceValue() * -1, $taxItem);
            $invoice->addItem($invoiceItem);
        }

        if ($this->getTransport() instanceof Transport) {
            $invoiceItem = new InvoiceItem($this->getTransport()->getName(), 1, $this->transportPrice);
            $invoice->addItem($invoiceItem);
        }

        $orderInvoice = (new OrderInvoice())
            ->setInvoice($invoice)
            ->setOrder($this)
        ;

        $this->orderInvoices->add($orderInvoice);

        return $this;
    }

    public function getOrderInvoices(): Collection
    {
        return $this->orderInvoices;
    }

    public function getInvoices(): Collection
    {
        return $this->orderInvoices;
    }

    public function setOrderInvoices(ArrayCollection $orderInvoices): self
    {
        $this->orderInvoices = $orderInvoices;

        return $this;
    }

    public function getApiResponse(): ?array
    {
        return $this->apiResponse;
    }

    public function setApiResponse(array $apiResponse): self
    {
        $this->apiResponse = $apiResponse;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getPaymentStatus()
    {
        if (!$this->paymentStatus instanceof OrderStatus) {
            if ($this->orderInvoices->get(0)) {
                return $this->orderInvoices->get(0)->getInvoice()->getPaymentStatus();
            }

            // return Invoice::STATUS_AWAITING_PAYMENT;
        }

        return $this->paymentStatus;
    }

    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getStatus(string $type = OrderStatus::ORDER): ?OrderStatus
    {
        foreach ($this->statuses as $orderStatus) {
            if ($orderStatus->getType() === $type) {
                return $orderStatus;
            }
        }

        return null;
    }

    public function setStatus(string $status): self
    {
        $orderStatus = (new OrderStatus())
            ->setType(OrderStatus::ORDER)
            ->setStatus($status)
            ->setOrder($this)
        ;

        $this->statuses[] = $orderStatus;
        $this->status = $orderStatus;

        return $this;
    }

    public function getTransport(): ?Transport
    {
        if ($this->transportData === []) {
            return null;
        }

        return (new Transport())->fromArray($this->transportData);
    }

    public function setTransport(?Transport $transport): self
    {
        if ($transport instanceof Transport) {
            $transport->setOrder($this);
            $this->transportPrice = $transport->getPrice();
            $this->transportData = $transport->toArray();
        }

        return $this;
    }

    public function getDeliveryAddedPrice($transportPrice = 0)
    {
        $addedPrice = 0;
        foreach ($this->orderProducts as $orderProduct) {
            if (!(0 == $transportPrice && $orderProduct->getProduct()->getDisableDeliveryPriceOnFreeShipping())) {
                $addedPrice += ($orderProduct->getProduct()->getDeliveryAddedPrice() * $orderProduct->getQuantity());
            }
        }

        return $addedPrice;
    }

    public function getTransportPrice(): float
    {
        return (float) $this->transportPrice;
    }

    public function setTransportPrice(float $transportPrice): self
    {
        $this->transportPrice = (string) $transportPrice;

        return $this;
    }

    public function getWeight(): float
    {
        $w = 0;
        foreach ($this->orderProducts as $orderProduct) {
            $w += $orderProduct->getWeight();
        }

        return $w;
    }

    public function getTrackingNumber(): ?string
    {
        return $this->trackingNumber;
    }

    public function setTrackingNumber(?string $trackingNumber): self
    {
        $this->trackingNumber = $trackingNumber;

        return $this;
    }

    public function getShippingService(): ?string
    {
        return $this->shippingService;
    }

    public function setShippingService(?string $shippingService): self
    {
        $this->shippingService = $shippingService;

        return $this;
    }

    public function isCollect()
    {
        return $this->getTransport() instanceof Transport && Transport::TYPE_COLLECT === $this->getTransport()->getType();
    }

    public function getStatuses(): Collection
    {
        return $this->statuses;
    }

    public function setStatuses(ArrayCollection $statuses): self
    {
        $this->statuses = $statuses;

        return $this;
    }

    public function setPaymentStatus(string $status): self
    {
        $orderStatus = (new OrderStatus())
            ->setType(OrderStatus::PAYMENT)
            ->setStatus($status)
            ->setOrder($this)
        ;

        $this->statuses[] = $orderStatus;
        $this->paymentStatus = $orderStatus;

        return $this;
    }

    public function setDeliveryStatus(string $status): self
    {
        $orderStatus = (new OrderStatus())
            ->setType(OrderStatus::DELIVERY)
            ->setStatus($status)
            ->setOrder($this)
        ;

        $this->statuses[] = $orderStatus;
        $this->deliveryStatus = $orderStatus;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function isCanceled(): bool
    {
        foreach ($this->statuses as $status) {
            if (OrderStatus::ORDER == $status->getType()) {
                return OrderStatus::CANCELED == $status->getStatus();
            }
        }

        return false;
    }

    #[JMS\VirtualProperty]
    public function isPaid(): bool
    {
        foreach ($this->statuses as $status) {
            if (OrderStatus::PAYMENT == $status->getType()) {
                return OrderStatus::PAID == $status->getStatus();
            }
        }

        return false;
    }

    public function hasDelivery(): bool
    {
        foreach ($this->orderProducts as $orderProduct) {
            if (Product::TYPE_STANDARD == $orderProduct->getProduct()->getType()) {
                return true;
            }
        }

        return false;
    }

    public function getTransportServiceData(): array
    {
        return $this->transportServiceData;
    }

    public function setTransportServiceData(array $transportServiceData): self
    {
        $this->transportServiceData = $transportServiceData;

        return $this;
    }

    /**
     * Refresh all products information (price, discounts ...).
     */
    public function refresh(): self
    {
        foreach ($this->orderProducts as $orderProduct) {
            $orderProduct->refresh($this->getUser());
        }

        return $this;
    }

    public function getDiscounts(): Collection
    {
        return $this->discounts;
    }

    public function setDiscounts(Collection $discounts): self
    {
        $this->discounts = $discounts;

        return $this;
    }

    public function addDiscount(Discount $discount): self
    {
        if (!$discount->isOrderValid($this)) {
            return $this;
        }

        foreach ($this->discounts as $orderDiscount) {
            if ($orderDiscount->getDiscount() === $discount) {
                return $this;
            }
        }

        $orderDiscount = (new OrderDiscount())
            ->setOrder($this)
            ->setDiscount($discount)
        ;

        $this->discounts[] = $orderDiscount;

        return $this;
    }

    public function useDiscount(): void
    {
        foreach ($this->orderProducts as $orderProduct) {
            $orderProduct->useDiscount($this->user);
        }

        foreach ($this->discounts as $orderDiscount) {
            $orderDiscount->getDiscount()->useCode($this->user);
        }
    }

    public function refreshValidDiscount(): void
    {
        foreach ($this->discounts as $orderDiscount) {
            $discount = $orderDiscount->getDiscount();
            if (!$discount->isOrderValid($this)) {
                $this->discounts->removeElement($orderDiscount);
            }
        }
    }

    #[JMS\VirtualProperty]
    public function getFormattedNumber(): string
    {
        return str_pad($this->id, 5, '0', STR_PAD_LEFT);
    }

    public function refreshFromBasket(Basket $basket, array $giftOptions = []): void
    {
        $this->resetProducts();
        foreach ($basket->getBasketProducts() as $basketProduct) {
            if ($basketProduct->getQuantity() > 0) {
                $this->addProduct($basketProduct->getProduct(), $basketProduct->getQuantity());
                if (($giftOptions['enabled'] ?? false) && $basketProduct->hasGiftWrappingOption()) {
                    $this->setProductGiftWrappingOption(
                        $basketProduct->getProduct(),
                        true,
                        $giftOptions['price'] ?? $basketProduct->getGiftWrappingPrice(),
                        $giftOptions['tax'] ?? $basketProduct->getGiftWrappingTax()
                    );
                }
            }
        }

        $this->refresh();
    }

    public function getTransportData(): ?array
    {
        return $this->transportData;
    }

    public function setTransportData(?array $transportData): self
    {
        $this->transportData = $transportData;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getMainInvoice(): ?Invoice
    {
        if ($orderInvoice = $this->orderInvoices->first()) {
            return $orderInvoice->getInvoice();
        }

        return null;
    }

    public function getOrderAddressData(): ?array
    {
        return $this->orderAddressData;
    }

    public function setOrderAddressData(?array $orderAddressData): self
    {
        $this->orderAddressData = $orderAddressData;

        return $this;
    }

    public function getShippingAddressData(): ?array
    {
        return $this->shippingAddressData;
    }

    public function setShippingAddressData(?array $shippingAddressData): self
    {
        $this->shippingAddressData = $shippingAddressData;

        return $this;
    }

    public function getTotalPrice(): float
    {
        return (float) $this->totalPrice;
    }

    public function setTotalPrice(float $totalPrice): self
    {
        $this->totalPrice = (string) $totalPrice;

        return $this;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function prePersistUpdate(): void
    {
        $this->totalPrice = (string) $this->getPrice();
    }
}
