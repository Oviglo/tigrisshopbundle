<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Repository\OrderMessageRepository;

#[ORM\Entity(repositoryClass: OrderMessageRepository::class)]
#[ORM\Table(name: 'shop_order_message')]
class OrderMessage
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\ManyToOne()]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[JMS\Exclude()]
    private Order $order;

    #[ORM\Column(type: Types::TEXT)]
    private string $content;

    #[ORM\ManyToOne()]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private User $author;

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }
}
