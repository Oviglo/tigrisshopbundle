<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\Model\User;

#[ORM\Entity]
#[ORM\Table(name: 'shop_basket')]
class Basket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private User $user;

    /**
     * @var Collection<BasketProduct>
     */
    #[ORM\OneToMany(targetEntity: BasketProduct::class, mappedBy: 'basket', cascade: ['all'], orphanRemoval: true)]
    private Collection $basketProducts;

    #[ORM\OneToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private Order|null $order = null;

    /**
     * @var Collection<Discount>
     */
    #[ORM\ManyToMany(targetEntity: Discount::class)]
    #[ORM\JoinTable(name: 'shop_basket_discount')]
    private Collection $discounts;

    public function __construct()
    {
        $this->basketProducts = new ArrayCollection();
        $this->discounts = new ArrayCollection();
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBasketProducts(): Collection
    {
        return $this->basketProducts;
    }

    public function getBasketProduct(Product $product)
    {
        foreach ($this->basketProducts as $basketProduct) {
            if ($basketProduct->getProduct()->getId() == $product->getId()) {
                return $basketProduct;
            }
        }

        return null;
    }

    public function setBasketProducts(Collection $basketProducts): self
    {
        $this->basketProducts = $basketProducts;

        return $this;
    }

    public function addProduct(Product $product, int $quantity = 1): self
    {
        if (Product::TYPE_VIRTUAL != $product->getType()) {
            $quantity = min($quantity, $product->getQuantity());
        }

        foreach ($this->basketProducts as $basketProduct) {
            if ($basketProduct->getProduct()->getId() == $product->getId()) {
                $basketProduct->setQuantity($basketProduct->getQuantity() + $quantity);

                return $this;
            }
        }

        $basketProduct = (new BasketProduct())
            ->setBasket($this)
            ->setProduct($product)
            ->setQuantity($quantity)
        ;

        $this->basketProducts[] = $basketProduct;

        return $this;
    }

    /**
     * @return array<Product>
     */
    public function getProducts(): array
    {
        $products = [];
        foreach ($this->basketProducts as $basketProduct) {
            $products[] = $basketProduct->getProduct();
        }

        return $products;
    }

    public function changeQuantity(Product $product, $quantity)
    {
        foreach ($this->basketProducts as $basketProduct) {
            if ($basketProduct->getProduct()->getId() === $product->getId()) {
                if ($quantity > 0) {
                    $basketProduct->setQuantity($quantity);
                } else {
                    $this->basketProducts->removeElement($basketProduct);
                }

                return $this;
            }
        }
        return null;
    }

    public function removeProduct(Product $product): self
    {
        foreach ($this->basketProducts as $basketProduct) {
            if ($basketProduct->getProduct()->getId() == $product->getId()) {
                $this->basketProducts->removeElement($basketProduct);

                return $this;
            }
        }

        return $this;
    }

    public function getPrice(User $user = null): float
    {
        $price = 0.0;
        foreach ($this->basketProducts as $basketProduct) {
            $price += $basketProduct->getPrice(true, $user, true);
        }

        return $price;
    }

    public function hasDelivery(): bool
    {
        foreach ($this->basketProducts as $basketProduct) {
            if (Product::TYPE_STANDARD == $basketProduct->getProduct()->getType()) {
                return true;
            }
        }

        return false;
    }

    public function getProductCount(): int
    {
        return count($this->basketProducts);
    }

    public function getShippingAddress()
    {
        return $this->order->getShippingAddress();
    }

    public function getOrderAddress(): Address
    {
        return $this->order->getOrderAddress();
    }

    public function getProductsQuantity()
    {
        $quantity = 0;
        foreach ($this->basketProducts as $bp) {
            $quantity += $bp->getQuantity();
        }

        return $quantity;
    }

    public function getProductQuantity(Product $product)
    {
        foreach ($this->basketProducts as $bp) {
            if ($bp->getProduct()->getId() == $product->getId()) {
                return $bp->getQuantity();
            }
        }

        return 0;
    }

    public function getOrder(): null|Order
    {
        return $this->order;
    }

    public function setOrder(null|Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getWeight()
    {
        $w = 0;
        foreach ($this->basketProducts as $basketProduct) {
            $w += $basketProduct->getWeight();
        }

        return $w;
    }

    public function getMaxWidth()
    {
        $maxWidth = 0;
        foreach ($this->basketProducts as $basketProduct) {
            $maxWidth = max($maxWidth, $basketProduct->getProduct()->getMaxWidth());
        }

        return $maxWidth;
    }

    public function getMaxHeight()
    {
        $maxHeight = 0;
        foreach ($this->basketProducts as $basketProduct) {
            $maxHeight = max($maxHeight, $basketProduct->getProduct()->getMaxHeight());
        }

        return $maxHeight;
    }

    public function getMaxDepth()
    {
        $maxDepth = 0;
        foreach ($this->basketProducts as $basketProduct) {
            $maxDepth = max($maxDepth, $basketProduct->getProduct()->getMaxDepth());
        }

        return $maxDepth;
    }

    /**
     * Return the total transport price majoration (product added price).
     */
    public function getDeliveryAddedPrice(): float
    {
        $addedPrice = 0;
        foreach ($this->basketProducts as $basketProduct) {
            $addedPrice += ($basketProduct->getProduct()->getDeliveryAddedPrice() * $basketProduct->getQuantity());
        }

        return $addedPrice;
    }

    /**
     * Return unavailable transport for one or more product.
     */
    public function getExcludeTransports(): ArrayCollection
    {
        $excludeTransports = new ArrayCollection();

        foreach ($this->basketProducts as $basketProduct) {
            $product = $basketProduct->getProduct();
            foreach ($product->getUseTransports() as $transport) {
                if (!$excludeTransports->contains($transport)) {
                    $excludeTransports->add($transport);
                }
            }
        }

        return $excludeTransports;
    }

    public function getDiscounts(): Collection
    {
        return $this->discounts;
    }

    public function addDiscount(Discount $discount): self
    {
        if (!$this->discounts->contains($discount) && Discount::ITEM_TYPE_ORDER === $discount->getItemType()) {
            $this->discounts[] = $discount;
        }

        return $this;
    }

    public function changeGiftWrappingOption(Product $product, bool $enable, float $price, Tax $tax): self
    {
        foreach ($this->basketProducts as $basketProduct) {
            if ($basketProduct->getProduct()->getId() === $product->getId()) {
                $basketProduct
                    ->setGiftWrappingOption($enable)
                    ->setGiftWrappingPrice($price)
                    ->setGiftWrappingTaxValue($tax->getValue())
                ;

                return $this;
            }
        }
    }
}
