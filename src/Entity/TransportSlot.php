<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ORM\Table(name: 'shop_transport_slot')]
class TransportSlot
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'slots', cascade: ['all'])]
    private Transport $transport;

    #[ORM\Column(type: Types::DECIMAL, scale: 3)]
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 999)]
    private float|string $maxWeight = '0.0';

    #[ORM\Column(type: Types::DECIMAL, scale: 2)]
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 999)]
    private float|string $price = '0';

    public function getId()
    {
        return $this->id;
    }

    public function setTransport(Transport $transport)
    {
        $this->transport = $transport;

        return $this;
    }

    public function getTransport(): Transport
    {
        return $this->transport;
    }

    public function getMaxWeight(): float
    {
        return (float) $this->maxWeight;
    }

    public function setMaxWeight(float $maxWeight): self
    {
        $this->maxWeight = (string) $maxWeight;

        return $this;
    }

    public function getPrice(): float
    {
        return (float) $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'price' => $this->price,
            'maxWeight' => $this->maxWeight,
        ];
    }

    public function fromArray(array $data): self
    {
        $this->price = $data['price'] ?? 0;
        $this->maxWeight = $data['maxWeight'] ?? 0;

        return $this;
    }
}
