<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use Gedmo\Tree\Traits\NestedSetEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\File;
use Tigris\ShopBundle\Repository\CategoryRepository;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ORM\Table(name: 'shop_category')]
#[Gedmo\Tree(type: 'nested')]
class Category implements Translatable, \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;
    use NestedSetEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 180)]
    #[Gedmo\Translatable]
    #[Assert\NotBlank()]
    private string|null $name = '';

    #[ORM\Column(length: 180, nullable: true)]
    private string|null $namePrefix = null;

    #[JMS\AccessType(type: 'public_method')]
    private $treeName;

    #[ORM\Column(length: 180, unique: true)]
    #[Gedmo\Slug(fields: ['name'])]
    #[Gedmo\Translatable]
    private string $slug;

    #[ORM\Column]
    private bool $public = true;

    #[Gedmo\Locale]
    private string|null $locale = null;

    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(onDelete: 'CASCADE', nullable: true)]
    #[Gedmo\TreeParent]
    #[Gedmo\SortableGroup]
    #[JMS\Exclude]
    private Category|null $parent = null;

    #[ORM\OneToMany(targetEntity: Category::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['left' => 'ASC'])]
    #[JMS\Exclude]
    private Collection $children;

    #[ORM\ManyToMany(targetEntity: Product::class, mappedBy: 'categories')]
    #[JMS\Exclude]
    private Collection $products;

    #[ORM\Column]
    #[Gedmo\SortablePosition]
    private int $position = 0;

    #[ORM\Column]
    private bool $accessAuthenticated = false;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private File|null $image = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private string|null $description = null;

    #[ORM\ManyToMany(targetEntity: Discount::class, mappedBy: 'categories')]
    private Collection $discounts;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->left = 0;
        $this->level = 0;
        $this->right = 0;
        $this->root = null;
        $this->discounts = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function __toString(): string
    {
        return str_pad('', $this->level, '-', STR_PAD_LEFT).' '.$this->name;
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTreeName()
    {
        return str_pad('', $this->level, '-', STR_PAD_LEFT).' '.$this->name;
    }

    public function setTreeName()
    {
    }

    public function setName(string|null $name): self
    {
        $this->name = $name ?? '';

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(Category|null $parent): self
    {
        $this->parent = $parent;
        if (!$parent instanceof \Tigris\ShopBundle\Entity\Category) {
            $this->level = 0;
        }

        return $this;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts(Collection $products)
    {
        $this->products = $products;

        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    public function getLeft(): ?int
    {
        return $this->left;
    }

    public function getNamePrefix()
    {
        return $this->namePrefix;
    }

    public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;

        return $this;
    }

    public function getAccessAuthenticated()
    {
        return $this->accessAuthenticated;
    }

    public function isAccessAuthenticated()
    {
        return $this->accessAuthenticated;
    }

    public function setAccessAuthenticated($accessAuthenticated): self
    {
        $this->accessAuthenticated = $accessAuthenticated;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage(File|null $image)
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description = null)
    {
        $this->description = $description;

        return $this;
    }

    public function getDiscounts(): Collection
    {
        return $this->discounts;
    }

    public function setDiscounts(Collection $discounts): self
    {
        $this->discounts = $discounts;

        return $this;
    }
}
