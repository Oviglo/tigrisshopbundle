<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Contracts\Translation\TranslatorInterface;

#[ORM\Entity]
#[ORM\Table(name: 'shop_order_status')]
class OrderStatus implements \Stringable
{
    final public const TYPE_SUCCESS = 'success';
    final public const TYPE_WARNING = 'warning';
    final public const TYPE_DANGER = 'danger';
    final public const TYPE_INFO = 'info';
    final public const TYPE_DEFAULT = 'default';

    final public const PAYMENT = 'payment';
    final public const ORDER = 'order';
    final public const DELIVERY = 'delivery';

    final public const OPEN = 'open';
    final public const CONFIRMED = 'confirmed';
    final public const COMPLETED = 'completed';
    final public const CANCELED = 'canceled';

    final public const UNPAID = 'unpaid';
    final public const FAILED = 'failed';
    final public const EXPIRED = 'expired';
    final public const PAID = 'paid';
    final public const REFUNDING = 'refunding';
    final public const REFUNDED = 'refunded';

    final public const UNFULFILLED = 'unfulfilled';
    final public const SHIPPING = 'shipping';
    final public const SHIPPED = 'shipped';
    final public const ARRIVED = 'arrived';
    final public const COLLECTED = 'collected';
    final public const RETURNING = 'returning';
    final public const RETURNED = 'returned';
    final public const AVAILABLE = 'available';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTimeInterface|null $createdAt;

    #[ORM\Column(length: 30)]
    private string $status;

    #[ORM\Column(length: 30)]
    private string $type;

    #[ORM\ManyToOne(inversedBy: 'statuses')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Order $order;

    #[JMS\Exclude]
    private TranslatorInterface|null $translator = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int|null
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;

        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    public function getTranslator()
    {
        return $this->translator;
    }

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getTrans()
    {
        if ($this->translator instanceof TranslatorInterface) {
            return $this->translator->trans('shop.order.status.'.$this->getStatus());
        }

        return $this->getStatus();
    }

    public function __toString(): string
    {
        return $this->status;
    }

    #[JMS\VirtualProperty]
    public function getColorType()
    {
        return match ($this->status) {
            self::CANCELED, self::FAILED => self::TYPE_DANGER,
            self::COMPLETED, self::SHIPPED, self::AVAILABLE, self::PAID => self::TYPE_SUCCESS,
            self::SHIPPING, self::UNPAID, self::OPEN => self::TYPE_WARNING,
            self::CONFIRMED => self::TYPE_INFO,
            default => self::TYPE_DEFAULT,
        };
    }
}
