<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Translatable\Translatable;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\InvoiceBundle\Invoice\Calculator;
use Tigris\ShopBundle\Repository\ProductRepository;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ORM\Table(name: 'shop_product')]
#[Gedmo\Loggable]
class Product implements Translatable, \Stringable
{
    use TimestampableEntity;

    final public const TYPE_STANDARD = 1;
    final public const TYPE_VIRTUAL = 2;

    final public const STATUS_AVAILABLE = 'available';
    final public const STATUS_UNAVAILABLE = 'unavailable';
    final public const STATUS_AVAILABLE_AT = 'available_at';
    final public const STATUS_PRE_ORDER = 'pre_order';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'products')]
    #[ORM\JoinTable(name: 'shop_product_category')]
    #[ORM\JoinColumn(name: 'product_id', onDelete: 'CASCADE')]
    #[ORM\InverseJoinColumn(name: 'category_id', onDelete: 'CASCADE')]
    private Collection $categories;

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 180)]
    #[Gedmo\Translatable]
    #[Gedmo\Versioned]
    private string $name;

    #[ORM\Column(unique: true)]
    #[Gedmo\Slug(fields: ['name'])]
    #[Gedmo\Translatable]
    private ?string $slug = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Gedmo\Versioned]
    private ?string $reference = null;

    #[ORM\Column]
    private bool $public = true;

    #[ORM\Column]
    #[Assert\NotBlank]
    private int $type = self::TYPE_STANDARD;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Gedmo\Versioned]
    private ?string $description = null;

    #[ORM\OneToMany(targetEntity: ProductImage::class, mappedBy: 'product', cascade: ['persist', 'refresh', 'remove'], orphanRemoval: true)]
    #[ORM\OrderBy(['position' => 'ASC'])]
    private Collection $productImages;

    #[ORM\OneToMany(targetEntity: ProductFile::class, mappedBy: 'product', cascade: ['persist', 'refresh', 'remove'], orphanRemoval: true)]
    private Collection $productFiles;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Tax $tax = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    #[Gedmo\Versioned]
    #[Assert\GreaterThanOrEqual(0)]
    private float|string $price = 0;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    #[Assert\GreaterThanOrEqual(0)]
    private ?string $originalPrice = null;

    #[ORM\Column]
    #[Assert\GreaterThanOrEqual(0)]
    private int $quantity = 1;

    #[ORM\Column]
    #[Assert\GreaterThan(0)]
    #[JMS\Groups(['Admin'])]
    private int $maxQuantity = 1;

    #[ORM\Column]
    #[JMS\Groups(['Admin'])]
    private ?bool $enableMaxQuantity = false;

    #[ORM\Column(type: Types::DECIMAL, scale: 2, nullable: true)]
    private float|string $width = 0;

    #[ORM\Column(type: Types::DECIMAL, scale: 2, nullable: true)]
    private float|string $height = 0;

    #[ORM\Column(type: Types::DECIMAL, scale: 2, nullable: true)]
    private float|string $depth = 0;

    #[ORM\Column(type: Types::DECIMAL, scale: 3, nullable: true)]
    private float|string $weight = 0;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    #[JMS\Groups(['Admin'])]
    private float|string $deliveryAddedPrice = 0;

    #[ORM\Column]
    private bool $disableDeliveryPriceOnFreeShipping = true;

    #[ORM\Column(type: Types::JSON)]
    #[Gedmo\Translatable]
    #[Gedmo\Versioned]
    private array $options = [];

    #[ORM\Column]
    private bool $top = false;

    #[ORM\Column]
    #[JMS\Groups(['Admin'])]
    private int $saleCount = 0;

    #[ORM\ManyToMany(targetEntity: Transport::class)]
    #[ORM\JoinTable(name: 'shop_product_transport')]
    private Collection $useTransports;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $availableDate;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $preOrderDate;

    #[ORM\Column(length: 30, options: ['default' => self::STATUS_AVAILABLE])]
    private string $availableStatus = self::STATUS_AVAILABLE;

    #[ORM\ManyToMany(targetEntity: Discount::class, mappedBy: 'products')]
    private Collection $discounts;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->productImages = new ArrayCollection();
        $this->productFiles = new ArrayCollection();
        $this->useTransports = new ArrayCollection();
        $this->availableDate = new \DateTime();
        $this->preOrderDate = new \DateTime();
        $this->discounts = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function __call(string $name, array $arguments)
    {
        $option = strtolower(substr($name, 3));
        $access = strtolower(substr($name, 0, 3));

        if (array_key_exists($option, $this->options) && 'get' === $access) {
            return $this->options[$option];
        }

        if ('set' === $access && [] !== $arguments) {
            $this->options[$option] = $arguments[0];

            return $this;
        }

        return null;
    }

    public function __clone()
    {
        $this->slug = null;
        $this->name .= ' 2';
        $this->reference .= '2';
        $this->saleCount = 0;
        $this->quantity = 0;
    }

    public function __sleep(): array
    {
        return ['id', 'name', 'reference', 'price', 'deliveryAddedPrice', 'width', 'height', 'depth', 'weight', 'type'];
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'reference' => $this->reference,
            'price' => $this->price,
            'type' => $this->type,
            'deliveryAddedPrice' => $this->deliveryAddedPrice,
            'width' => $this->width,
            'height' => $this->height,
            'depth' => $this->depth,
            'weight' => $this->weight,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function setCategories(Collection $categories): Product
    {
        $this->categories = $categories;

        return $this;
    }

    public function addCategory(Category $category): Product
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function hasCategory(Category $category): bool
    {
        return $this->categories->contains($category);
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): Product
    {
        $this->type = $type;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference($reference): Product
    {
        $this->reference = $reference;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): Product
    {
        $this->name = $name;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getImages(): Collection
    {
        return $this->productImages;
    }

    public function setImages($images): self
    {
        foreach ($images as $image) {
            $this->setImage($image);
        }

        return $this;
    }

    public function setImage(?File $image = null): self
    {
        if ($image instanceof File) {
            $productImage = (new ProductImage())
                ->setImage($image)
                ->setProduct($this)
                ->setPosition(count($this->productImages));
            $this->productImages[] = $productImage;
        }

        return $this;
    }

    public function addImage(ProductImage $image): self
    {
        if (!$this->productImages->contains($image)) {
            $image->setProduct($this);
            $this->productImages[] = $image;
        }

        return $this;
    }

    public function removeImage(ProductImage $image): self
    {
        if ($this->productImages->contains($image)) {
            $this->productImages->removeElement($image);
        }

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getImage(): ?File
    {
        if (count($this->productImages) > 0) {
            return $this->productImages[0]->getImage();
        }

        return null;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): Product
    {
        $this->public = $public;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description ?? '';
    }

    public function setDescription(?string $description = null): Product
    {
        $this->description = $description;

        return $this;
    }

    public function getTax(): ?Tax
    {
        return $this->tax;
    }

    public function setTax(Tax $tax): Product
    {
        $this->tax = $tax;

        return $this;
    }

    public function getPrice(bool $useDiscount = false, ?User $user = null): float
    {
        if ($useDiscount) {
            return $this->price - $this->getDiscountValue($user);
        }

        return $this->price;
    }

    public function getTaxPrice(bool $useDiscount = false, ?User $user = null): float
    {
        if (!$this->tax instanceof Tax) {
            return 0.0;
        }

        // Pour un soucis d'arrondi de calcul
        return $this->getPrice($useDiscount, $user) - $this->getETPrice($useDiscount, $user);

        // return Calculator::taxPrice($this->getETPrice($useDiscount, $user), $this->tax->getValue());
    }

    public function getETPrice(bool $useDiscount = false, ?User $user = null): float
    {
        return $this->getETAmount($useDiscount, $user);
    }

    public function setPrice(float $price): Product
    {
        $this->price = $price;

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getFormatPrice(): string
    {
        return number_format($this->price, 2).Currencies::getSymbol('EUR');
    }

    public function getETAmount(bool $useDiscount = false, ?User $user = null): float
    {
        if ($this->tax instanceof Tax) {
            return Calculator::amountET($this->getPrice($useDiscount, $user), $this->tax->getValue());
        }

        return $this->getPrice($useDiscount, $user);
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity($quantity): Product
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getMaxQuantity(): int
    {
        $qt = $this->enableMaxQuantity ? $this->maxQuantity : $this->quantity;

        return $qt > 0 ? $qt : 1;
    }

    public function setMaxQuantity(int $maxQuantity): Product
    {
        $this->maxQuantity = $maxQuantity;

        return $this;
    }

    public function isEnableMaxQuantity(): bool
    {
        return $this->enableMaxQuantity ?? false;
    }

    public function setEnableMaxQuantity(bool $enableMaxQuantity): Product
    {
        $this->enableMaxQuantity = $enableMaxQuantity;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width = null): Product
    {
        $this->width = $width ?? 0;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height = null): Product
    {
        $this->height = $height ?? 0;

        return $this;
    }

    public function getDepth(): ?float
    {
        return $this->depth;
    }

    public function setDepth(?float $depth = null): Product
    {
        $this->depth = $depth ?? 0;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight = null): Product
    {
        $this->weight = $weight ?? 0;

        return $this;
    }

    public function getDeliveryAddedPrice(): float
    {
        return $this->deliveryAddedPrice;
    }

    public function setDeliveryAddedPrice(float $deliveryAddedPrice): Product
    {
        $this->deliveryAddedPrice = $deliveryAddedPrice;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): Product
    {
        $this->slug = $slug;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): Product
    {
        $this->options = $options;

        return $this;
    }

    public function isTop(): bool
    {
        return $this->top;
    }

    public function setTop($top): Product
    {
        $this->top = $top;

        return $this;
    }

    public function getSaleCount(): int
    {
        return $this->saleCount;
    }

    public function setSaleCount($saleCount): Product
    {
        $this->saleCount = $saleCount;

        return $this;
    }

    public function addSale($count = 1): Product
    {
        $this->saleCount += $count;

        return $this;
    }

    public function getDisableDeliveryPriceOnFreeShipping(): bool
    {
        return $this->disableDeliveryPriceOnFreeShipping;
    }

    public function setDisableDeliveryPriceOnFreeShipping(bool $disableDeliveryPriceOnFreeShipping): Product
    {
        $this->disableDeliveryPriceOnFreeShipping = $disableDeliveryPriceOnFreeShipping;

        return $this;
    }

    public function getProductImages(): Collection
    {
        return $this->productImages;
    }

    public function setProductImages(Collection $productImages): Product
    {
        $this->productImages = $productImages;

        return $this;
    }

    public function getOriginalPrice(): ?float
    {
        return (float) $this->originalPrice;
    }

    public function setOriginalPrice(?float $originalPrice = null): Product
    {
        $this->originalPrice = (string) $originalPrice;

        return $this;
    }

    public function getUseTransports(): Collection
    {
        return $this->useTransports;
    }

    public function setUseTransports(Collection $useTransports): Product
    {
        $this->useTransports = $useTransports;

        return $this;
    }

    public function getAvailableDate(): ?\DateTimeInterface
    {
        return $this->availableDate;
    }

    public function setAvailableDate(?\DateTime $availableDate = null): self
    {
        $this->availableDate = $availableDate;

        return $this;
    }

    public function getPreOrderDate()
    {
        return $this->preOrderDate;
    }

    public function setPreOrderDate(?\DateTime $preOrderDate = null): self
    {
        $this->preOrderDate = $preOrderDate;

        return $this;
    }

    public function getAvailableStatus()
    {
        return $this->availableStatus;
    }

    public function setAvailableStatus(string $availableStatus): Product
    {
        $this->availableStatus = $availableStatus;

        return $this;
    }

    /**
     * Pre order is available if current date between pre order date and available date.
     */
    public function isPreOrder(): bool
    {
        return self::STATUS_PRE_ORDER === $this->availableStatus && $this->preOrderDate <= new \DateTime() && $this->availableDate > new \DateTime();
    }

    public function getProductFiles(): Collection
    {
        return $this->productFiles;
    }

    public function addProductFile(ProductFile $productFile): Product
    {
        $productFile->setProduct($this);

        $this->productFiles[] = $productFile;

        return $this;
    }

    public function removeProductFile(ProductFile $productFile): Product
    {
        if ($this->productFiles->contains($productFile)) {
            $this->productFiles->removeElement($productFile);
        }

        return $this;
    }

    public function getDiscounts(): Collection
    {
        $discounts = clone $this->discounts;

        // Discount on categories
        foreach ($this->categories as $category) {
            foreach ($category->getDiscounts() as $discount) {
                $discounts->add($discount);
            }
        }

        return $discounts;
    }

    public function setDiscounts(Collection $discounts): Product
    {
        $this->discounts = $discounts;

        return $this;
    }

    public function getDiscountValue(?User $user = null): float
    {
        $value = 0;
        $usedDiscounts = new ArrayCollection();
        foreach ($this->getDiscounts() as $discount) {
            // A discount need to be used only one time
            if (!$usedDiscounts->contains($discount)) {
                $value += $discount->getDiscount($this, $user);
                $usedDiscounts[] = $discount;
            }
        }

        return $value;
    }
}
