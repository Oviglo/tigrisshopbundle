<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Discount\OrderDiscount;

#[ORM\Entity]
#[ORM\Table(name: 'shop_order_product')]
class OrderProduct
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    #[JMS\Exclude]
    private Product $product;

    #[ORM\ManyToOne(inversedBy: 'orderProducts')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Order $order;

    #[ORM\Column]
    private int $quantity = 1;

    #[ORM\Column(length: 120)]
    private string $name;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private string $price = '0';

    #[ORM\Column(nullable: true)]
    private string|null $tax = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private string $ETPrice;

    #[ORM\Column(type: Types::JSON)]
    private array $discounts = [];

    #[ORM\Column]
    private bool $giftWrappingOption = false;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private string $giftWrappingPrice = '0';

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    private string|null $giftWrappingTaxValue = null;

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(bool $withDiscount = false, bool $withGiftOption = false): float
    {
        $price = $this->price;

        if ($withDiscount && null != $this->discounts) {
            foreach ($this->discounts as $discount) {
                $price -= $discount['value'];
            }
        }

        if ($withGiftOption && $this->giftWrappingOption) {
            $price += $this->giftWrappingPrice;
        }

        return $price;
    }

    public function setPrice(float $price): self
    {
        $this->price = (string) $price;

        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct(Product $product, User $user = null): self
    {
        $this->product = $product;
        $this->refresh($user);

        return $this;
    }

    public function refresh(User $user = null)
    {
        $this->price = $this->product->getPrice();
        $this->name = $this->product->getName();
        $this->ETPrice = $this->product->getETPrice(true, $this->order->getUser());
        $this->tax = (string) $this->product->getTax();

        // Reset en set discounts
        $this->discounts = [];
        foreach ($this->product->getDiscounts() as $discount) {
            if ($discount->isValid($this->product, $user)) {
                $od = new OrderDiscount($discount->getName(), $discount->getDiscount($this->product, $user), $this->quantity ?? 1);

                $this->discounts[] = $od->toArray();
            }
        }

        return $this;
    }

    public function getWeight()
    {
        return $this->quantity * $this->product->getWeight();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTax()
    {
        return $this->tax;
    }

    public function setTax(string $tax)
    {
        $this->tax = $tax;

        return $this;
    }

    public function getETPrice(): float
    {
        return (float) $this->ETPrice;
    }

    public function setETPrice(float $ETPrice): self
    {
        $this->ETPrice = (string) $ETPrice;

        return $this;
    }

    public function getDiscounts()
    {
        return $this->discounts ?? [];
    }

    public function setDiscounts(array $discounts)
    {
        $this->discounts = $discounts;

        return $this;
    }

    public function addDiscount(OrderDiscount $discount)
    {
        $this->discounts[] = $discount->toArray();

        return $this;
    }

    public function useDiscount(User $user): void
    {
        foreach ($this->discounts as $discount) {
            // $discount->useCode($user);
        }
    }

    public function hasGiftWrappingOption(): bool
    {
        return $this->giftWrappingOption ?? false;
    }

    public function setGiftWrappingOption(bool $giftWrappingOption): self
    {
        $this->giftWrappingOption = $giftWrappingOption;

        return $this;
    }

    public function getGiftWrappingPrice(): float
    {
        return (float) ($this->giftWrappingPrice ?? 0.0);
    }

    public function setGiftWrappingPrice(float $giftWrappingPrice): self
    {
        $this->giftWrappingPrice = (string) $giftWrappingPrice;

        return $this;
    }

    public function getGiftWrappingTaxValue(): float|null
    {
        return (float) $this->giftWrappingTaxValue;
    }

    public function setGiftWrappingTaxValue(float|null $giftWrappingTaxValue): self
    {
        $this->giftWrappingTaxValue = (string) $giftWrappingTaxValue;

        return $this;
    }
}
