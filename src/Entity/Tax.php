<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\ShopBundle\Repository\TaxRepository;

#[ORM\Entity(repositoryClass: TaxRepository::class)]
#[ORM\Table(name: 'shop_tax')]
class Tax implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 100)]
    private string $name;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    #[Assert\Range(min: 0, max: 100)]
    private string $value;

    public function getId(): int|null
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setValue(float $value): self
    {
        $this->value = (string) $value;

        return $this;
    }

    public function getValue(): float
    {
        return (float) $this->value;
    }

    public function __toString(): string
    {
        return strtoupper($this->name)." ($this->value%)";
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'value' => $this->getValue(),
        ];
    }
}
