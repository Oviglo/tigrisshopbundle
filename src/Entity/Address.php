<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\Model\User;

#[ORM\Entity]
#[ORM\Table(name: 'shop_address')]
#[ORM\HasLifecycleCallbacks]
class Address implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 120)]
    #[Assert\NotBlank()]
    private ?string $country = '';

    #[ORM\Column(length: 20)]
    #[Assert\NotBlank]
    private ?string $postal = '';

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    private ?string $city = '';

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    private ?string $firstname = '';

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    private ?string $name = '';

    #[ORM\Column(length: 180)]
    private ?string $civility = '';

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    private ?string $line1 = '';

    private ?string $address = '';

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $complement = null;

    #[ORM\ManyToOne(inversedBy: 'addresses')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private User $user;

    #[ORM\Column]
    private bool $isDefault = false;

    #[ORM\Column(type: Types::DECIMAL, scale: 6, nullable: true)]
    private null|float|string $lat = null;

    #[ORM\Column(type: Types::DECIMAL, scale: 6, nullable: true)]
    private null|float|string $lng = null;

    #[ORM\Column(length: 15, nullable: true)]
    #[Assert\NotBlank]
    private ?string $phone = null;

    public function __sleep(): array
    {
        return ['country', 'city', 'postal', 'firstname', 'name', 'civility', 'line1', 'complement', 'lat', 'lng', 'phone'];
    }

    public function __toString(): string
    {
        $country = null != $this->getCountry() ? Countries::getName($this->getCountry()) : '';

        return "{$this->getLine1()} {$this->getPostal()} {$this->getCity()} {$country}";
    }

    public function toArray(): array
    {
        return [
            'country' => $this->country,
            'city' => $this->city,
            'postal' => $this->postal,
            'firstname' => $this->firstname,
            'name' => $this->name,
            'civility' => $this->civility,
            'line1' => $this->line1,
            'complement' => $this->complement,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'phone' => $this->phone,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    public function getPostal()
    {
        return $this->postal;
    }

    public function setPostal(?string $postal): self
    {
        $this->postal = $postal;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFullName()
    {
        return $this->firstname.' '.$this->name;
    }

    public function getCivility()
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getLine1()
    {
        return $this->line1 ?? $this->address;
    }

    public function setLine1(?string $line1): self
    {
        $this->line1 = $line1;

        return $this;
    }

    public function getComplement()
    {
        return $this->complement ?? '';
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;
        if ((null === $this->firstname || '' === $this->firstname) && method_exists($user, 'getFirstname') && !empty($user->getFirstname())) {
            $this->firstname = $user->getFirstname();
        }
        if ((null === $this->name || '' === $this->name) && method_exists($user, 'getName') && !empty($user->getName())) {
            $this->name = $user->getName();
        }

        return $this;
    }

    public function toHtml(): string
    {
        $country = null != $this->getCountry() ? Countries::getName($this->getCountry()) : '';

        $html = "<strong>{$this->getName()} {$this->getFirstname()}</strong> <br/>
        {$this->getLine1()}<br/>
        {$this->getPostal()} {$this->getCity()} <br/>
        {$country}";

        if (null !== $this->phone && '' !== $this->phone) {
            $html .= "<br/>
            {$this->phone}";
        }

        return $html;
    }

    public function getIsDefault()
    {
        return $this->isDefault;
    }

    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    public function getLat(): ?string
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = (string) $lat;

        return $this;
    }

    public function getLng(): ?string
    {
        return $this->lng;
    }

    public function setLng(float $lng): self
    {
        $this->lng = (string) $lng;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function fromArray(array $data): self
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key) && !in_array($key, ['id', 'user'])) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }
}
