<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\File;

#[ORM\Entity]
#[ORM\Table(name: 'shop_product_image')]
class ProductImage
{
    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'productImages')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private Product $product;

    #[ORM\Id]
    #[ORM\ManyToOne]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private File $image;

    #[ORM\Column]
    private int $position = 0;

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getImage(): File
    {
        return $this->image;
    }

    public function setImage(File $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position ?? 0;

        return $this;
    }
}
