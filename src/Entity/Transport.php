<?php

namespace Tigris\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\ShopBundle\Repository\TransportRepository;

#[ORM\Entity(repositoryClass: TransportRepository::class)]
#[ORM\Table(name: 'shop_transport')]
class Transport implements \Stringable
{
    final public const TYPE_SHIPPING = 'shipping';
    final public const TYPE_COLLECT = 'collect';

    final public const SERVICE_MONDIAL_RELAY = 'mondial_relay';
    final public const SERVICE_COLISSIMO = 'colissimo';
    final public const SERVICE_LAPOSTE = 'la_poste';

    final public const SERVICES_CHOICES = [
        'shop.transport.service.no_service' => '',
        'shop.transport.service.mondial_relay' => self::SERVICE_MONDIAL_RELAY,
        'shop.transport.service.colissimo' => self::SERVICE_COLISSIMO,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    #[Assert\NotBlank]
    private string $name = '';

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::JSON)]
    private array $countries = [];

    #[ORM\Column(type: Types::DECIMAL, scale: 3)]
    private float|string $maxWidth = 0.0;

    #[ORM\Column(type: Types::DECIMAL, scale: 3)]
    private float|string $maxHeight = 0.0;

    #[ORM\Column(type: Types::DECIMAL, scale: 3)]
    private float|string $maxDepth = '0.0';

    #[ORM\Column(type: Types::DECIMAL, scale: 3)]
    private float|string $maxWeight = '0.0';

    #[ORM\OneToMany(targetEntity: TransportSlot::class, mappedBy: 'transport', cascade: ['all'], orphanRemoval: true)]
    #[ORM\OrderBy(['price' => 'ASC'])]
    #[Assert\Valid]
    #[JMS\Exclude]
    private Collection $slots;

    #[ORM\Column(type: Types::DECIMAL, scale: 2)]
    private float|string $freeAtPrice = '0.0';

    #[JMS\Exclude]
    private ?Order $order = null;

    #[ORM\Column(type: Types::DECIMAL, scale: 2, nullable: true)]
    #[Assert\Range(min: 0, max: 999)]
    private null|float|string $maxDistance = null;

    #[ORM\Column(type: Types::DECIMAL, scale: 6, nullable: true)]
    private null|float|string $lat = null;

    #[ORM\Column(type: Types::DECIMAL, scale: 6, nullable: true)]
    private null|float|string $lng = null;

    #[ORM\Column(nullable: true)]
    private ?string $address = null;

    #[ORM\Column(type: Types::DECIMAL, scale: 2)]
    #[Assert\GreaterThanOrEqual(0)]
    private float|string $minPrice = '0';

    #[ORM\Column(length: 15)]
    private string $type = self::TYPE_SHIPPING;

    #[ORM\Column(length: 120)]
    private string $service = '';

    public function __construct()
    {
        $this->slots = new ArrayCollection();
    }

    public function __sleep()
    {
        return ['name', 'countries', 'maxWeight', 'maxWidth', 'maxDepth', 'slots', 'freeAtPrice', 'type', 'minPrice', 'service'];
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'countries' => $this->countries,
            'type' => $this->type,
            'freeAtPrice' => $this->freeAtPrice,
            'minPrice' => $this->minPrice,
            'maxWeight' => $this->maxWeight,
            'maxWidth' => $this->maxWidth,
            'maxDepth' => $this->maxDepth,
            'maxHeight' => $this->maxHeight,
            'service' => $this->service,
            'slots' => array_map(fn ($slot) => $slot->toArray(), $this->slots->toArray()),
        ];
    }

    public function fromArray(array $data): self
    {
        $this->name = $data['name'];
        $this->countries = $data['countries'];
        $this->type = $data['type'];
        $this->freeAtPrice = $data['freeAtPrice'];
        $this->minPrice = $data['minPrice'];
        $this->maxWeight = $data['maxWeight'];
        $this->maxWidth = $data['maxWidth'];
        $this->maxDepth = $data['maxDepth'];
        $this->maxHeight = $data['maxHeight'];
        $this->service = $data['service'];
        $this->slots = new ArrayCollection(array_map(fn ($slot) => (new TransportSlot())->fromArray($slot), $data['slots']));

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountries(): array
    {
        return $this->countries;
    }

    public function setCountries(array $countries): self
    {
        $this->countries = $countries;

        return $this;
    }

    public function getMaxWidth()
    {
        return $this->maxWidth;
    }

    public function setMaxWidth(float $maxWidth)
    {
        $this->maxWidth = $maxWidth;

        return $this;
    }

    public function getMaxHeight()
    {
        return $this->maxHeight;
    }

    public function setMaxHeight(float $maxHeight)
    {
        $this->maxHeight = $maxHeight;

        return $this;
    }

    public function getMaxDepth()
    {
        return $this->maxDepth;
    }

    public function setMaxDepth(float $maxDepth): self
    {
        $this->maxDepth = $maxDepth;

        return $this;
    }

    public function getMaxWeight(): float
    {
        return $this->maxWeight;
    }

    public function setMaxWeight(float $maxWeight): self
    {
        $this->maxWeight = $maxWeight;

        return $this;
    }

    public function getPrice(): float
    {
        if (!$this->order instanceof Order) {
            return 0;
        }
        $orderWeight = $this->order->getWeight();
        $orderPrice = $this->order->getPrice(false);

        // Free at price
        if ($this->freeAtPrice > 0 && $orderPrice >= $this->freeAtPrice) {
            return 0;
        }

        $price = 0;
        // Check for good slot
        foreach ($this->slots as $slot) {
            if ((float) $slot->getMaxWeight() >= $orderWeight) {
                $price = $slot->getPrice();

                break;
            }
        }

        // Add product added price (for fragile product for example)
        $price += $this->order->getDeliveryAddedPrice($price);

        return $price;
    }

    public function getSlots()
    {
        return $this->slots;
    }

    public function setSlots($slots)
    {
        $this->slots = $slots;

        return $this;
    }

    public function addSlot(TransportSlot $slot)
    {
        if (!$this->slots->contains($slot)) {
            $slot->setTransport($this);
            $this->slots[] = $slot;
            $this->maxWeight = max($this->maxWeight, $slot->getMaxWeight());
        }

        return $this;
    }

    public function removeSlot(TransportSlot $slot)
    {
        if ($this->slots->contains($slot)) {
            $this->slots->removeElement($slot);
            foreach ($this->slots as $slot) {
                $this->maxWeight = max($this->maxWeight, $slot->getMaxWeight());
            }
        }

        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    public function getFreeAtPrice()
    {
        return (float) $this->freeAtPrice;
    }

    public function setFreeAtPrice(float $freeAtPrice)
    {
        $this->freeAtPrice = (string) $freeAtPrice;

        return $this;
    }

    public function getMaxDistance(): ?float
    {
        return (float) $this->maxDistance;
    }

    public function setMaxDistance(?float $maxDistance): self
    {
        $this->maxDistance = (string) $maxDistance;

        return $this;
    }

    public function getLat()
    {
        return (float) $this->lat;
    }

    public function setLat(?float $lat)
    {
        $this->lat = (string) $lat;

        return $this;
    }

    public function getLng()
    {
        return (float) $this->lng;
    }

    public function setLng(?float $lng)
    {
        $this->lng = (string) $lng;

        return $this;
    }

    public function setAddress(?string $address)
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setMinPrice(?float $minPrice)
    {
        $this->minPrice = (string) $minPrice;

        return $this;
    }

    public function getMinPrice(): float
    {
        return (float) $this->minPrice;
    }

    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getService()
    {
        return $this->service;
    }

    public function setService(?string $service)
    {
        $this->service = $service ?? '';

        return $this;
    }
}
