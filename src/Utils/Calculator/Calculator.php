<?php

namespace Tigris\ShopBundle\Utils\Calculator;

use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Entity\Product;

final class Calculator
{
    public static function percent(float $price, float $percent): float
    {
        return round(($price * min($percent, 100)) / 100, 2, \PHP_ROUND_HALF_DOWN);
    }

    public static function discountProductValue(Discount $discount, Product $product, ?User $user = null): float
    {
        $productPrice = $product->getPrice();

        if (!$discount->isValid($product, $user)) {
            return 0;
        }

        return self::discountValue($discount, $productPrice);
    }

    public static function discountValue(Discount $discount, float $originalPrice): float
    {
        // Utilisation de la fonction min pour éviter des prix négatifs
        return match ($discount->getType()) {
            Discount::DISCOUNT_TYPE_PERCENT => self::percent($originalPrice, $discount->getValue()),
            Discount::DISCOUNT_TYPE_VALUE => min($originalPrice, $discount->getValue()),
            Discount::DISCOUNT_TYPE_NEW_PRICE => $originalPrice - min($originalPrice, $discount->getValue()),
            default => 0,
        };
    }

    public static function getAmountET(float $amountIT, float $taxValue): float
    {
        // PRIXTTC*100/(100+TVA)
        return round($amountIT, 2) * 100 / (100 + round($taxValue, 2));
    }

    public static function getTaxAmount(float $amountET, float $taxValue): float
    {
        return round($amountET, 2) * (round($taxValue, 2) / 100);
    }

    public static function getAmountIT(float $amountET, float $taxValue): float
    {
        return round($amountET, 2) + (round($amountET, 2) * round($taxValue, 2) / 100);
    }
}
