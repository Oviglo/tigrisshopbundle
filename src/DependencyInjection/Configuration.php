<?php

namespace Tigris\ShopBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('tigris_shop');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('payment_system')
                    ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                        ->scalarNode('client_id')->end()
                        ->scalarNode('client_secret')->end()
                        ->booleanNode('test')->end()
                        ->scalarNode('secret_key')->end()
                        ->scalarNode('security_key')->end()
                        ->scalarNode('company_code')->end()
                        ->scalarNode('ept_code')->end()
                        ->end()
                    ->end()
                ->end()

                ->booleanNode('enable_transport')->defaultTrue()->end()
                ->booleanNode('enable_gift_options')->defaultFalse()->end()
                ->scalarNode('mondial_relay_site_id')->defaultNull()->end()
                ->scalarNode('mondial_relay_site_key')->defaultNull()->end()
                ->scalarNode('la_poste_api_key')->defaultNull()->end()

                ->arrayNode('product_options')
                    ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('type')->end()
                            ->scalarNode('label')->end()
                            ->booleanNode('required')->defaultFalse()->end()
                            ->booleanNode('translatable')->defaultFalse()->end()
                            ->booleanNode('expanded')->end()
                            ->scalarNode('class')->end()
                            ->booleanNode('multiple')->end()
                            ->arrayNode('choices')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('value')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('options')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('value')
                                ->prototype('scalar')->end()
                            ->end()
                            ->arrayNode('attr')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('value')
                                ->prototype('scalar')->end()
                            ->end()

                        ->end()
                    ->end()
                ->end()

            ->end()
        ;

        return $treeBuilder;
    }
}
