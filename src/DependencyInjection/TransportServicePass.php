<?php

namespace Tigris\ShopBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Tigris\ShopBundle\TransportService\TransportServices;

class TransportServicePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(TransportServices::class)) {
            return;
        }

        $definition = $container->findDefinition(TransportServices::class);

        $taggedServices = $container->findTaggedServiceIds('tigris_shop.transport_service');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addService', [
                    new Reference($id),
                    $attributes['service'],
                ]);
            }
        }
    }
}
