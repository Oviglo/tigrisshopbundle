<?php

namespace Tigris\ShopBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class TigrisShopExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);
        $container->setParameter('tigris_shop', $config);
        $container->setParameter('tigris_shop.mondial_relay_site_id', $config['mondial_relay_site_id']);
        $container->setParameter('tigris_shop.mondial_relay_site_key', $config['mondial_relay_site_key']);
        $container->setParameter('tigris_shop.la_poste_api_key', $config['la_poste_api_key']);
        $container->setParameter('tigris_shop.enable_gift_options', $config['enable_gift_options']);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../../config/'));
        $loader->load('services.yaml');
    }
}
