<?php

namespace Tigris\ShopBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class MenuSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_ADMIN_MENU => 'onLoadAdminMenu',
        ];
    }

    public function onLoadAdminMenu(MenuEvent $event): void
    {
        $parent = $event->addChild('shop.menu.shop', [
            'uri' => '#shop',
        ]);

        $parent->addChild('shop.menu.products', [
            'route' => 'tigris_shop_admin_product_index',
        ]);

        $parent->addChild('shop.menu.discounts', [
            'route' => 'tigris_shop_admin_discount_index',
        ]);

        $parent->addChild('shop.menu.categories', [
            'route' => 'tigris_shop_admin_category_index',
        ]);

        $parent->addChild('shop.menu.taxes', [
            'route' => 'tigris_shop_admin_tax_index',
        ]);

        $parent->addChild('shop.menu.transports', [
            'route' => 'tigris_shop_admin_transport_index',
        ]);

        $parent->addChild('shop.menu.orders', [
            'route' => 'tigris_shop_admin_order_index',
        ]);
    }
}
