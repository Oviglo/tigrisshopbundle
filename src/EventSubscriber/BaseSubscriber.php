<?php

namespace Tigris\ShopBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Tigris\BaseBundle\Dashboard\DashboardCard;
use Tigris\BaseBundle\Dashboard\DashboardCardListItem;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\DashboardEvent;
use Tigris\BaseBundle\Event\Event;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Event\SearchEvent;
use Tigris\BaseBundle\Event\StatsEvent;
use Tigris\BaseBundle\Search\SearchResult;
use Tigris\BaseBundle\Utils\Stats;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\OrderStatus;
use Tigris\ShopBundle\Form\Type\ConfigType;
use Tigris\ShopBundle\Repository\BasketRepository;
use Tigris\ShopBundle\Repository\OrderRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

class BaseSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly BasketRepository $basketRepository,
        private readonly TokenStorageInterface $tokenStorage,
        private readonly ProductRepository $productRepository
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::AJAX_CRON => 'onAjaxCron',
            Events::LOAD_STATS => 'onLoadStats',
            Events::LOAD_CONFIGS => 'onLoadConfigs',
            Events::SEARCH => 'onSearch',
            Events::LOAD_ADMIN_DASHBOARD => 'onAdminDashboard',
        ];
    }

    public function onAjaxCron(Event $event): void
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $counter = 0;

        if ($user instanceof User) {
            $basket = $this->basketRepository->findOneByUser($user);
            if ($basket instanceof Basket) {
                $counter = $basket->getProductsQuantity();
            }
        }

        $event->setArgument('shopBasketCounter', $counter);
    }

    public function onLoadStats(StatsEvent $event): void
    {
        $event->addStats(new Stats('shop.menu.orders', 'tigris_shop_admin_order_chart'));
    }

    public function onLoadConfigs(FormEvent $event)
    {
        $form = $event->getForm();
        $form->add('TigrisShopBundle', ConfigType::class, [
            'label' => 'config.shop',
        ]);
    }

    public function onSearch(SearchEvent $event): void
    {
        $search = $event->getSearch();
        $options = $event->getOptions();

        if ('' === $search || (!in_array('all', $options) && !in_array('shop-product', $options))) {
            return;
        }
        $results = $this->productRepository->search($search);

        foreach ($results as $result) {
            $description = strip_tags((string) $result->getDescription());
            $description = substr($description, 0, 220).(strlen($description) > 220 ? '...' : '');
            $image = '';
            if (null !== $result->getImage()) {
                $image = $result->getImage()->getWebPath('thumbnail');
            }
            $event->addResult(new SearchResult($result->getName(), $description, 'tigris_shop_product_show', ['slug' => $result->getSlug()], $image));
        }
    }

    public function onAdminDashboard(DashboardEvent $event): void
    {
        /*$orderCount = $this->orderRepository->count(['status' => OrderStatus::COMPLETED]);

        $orderCard = (new DashboardCard('header'))
            ->setId('valid-orders')
            ->setTitle('dashboard.valid_orders')
            ->setRoute('tigris_shop_admin_order_index')
            ->setIcon('file-invoice')
            ->setValue($orderCount)
        ;
        $event->addCard($orderCard);

        $priceCard = (new DashboardCard('header'))
            ->setId('total-sale-price')
            ->setTitle('dashboard.total_sale_price')
            ->setRoute('tigris_shop_admin_order_index')
            ->setIcon('sack')
            ->setValue($this->orderRepository->getValidTotalPrice())
            ->setValueFormat(DashboardCard::VALUE_FORMAT_CURRENCY)
        ;
        $event->addCard($priceCard);

        $bestSellers = $this->productRepository->findData(['count' => 10, 'orders' => ['saleCount' => 'DESC', 'id' => 'DESC'], 'minSaleCount' => 1]);
        $bestSellerCard = (new DashboardCard('shop'))
            ->setId('best-sales-products')
            ->setTitle('dashboard.best_sale_products')
            ->setType(DashboardCard::TYPE_LIST)
        ;

        foreach ($bestSellers as $bestSeller) {
            $bestSellerCard->addListItem(new DashboardCardListItem($bestSeller->getName(), $bestSeller->getSaleCount()));
        }

        $event->addCard($bestSellerCard);*/
    }
}
