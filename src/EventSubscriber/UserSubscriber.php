<?php

namespace Tigris\ShopBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class UserSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_MY_ACCOUNT_MENU => 'onLoadMyAccountMenu',
        ];
    }

    public function onLoadMyAccountMenu(MenuEvent $event): void
    {
        $event->addChild('shop.menu.orders', ['route' => 'tigris_shop_order_index']);
    }
}
