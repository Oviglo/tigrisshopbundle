<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderStatus;

class OrderRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('oi, i, ps, ds, s')
            ->leftJoin('e.orderInvoices', 'oi')
            ->join('e.paymentStatus', 'ps')
            ->join('e.deliveryStatus', 'ds')
            ->join('e.status', 's')
            ->leftJoin('oi.invoice', 'i')
        ;
        if ('all' !== $criteria['status']) {
            if ('valid' === $criteria['status']) {
                $queryBuilder
                    ->andWhere('s.status != :status')
                    ->setParameter(':status', OrderStatus::CANCELED)
                ;
            } elseif ('canceled' === $criteria['status']) {
                $queryBuilder
                    ->andWhere('s.status = :status')
                    ->setParameter(':status', OrderStatus::CANCELED)
                ;
            } else {
                $queryBuilder
                    ->andWhere('s.status = :status')
                    ->setParameter(':status', $criteria['status'])
                ;
            }
        }

        if (isset($criteria['paymentStatus']) && 'all' != $criteria['paymentStatus']) {
            $queryBuilder
                ->andWhere('ps.status = :paymentStatus')
                ->setParameter(':paymentStatus', $criteria['paymentStatus'])
            ;
        }

        if (isset($criteria['deliveryStatus']) && 'all' != $criteria['deliveryStatus']) {
            $queryBuilder
                ->andWhere('ds.status = :deliveryStatus')
                ->setParameter(':deliveryStatus', $criteria['deliveryStatus'])
            ;
        }

        /*if (!empty($criteria['transport'])) {
            $queryBuilder
                ->andWhere('t.id = :transport')
                ->setParameter(':transport', $criteria['transport'])
            ;
        }*/

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function count(array $criteria = []): int
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('COUNT(e.id)')
            ->leftJoin('e.orderInvoices', 'oi')
            ->join('e.paymentStatus', 'ps')
            ->join('e.deliveryStatus', 'ds')
            ->join('e.status', 's')
            ->leftJoin('oi.invoice', 'i')
        ;
        if ('all' !== $criteria['status']) {
            if ('valid' === $criteria['status']) {
                $queryBuilder
                    ->andWhere('s.status != :status')
                    ->setParameter(':status', OrderStatus::CANCELED)
                ;
            } elseif ('canceled' === $criteria['status']) {
                $queryBuilder
                    ->andWhere('s.status = :status')
                    ->setParameter(':status', OrderStatus::CANCELED)
                ;
            } else {
                $queryBuilder
                    ->andWhere('s.status = :status')
                    ->setParameter(':status', $criteria['status'])
                ;
            }
        }

        if (isset($criteria['paymentStatus']) && 'all' != $criteria['paymentStatus']) {
            $queryBuilder
                ->andWhere('ps.status = :paymentStatus')
                ->setParameter(':paymentStatus', $criteria['paymentStatus'])
            ;
        }

        if (isset($criteria['deliveryStatus']) && 'all' != $criteria['deliveryStatus']) {
            $queryBuilder
                ->andWhere('ds.status = :deliveryStatus')
                ->setParameter(':deliveryStatus', $criteria['deliveryStatus'])
            ;
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function findByUser(User $user, array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('o')
            ->addSelect('oi, i, ic, ps, ds, s, d, op, p')
            ->leftJoin('o.orderInvoices', 'oi')
            ->leftJoin('oi.invoice', 'i')
            ->leftJoin('i.customer', 'ic')
            ->leftJoin('o.paymentStatus', 'ps')
            ->leftJoin('o.deliveryStatus', 'ds')
            ->leftJoin('o.statuses', 's')
            ->leftJoin('o.discounts', 'd')
            ->leftJoin('o.orderProducts', 'op')
            ->leftJoin('op.product', 'p')
            ->where('o.user = :user')
            ->setParameter(':user', $user)
        ;

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findLastByUser(User $user): ?Order
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameter(':user', $user)
            ->setMaxResults(1)
            ->orderBy('e.createdAt', 'desc')
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function getValidTotalPrice(): float
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('oi, i, ps, ds, s, op, p, od')
            ->leftJoin('e.orderInvoices', 'oi')
            ->join('e.paymentStatus', 'ps')
            ->join('e.deliveryStatus', 'ds')
            ->join('e.status', 's')
            ->join('e.orderProducts', 'op')
            ->leftJoin('e.discounts', 'od')
            ->leftJoin('op.product', 'p')
            ->leftJoin('oi.invoice', 'i')
            ->where('ps.status = :paymentStatus')
            ->setParameter(':paymentStatus', OrderStatus::PAID)
            ->andWhere('s.status != :status')
            ->setParameter(':status', OrderStatus::CANCELED)
        ;

        $result = $queryBuilder->getQuery()->getResult();

        $total = 0;
        foreach ($result as $order) {
            $total += $order->getPrice(false);
        }

        return $total;
    }

    public function findChartByDates(\DateTime $dateStart, \DateTime $dateEnd, $groupBy = null, $select = null)
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('count(e) as nb, SUBSTRING(e.createdAt, 1, 4) as year, SUBSTRING(e.createdAt, 6,2) as month')
            ->join('e.status', 's')
            ->where('e.createdAt >= :start')
            ->andWhere('e.createdAt < :end')
            ->setParameter(':start', $dateStart)
            ->setParameter(':end', $dateEnd)
            ->andWhere('s.status != :status')
            ->setParameter(':status', OrderStatus::CANCELED)
        ;

        if (null !== $select) {
            $queryBuilder->addSelect($select);
        }

        if (null !== $groupBy) {
            $queryBuilder->groupBy($groupBy);
        }

        $queryBuilder->addGroupBy('year, month')
            ->orderBy('year', 'ASC')
            ->addOrderBy('month', 'ASC')
        ;

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }
}
