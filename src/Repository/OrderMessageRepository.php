<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderMessage;

class OrderMessageRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderMessage::class);
    }

    public function findDataByOrder(Order $order, array $criteria): Paginator
    {
        $criteria['order'] = 'id';
        $criteria['rev'] = true;

        $queryBuilder = $this->createQueryBuilder('e');
        $this->addBasicCriteria($queryBuilder, $criteria);

        $queryBuilder->andWhere('e.order = :order')
            ->setParameter('order', $order)
        ;

        return new Paginator($queryBuilder, true);
    }
}
