<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Event\QueryEvent;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Event\ShopEvents;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class ProductRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry, private readonly EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct($registry, Product::class);
    }

    public function findData($criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('c, pi, i, if, pc, d, du, dp, dug, cd')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('c.parent', 'pc')
            ->leftJoin('e.productImages', 'pi')
            ->leftJoin('pi.image', 'i')
            ->leftJoin('i.folder', 'if')
            ->leftJoin('e.discounts', 'd')
            ->leftJoin('d.users', 'du')
            ->leftJoin('d.products', 'dp')
            ->leftJoin('d.userGroups', 'dug')
            ->leftJoin('c.discounts', 'cd')
        ;

        $event = new QueryEvent($queryBuilder, $criteria);
        $this->eventDispatcher->dispatch($event, ShopEvents::FILTER_REQUEST);
        $criteria = $event->getCriteria();

        if (isset($criteria['public']) && '' !== $criteria['public']) {
            $queryBuilder->andWhere('e.public = :public')
                ->setParameter(':public', $criteria['public'])
            ;

            unset($criteria['public']);
        }

        if (isset($criteria['search']) && !empty($criteria['search'])) {
            $queryBuilder->andWhere('(e.name LIKE :search OR e.reference LIKE :search)')
                ->setParameter(':search', '%'.trim((string) $criteria['search']).'%')
            ;

            unset($criteria['search']);
        }

        if (isset($criteria['minPrice'])) {
            $queryBuilder->andWhere('e.price >= :minPrice')
                ->setParameter(':minPrice', $criteria['minPrice'])
            ;
            unset($criteria['minPrice']);
        }

        if (isset($criteria['maxPrice'])) {
            $queryBuilder->andWhere('e.price <= :maxPrice')
                ->setParameter(':maxPrice', $criteria['maxPrice'])
            ;
            unset($criteria['maxPrice']);
        }

        if (isset($criteria['minSaleCount'])) {
            $queryBuilder->andWhere('e.saleCount >= :minSaleCount')
                ->setParameter(':minSaleCount', $criteria['minSaleCount'])
            ;
            unset($criteria['minSaleCount']);
        }

        if (isset($criteria['categories'])) {
            if (is_object($criteria['categories']) && method_exists($criteria['categories'], 'toArray')) {
                $criteria['categories'] = $criteria['categories']->toArray();
            }

            $criteria['categories'] = array_map(function ($value) {
                if ($value instanceof Category) {
                    return $value->getId();
                }

                return (int) $value;
            }, $criteria['categories']);

            // Delete value 0
            $zero = array_search(0, $criteria['categories']);
            if (false !== $zero) {
                unset($criteria['categories'][$zero]);
            }

            if ([] !== $criteria['categories']) {
                $queryBuilder
                    ->andWhere('c.id IN (:categories)')
                    ->setParameter(':categories', $criteria['categories'])
                ;
            }
        }

        // Parent category
        if (isset($criteria['root_category'])) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->orX('c = :rootCat', 'pc = :rootCat'))
                ->setParameter(':rootCat', $criteria['root_category'])
            ;
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder);
    }

    public function findMaxPrice(bool $public = true): float
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('MAX(e.price)')
            ->where('e.public = :public')
            ->setParameter('public', $public)
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getSingleScalarResult() ?? 0;
    }

    public function findMinPrice(bool $public = true): float
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('MIN(e.price)')
            ->where('e.public = :public')
            ->setParameter('public', $public)
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getSingleScalarResult() ?? 0;
    }

    public function getMinMaxPrice(bool $public = true): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->select('MIN(e.price) AS minPrice, MAX(e.price) AS maxPrice')
            ->where('e.public = :public')
            ->setParameter(':public', $public)
            ->setMaxResults(1)
        ;

        return $queryBuilder->getQuery()->getSingleResult();
    }

    public function findOneBySlug(string $slug)
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('c, pi, i')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.productImages', 'pi')
            ->leftJoin('pi.image', 'i')
            ->where('e.slug = :slug')
            ->setParameter(':slug', $slug)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function search($search)
    {
        $search = strtolower((string) $search);
        $queryBuilder = $this->createQueryBuilder('e');
        $queryBuilder->addSelect('c, pi, i')
            ->leftJoin('e.categories', 'c')
            ->leftJoin('e.productImages', 'pi')
            ->leftJoin('pi.image', 'i')
            ->where('e.public = true')
            ->andWhere($queryBuilder->expr()->like('LOWER(e.name)', ':search'))
            ->setParameter(':search', "%$search%")
            ->setMaxResults(20)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
