<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Discount;

class DiscountRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Discount::class);
    }

    public function findData(array $criteria): Paginator
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('u, g')
            ->leftJoin('d.users', 'u')
            ->leftJoin('d.userGroups', 'g');

        if (isset($criteria['search']) && !empty($criteria['search'])) {
            $qb->andWhere('(d.name LIKE :search OR d.code LIKE :search)')
                ->setParameter(':search', '%' . trim((string) $criteria['search']) . '%');

            unset($criteria['search']);
        }

        if (isset($criteria['itemType']) && $criteria['itemType'] !== '') {
            $qb->andWhere('d.itemType = :itemType')
                ->setParameter(':itemType', $criteria['itemType']);

            unset($criteria['itemType']);
        }

        if (isset($criteria['validity']) && $criteria['validity'] !== '') {
            switch ($criteria['validity']) {
                case 'valid':
                    $qb->andWhere($qb->expr()->orX('d.endDate > :now', 'd.endDate IS NULL'));
                    $qb->andWhere('d.startDate < :now');
                    break;
                case 'futur':
                    $qb->andWhere('d.startDate > :now');
                    break;
                case 'expired':
                    $qb->andWhere('d.endDate < :now');
                    break;
            }

            $qb->setParameter('now', new \DateTime());
            unset($criteria['validity']);
        }

        $this->addBasicCriteria($qb, $criteria);

        return new Paginator($qb->getQuery());
    }

    /**
     * @return array<Discount>
     */
    public function findValidByCode(string $code, User $user = null): array
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('u, g, cu')
            ->where('d.code = :code')
            ->andWhere('d.enabled = true')
            ->setParameter(':code', trim($code))
            ->leftJoin('d.users', 'u')
            ->leftJoin('d.codeUsers', 'cu')
            ->leftJoin('d.userGroups', 'g');

        if ($user instanceof User) {
            $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->andX('u = :user', 'SIZE(d.users) > 0'),
                        $qb->expr()->andX('g IN (:groups)', 'SIZE(d.userGroups) > 0'),
                        $qb->expr()->andX('SIZE(d.userGroups) = 0', 'SIZE(d.users) = 0') // No user and no groups (for all users)
                    )
                )
                ->setParameter(':user', $user)
                ->setParameter(':groups', $user->getGroups());
        }

        $now = new \DateTime();

        $qb
            ->andWhere('d.startDate <= :now')
            ->setParameter(':now', $now)
            ->andWhere($qb->expr()->orX('d.endDate IS NULL', 'd.endDate >= :now'));

        $qb->andWhere(
            $qb->expr()->orX(
                'd.useCountLimit = 0',
                'd.useCountLimit > d.useCount'
            )
        );

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<Discount>
     */
    public function findOrderDiscounts(bool $enabled = true): array
    {
        $qb = $this->createQueryBuilder('d')
            ->where('d.itemType = :itemType')
            ->setParameter(':itemType', Discount::ITEM_TYPE_ORDER);

        if ($enabled) {
            $qb->andWhere('d.enabled = true');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array<Discount>
     */
    public function findCodeDiscountsByUser(User $user): array
    {
        $qb = $this->createQueryBuilder('d')
            ->join('d.codeUsers', 'uc')
            ->where('uc.user = :user')
            ->setParameter(':user', $user);

        return $qb->getQuery()->getResult();
    }
}
