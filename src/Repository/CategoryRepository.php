<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Category;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class CategoryRepository extends NestedTreeRepository implements ServiceEntityRepositoryInterface
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        $manager = $registry->getManagerForClass(Category::class);

        parent::__construct($manager, $manager->getClassMetadata(Category::class));
    }

    public function findData(array $criteria = [])
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('i')
            ->leftJoin('e.image', 'i')
        ;
        $criteria['order'] = 'left';
        $criteria['rev'] = false;

        if (isset($criteria['public']) && $criteria['public']) {
            $queryBuilder->andWhere('e.public = :public')
                ->setParameter(':public', true)
            ;
        }

        if (isset($criteria['parent']) && $criteria['parent']) {
            $queryBuilder->andWhere('e.parent IS NULL');
        }

        $this->addBasicCriteria($queryBuilder, $criteria);
        $queryBuilder->addOrderBy('e.position', 'DESC');

        return new Paginator($queryBuilder, true);
    }

    public function getTree()
    {
        $queryBuilder = $this->getRootNodesQueryBuilder();
        $queryBuilder
            ->addOrderBy('node.position', 'ASC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
