<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Address;

class AddressRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Address::class);
    }

    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameter(':user', $user)
            ->getQuery()
            ->getResult()
        ;
    }
}
