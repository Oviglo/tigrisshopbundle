<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\ShopBundle\Entity\Basket;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class BasketRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Basket::class);
    }

    public function findData($criteria)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findOneByUser(User $user): ?Basket
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('bp', 'p')
            ->leftJoin('e.basketProducts', 'bp')
            ->leftJoin('bp.product', 'p')
            ->where('e.user = :user')
            ->setParameter(':user', $user)
            ->orderBy('p.name', 'ASC')
        ;

        return $queryBuilder->getquery()->getOneOrNullResult();
    }
}
