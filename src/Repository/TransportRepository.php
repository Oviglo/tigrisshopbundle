<?php

namespace Tigris\ShopBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Transport;

class TransportRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transport::class);
    }

    public function findData($criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findByWeightAndCountry(float $weight, string $country)
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $queryBuilder->where('(e.maxWeight >= :w OR e.maxWeight = 0)')
            ->setParameter(':w', $weight)
            ->andWhere($queryBuilder->expr()->like('e.countries', ':country'))
            ->setParameter(':country', "%{$country}%")
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByWeightAndAddress(float $weight, Address $address, $minPrice = null, $excludeTransports = [])
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $queryBuilder->where('(e.maxWeight >= :w OR e.maxWeight = 0)')
            ->setParameter(':w', $weight)
            ->andWhere($queryBuilder->expr()->like('e.countries', ':country'))
            ->setParameter(':country', "%{$address->getCountry()}%")
        ;

        if (null !== $minPrice && $minPrice > 0) {
            $queryBuilder
                ->andWhere('e.minPrice <= :minPrice')
                ->setParameter(':minPrice', $minPrice)
            ;
        }

        foreach ($excludeTransports as $key => $transport) {
            $queryBuilder
                ->andWhere("e.id != :transport$key")
                ->setParameter(":transport$key", $transport->getId())
            ;
        }

        $result = $queryBuilder->getQuery()->getResult();
        foreach ($result as $key => $transp) {
            if ($transp->getMaxDistance() > 0 && ($address->getLat() === null || $address->getLat() === '' || $address->getLat() === '0' ||
            ($address->getLng() === null || $address->getLng() === '' || $address->getLng() === '0') ||
            Utils::distance($address->getLat(), $address->getLng(), $transp->getLat(), $transp->getLng()) > $transp->getMaxDistance())) {
                unset($result[$key]);
            }
        }

        return $result;
    }
}
