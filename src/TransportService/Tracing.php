<?php

namespace Tigris\ShopBundle\TransportService;

class Tracing
{
    public array $events = [];

    public function addEvent(\DateTime $date, string $description): self
    {
        $this->events[] = [
            'date' => $date,
            'description' => $description,
        ];

        return $this;
    }
}
