<?php

namespace Tigris\ShopBundle\TransportService;

interface TransportServiceInterface
{
    public const TRACING_TYPE_NONE = 'none';
    public const TRACING_TYPE_API = 'api';
    public const TRACING_TYPE_URL = 'url';

    public function getName(): string;

    public function getTracingType(): string;

    public function getTracingUrl(string $tracingCode): string;

    public function getTracingInfos(string $tracingCode): ?Tracing;
}
