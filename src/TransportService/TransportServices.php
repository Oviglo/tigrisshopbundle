<?php

namespace Tigris\ShopBundle\TransportService;

class TransportServices
{
    private array $services = [];

    public function addService(TransportServiceInterface $service): self
    {
        $name = $service->getName();
        if (!isset($this->services[$name])) {
            $this->services[$name] = $service;
        }

        return $this;
    }

    public function getService($name): ?TransportServiceInterface
    {
        return $this->services[$name] ?? null;
    }
}
