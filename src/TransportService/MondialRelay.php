<?php

namespace Tigris\ShopBundle\TransportService;

use DansMaCulotte\MondialRelay\DeliveryChoice;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Transport;

class MondialRelay extends AbstractTransportService
{
    final public const TRACING_URL = 'https://www.mondialrelay.com/public/permanent/tracking.aspx';

    private readonly string $siteId;

    public function getName(): string
    {
        return Transport::SERVICE_MONDIAL_RELAY;
    }

    public function getRoute(Address $address)
    {
        return [
            'name' => 'tigris_shop_mondialrelay_chooserelay',
            'params' => [
                'postal' => $address->getPostal(),
                'city' => $address->getCity(),
                'country' => $address->getCountry(),
            ],
        ];
    }

    public function getTracingType(): string
    {
        return static::TRACING_TYPE_URL;
    }

    private readonly DeliveryChoice $deliveryChoice;

    public function __construct(string|null $siteId, string|null $siteKey)
    {
        if ($siteId === null || $siteId === '' || ($siteKey === null || $siteKey === '')) {
            return;
        }

        $this->siteId = $siteId;

        $this->deliveryChoice = new DeliveryChoice([
            'site_id' => $siteId,
            'site_key' => $siteKey,
        ]);
    }

    public function getDeliveryChoice(string $postal, string $city, string $country = 'FR'): array
    {
        return $this->deliveryChoice->findPickupPoints($country, $city, $postal);
    }

    public function getDeliveryPoint($pointId, $country = 'FR')
    {
        return $this->deliveryChoice->findPickupPointByCode($country, $pointId);
    }

    public function getTracingUrl(string $tracingCode): string
    {
        return self::TRACING_URL.'?'.http_build_query(['ens' => $this->siteId, 'exp' => $tracingCode]);
    }
}
