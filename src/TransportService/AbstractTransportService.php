<?php

namespace Tigris\ShopBundle\TransportService;

use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Transport;

abstract class AbstractTransportService implements TransportServiceInterface
{
    protected Transport $transport;

    public function getRoute(Address $address)
    {
        return null;
    }

    public function getTransport(): Transport
    {
        return $this->transport;
    }

    public function setTransport(Transport $transport): self
    {
        $this->transport = $transport;

        return $this;
    }

    public function getTracingUrl(string $tracingCode): string
    {
        return '';
    }

    public function getTracingInfos(string $tracingCode): ?Tracing
    {
        return null;
    }
}
