<?php

namespace Tigris\ShopBundle\TransportService;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Tigris\ShopBundle\Entity\Transport;

class Colissimo extends AbstractTransportService
{
    public function getName(): string
    {
        return Transport::SERVICE_COLISSIMO;
    }

    public function getTracingType(): string
    {
        return static::TRACING_TYPE_API;
    }

    /** @var string */
    final public const API_URL = 'https://api.laposte.fr';

    /** @var string */
    final public const SERVICE_URI = '/suivi/v2/idships/';

    private ?Client $httpClient = null;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ?string $apiKey = null
    ) {
        if (null !== $this->apiKey) {
            $this->httpClient = new Client(
                [
                    'base_uri' => self::API_URL,
                    'headers' => [
                        'X-Okapi-Key' => $this->apiKey,
                        'Accept' => 'application/json',
                    ],
                ]
            );
        }
    }

    public function getTracingInfos(string $tracingCode): ?Tracing
    {
        try {
            $response = $this->httpClient->request('GET', self::SERVICE_URI.$tracingCode);

            $body = json_decode((string) $response->getBody(), true, 512, JSON_THROW_ON_ERROR);

            $tracing = new Tracing();

            foreach ($body['shipment']['event'] as $event) {
                $tracing->addEvent(new \DateTime($event['date']), $event['label']);
            }

            return $tracing;
        } catch (\Exception $e) {
            $this->logger->error('[API LaPoste] '.$e->getMessage());
        }

        return null;
    }
}
