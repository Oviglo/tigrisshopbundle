<?php

namespace Tigris\ShopBundle\Event;

use Tigris\BaseBundle\Event\ViewEvent;
use Tigris\ShopBundle\Entity\Product;

class ProductEvent extends ViewEvent
{
    public function __construct(private Product $product)
    {
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
