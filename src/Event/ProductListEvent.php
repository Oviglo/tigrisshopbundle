<?php

namespace Tigris\ShopBundle\Event;

use Tigris\BaseBundle\Event\ViewEvent;

class ProductListEvent extends ViewEvent
{
    public function __construct(private array $products)
    {
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function setProducts(array $products): self
    {
        $this->products = $products;

        return $this;
    }
}
