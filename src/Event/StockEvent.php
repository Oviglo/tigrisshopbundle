<?php

namespace Tigris\ShopBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\ShopBundle\Entity\Product;

class StockEvent extends Event
{
    public function __construct(private Product $product, private readonly int $oldQantity)
    {
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getOldQuantity(): int
    {
        return $this->oldQantity;
    }
}
