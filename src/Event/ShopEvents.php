<?php

namespace Tigris\ShopBundle\Event;

class ShopEvents
{
    final public const ADD_TO_BASKET = 'shop.add_to_basket';
    final public const CHANGE_BASKET_QUANTITY = 'shop.basket_change_quantity';
    final public const PAYMENT_SUCCESS = 'shop.payment_success';
    final public const PAYMENT_FAILED = 'shop.payment_failed';
    final public const FILTER_REQUEST = 'shop.filter_request';
    final public const UPDATE_STOCK = 'shop.update_stock';
    final public const SHOW_PRODUCT = 'tigris_shop.show_product';
    final public const LOAD_PRODUCT_LIST = 'tigris_show.load_product_list';
}
