<?php

namespace Tigris\ShopBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\ShopBundle\Entity\Order;

class PaymentEvent extends Event
{
    public function __construct(private Order $order, private array $apiResponse)
    {
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getApiResponse(): array
    {
        return $this->apiResponse;
    }

    public function setApiResponse(array $apiResponse): self
    {
        $this->apiResponse = $apiResponse;

        return $this;
    }
}
