<?php

namespace Tigris\ShopBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\Product;

class BasketEvent extends Event
{
    public function __construct(private Basket $basket, private Product $product, private int $quantity)
    {
    }

    public function getBasket(): Basket
    {
        return $this->basket;
    }

    public function setBasket(Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
