<?php

namespace Tigris\ShopBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Repository\DiscountRepository;

class DiscountManager
{
    public function __construct(
        private readonly DiscountRepository $discountRepository,
        private readonly TokenStorageInterface $token,
        private readonly EntityManagerInterface $em
    ) {
    }

    public function canActivateCode(string $code, User $user = null): bool
    {
        $user = $this->getUser($user);

        $discounts = $this->discountRepository->findValidByCode($code, $user);

        return [] !== $discounts;
    }

    /**
     * @param array<Product>
     */
    public function canActivateDiscount(Discount $discount, array $products = []): bool
    {
        $discountProducts = $discount->getProducts();

        foreach ($products as $product) {
            if ($discountProducts->contains($product)) {
                return true;
            }
        }

        return false;
    }

    public function activateCode(string $code, User $user = null): bool
    {
        return $this->triggerCode($code, $user, true);
    }

    public function unactivateCode(string $code, User $user = null): bool
    {
        return $this->triggerCode($code, $user, false);
    }

    public function triggerCode(string $code, User $user = null, ?bool $enable = null): bool
    {
        $user = $this->getUser($user);
        $discounts = $this->discountRepository->findValidByCode($code, $user);

        if ([] === $discounts) {
            return false;
        }

        foreach ($discounts as $discount) {
            if (null === $enable) {
                $enable = !$discount->haveCodeUser($user);
            }

            if ($enable) {
                $discount->addCodeUser($user);
            } else {
                $discount->removeCodeUser($user);
            }

            foreach ($discount->getCodeUsers() as $userCode) {
                $this->em->persist($userCode);
            }
            $this->em->persist($discount);
        }

        $this->em->flush();

        return true;
    }

    public function applyDiscountOnOrder(Order $order): void
    {
        $discounts = $this->discountRepository->findOrderDiscounts();

        foreach ($discounts as $discount) {
            if ($discount->isOrderValid($order)) {
                $order->addDiscount($discount);
            }
        }
    }

    public function unactivateAllCodes(User $user): void
    {
        $discounts = $this->discountRepository->findCodeDiscountsByUser($user);
        foreach ($discounts as $discount) {
            $discount->removeCodeUser($user);
        }

        $this->em->flush();
    }

    private function getUser(User $user = null): User
    {
        if ($user instanceof User) {
            return $user;
        }

        $token = $this->token->getToken();

        if (!$token instanceof TokenInterface) {
            throw new \Exception('User needed to activate discount code', 1);
        }

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            throw new \Exception('User needed to activate discount code', 1);
        }

        return $user;
    }
}
