<?php

namespace Tigris\ShopBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Contracts\Geocoding\GeocodingInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\InvoiceBundle\Manager\InvoiceManager;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Event\ShopEvents;
use Tigris\ShopBundle\Event\StockEvent;
use Tigris\ShopBundle\Repository\AddressRepository;
use Tigris\ShopBundle\Repository\BasketRepository;
use Tigris\ShopBundle\Repository\OrderRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

class ShopManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ProductRepository $productRepository,
        private readonly BasketRepository $basketRepository,
        private readonly AddressRepository $addressRepository,
        private readonly OrderRepository $orderRepository,
        private readonly GeocodingInterface $geocoding,
        private readonly Security $security,
        private readonly InvoiceManager $invoiceManager,
        private readonly EventDispatcherInterface $eventDispatcher
    ) {
    }

    public function findProductData($criteria): Paginator
    {
        return $this->productRepository->findData($criteria);
    }

    public function saveProduct(Product $entity)
    {
        if (null === $entity->getId()) {
            $this->em->persist($entity);
        }

        $this->em->flush();
    }

    public function removeProduct(Product $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function getBasketByUser(User $user): ?Basket
    {
        $entity = $this->basketRepository->findOneByUser($user);
        if (null == $entity) {
            $entity = (new Basket())
                ->setUser($user);
        }

        foreach ($entity->getBasketProducts() as $basketProduct) {
            // Reset quantity
            if (Product::TYPE_STANDARD == $basketProduct->getProduct()->getType()) {
                $basketProduct->setQuantity(min($basketProduct->getQuantity(), $basketProduct->getProduct()->getQuantity()));
            }

            if (!$this->security->isGranted('buy', $basketProduct->getProduct())) {
                $basketProduct->setQuantity(0);
            }
        }

        $this->em->persist($entity);

        return $entity;
    }

    public function removeBasket(Basket $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function getAddressByUser(User $user)
    {
        return $this->addressRepository->findByUser($user);
    }

    public function saveAddress(Address $entity)
    {
        if ($entity->getLat() === null || $entity->getLat() === '' || $entity->getLat() === '0' || ($entity->getLng() === null || $entity->getLng() === '' || $entity->getLng() === '0')) {
            try {
                $geoResponse = $this->geocoding->searchAddress((string) $entity);
                if ($geoResponse->addresses !== []) {
                    $add = $geoResponse->addresses[0];
                    $entity->setLat($add->lat);
                    $entity->setLng($add->lng);
                }
            } catch (\Exception) {
            }
        }

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    public function findOrderData($criteria)
    {
        return $this->orderRepository->findData($criteria);
    }

    public function saveOrder(Order $entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function findOrderByUser(User $user, $criteria)
    {
        return $this->orderRepository->findByUser($user, $criteria);
    }

    public function findLastOrderByUser(User $user): ?Order
    {
        return $this->orderRepository->findLastByUser($user);
    }

    public function createInvoiceFromOrder(Order $order)
    {
        $invoice = $this->invoiceManager->create($order->getUser())
            ->setName((new \DateTime())->format('Ymdhi').' '.$order->getUser()->getUsername())
            ->setAmountIT($order->getPrice())
            // ->setAmountET($order->getPrice())
            ->setBillingAddress($order->getOrderAddress()->toHtml())
            ->setAuthor($order->getUser());
        // Update invoice customer full name
        $invoice->getCustomer()->setFullName($order->getOrderAddress()->getFullName());

        return $invoice;
    }

    public function updateStock(Order $order): void
    {
        foreach ($order->getOrderProducts() as $orderProduct) {
            $product = $orderProduct->getProduct();
            $oldQty = $product->getQuantity();
            if (Product::TYPE_STANDARD == $product->getType()) {
                $oldQty -= $orderProduct->getQuantity();
                $product->setQuantity($oldQty);
            }

            $product->addSale($orderProduct->getQuantity()); // Increment sale count

            $this->em->persist($product);

            $this->eventDispatcher->dispatch(new StockEvent($product, $oldQty), ShopEvents::UPDATE_STOCK);
        }

        $this->em->flush();
    }
}
