<?php

namespace Tigris\ShopBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\NotificationEvent;
use Tigris\BaseBundle\Manager\AbstractManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\ShopBundle\Entity\OrderMessage;
use Tigris\ShopBundle\Repository\OrderMessageRepository;

class OrderMessageManager extends AbstractManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly UserRepository $userRepository,
    ) {
    }

    public function add(OrderMessage $entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();

        $order = $entity->getOrder();
        $orderUser = $order->getUser();
        $author = $entity->getAuthor();

        // Author of message is order user => send notification to admins
        if ($orderUser instanceof User && $author->isEqualTo($orderUser)) {
            $adminUsers = $this->userRepository->findAdmins();
            foreach ($adminUsers as $adminUser) {
                $this->sendPostNotification($author, $adminUser, $entity);
            }
        } elseif ($orderUser instanceof User) {
            $this->sendPostNotification($author, $orderUser, $entity);
        }
    }

    private function sendPostNotification(User $author, User $user, OrderMessage $entity): void
    {
        $event = new NotificationEvent($author, $user, [
            'title' => 'shop.order_message.notification.title',
            'message' => 'shop.order_message.notification.content',
            'messageParams' => [],
            'buttons' => [
                [
                    'label' => 'shop.order_message.notification.btn',
                    'route' => 'tigris_shop_order_show',
                    'routeParams' => ['id' => $entity->getOrder()->getId()],
                ],
            ],
        ], $entity);

        $this->eventDispatcher->dispatch($event, Events::NOTIFY);
    }
}
