<?php

namespace Tigris\ShopBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tigris\ShopBundle\Repository\OrderRepository;

#[AsCommand(
    name: 'tigris:shop:generate-order-total-price',
    description: 'Generate total price for all orders.',
)]
class GenerateOrderTotalPriceCommand extends Command
{
    public function __construct(private readonly OrderRepository $orderRepository, private readonly EntityManagerInterface $entityManager)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Generate total price for all orders.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Generate total price for all orders...');

        $orders = $this->orderRepository->findAll();
        foreach ($orders as $order) {
            $totalPrice = $order->getPrice();
            $order->setTotalPrice($totalPrice);
            $this->entityManager->persist($order);
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
