<?php

namespace Tigris\ShopBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Repository\OrderDiscountRepository;
use Tigris\ShopBundle\Repository\OrderRepository;

#[AsCommand(name: 'tigris:shop:fix-database')]
class FixDatabaseCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly OrderDiscountRepository $orderDiscountRepository,
        private readonly OrderRepository $orderRepository
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->updateOrderDiscount($output);
        $this->updateOrder($output);

        return Command::SUCCESS;
    }

    private function updateOrderDiscount(OutputInterface $output): void
    {
        $conn = $this->em->getConnection();
        $query = "UPDATE shop_order_discount SET discount_data='[]'";
        $stmt = $conn->prepare($query);
        $stmt->executeQuery();

        $ids = $this->orderDiscountRepository->findIds();
        $output->writeln('<info>Found '.count($ids).' OrderDiscount</info>');

        foreach ($ids as $id) {
            try {
                $entity = $this->orderDiscountRepository->findOneBy(['id' => $id]);
                if (($discount = $entity->getDiscount()) instanceof Discount) {
                    $entity->setDiscountData($discount->toArray());
                }
            } catch (\Exception $e) {
                $output->writeln('<error>'.$e->getMessage().'</error>');

                continue;
            }
        }

        $this->em->flush();
    }

    private function updateOrder(OutputInterface $output): void
    {
        $conn = $this->em->getConnection();
        $query = "UPDATE shop_order SET transport_data='[]'";
        $stmt = $conn->prepare($query);
        $stmt->executeQuery();

        $ids = $this->orderRepository->findIds();
        $output->writeln('<info>Found '.count($ids).' Order</info>');

        // @var Order
        foreach ($ids as $id) {
            try {
                $entity = $this->orderRepository->findOneBy(['id' => $id]);
                if (($transport = $entity->getTransport()) instanceof Transport) {
                    $entity->setTransportData($transport->toArray());
                }
                if (($address = $entity->getOrderAddress()) instanceof Address) {
                    $entity->setOrderAddressData($address->toArray());
                }
                if (($address = $entity->getShippingAddress()) instanceof Address) {
                    $entity->setShippingAddressData($address->toArray());
                }
            } catch (\Exception $e) {
                $output->writeln('<error>'.$e->getMessage().'</error>');

                continue;
            }
        }

        $this->em->flush();
    }
}
