<?php

namespace Tigris\ShopBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'tigris:shop:update-database')]
class UpdateDatabaseCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->updateProducts($output);
        $this->updateOrders($output);
        $this->updateOrderProducts($output);

        $output->writeln('<info>Done !</info>');

        return Command::SUCCESS;
    }

    private function updateProducts(OutputInterface $output): void
    {
        $conn = $this->em->getConnection();
        $categoryQuery = 'SELECT * FROM shop_product';
        $stmt = $conn->prepare($categoryQuery);
        $queryResult = $stmt->executeQuery();
        $products = $queryResult->fetchAllAssociative();

        $output->writeln('<info>Found '.count($products).' product(s)</info>');

        $this->updateEnableMaxQuantity();
        $this->updateProductWeight();
        $this->updateProductHeight();
        $this->updateProductWidth();
        $this->updateProductDepth();
    }

    private function updateEnableMaxQuantity(): void
    {
        $conn = $this->em->getConnection();
        $categoryQuery = "UPDATE shop_product SET enable_max_quantity='0' WHERE enable_max_quantity IS NULL";
        $stmt = $conn->prepare($categoryQuery);
        $stmt->executeQuery();
    }

    private function updateProductWeight(): void
    {
        $conn = $this->em->getConnection();
        $categoryQuery = "UPDATE shop_product SET `weight`='0.00' WHERE `weight` IS NULL";
        $stmt = $conn->prepare($categoryQuery);
        $stmt->executeQuery();
    }

    private function updateProductHeight(): void
    {
        $conn = $this->em->getConnection();
        $categoryQuery = "UPDATE shop_product SET height='0.00' WHERE height IS NULL";
        $stmt = $conn->prepare($categoryQuery);
        $stmt->executeQuery();
    }

    private function updateProductWidth(): void
    {
        $conn = $this->em->getConnection();
        $categoryQuery = "UPDATE shop_product SET width='0.00' WHERE width IS NULL";
        $stmt = $conn->prepare($categoryQuery);
        $stmt->executeQuery();
    }

    private function updateProductDepth(): void
    {
        $conn = $this->em->getConnection();
        $categoryQuery = "UPDATE shop_product SET depth='0.00' WHERE depth IS NULL";
        $stmt = $conn->prepare($categoryQuery);
        $stmt->executeQuery();
    }

    private function updateOrders(OutputInterface $output): void
    {
        $conn = $this->em->getConnection();
        $query = 'SELECT * FROM shop_order';
        $stmt = $conn->prepare($query);
        $result = $stmt->executeQuery();
        $orders = $result->fetchAllAssociative();
        $output->writeln('<info>Found '.count($orders).' order(s)</info>');

        foreach ($orders as $order) {
            if ($output->isVerbose()) {
                $output->writeln('<comment>Order '.$order['id'].'</comment>');
            }

            if (empty($product['transport_service_data'])) {
                // $this->updateTransportServiceData((int) $order['id']);
            }
        }
    }

    private function updateOrderProducts(OutputInterface $output): void
    {
        $conn = $this->em->getConnection();
        $query = 'SELECT * FROM shop_order_product';
        $stmt = $conn->prepare($query);
        $result = $stmt->executeQuery();
        $results = $result->fetchAllAssociative();
        $output->writeln('<info>Found '.count($results).' order product(s)</info>');

        $this->updateOrderProductDiscounts();
    }

    private function updateOrderProductDiscounts(): void
    {
        $conn = $this->em->getConnection();
        $query = "UPDATE shop_order_product SET discounts='[]' WHERE discounts LIKE 'null'";
        $stmt = $conn->prepare($query);
        $stmt->executeQuery();
    }
}
