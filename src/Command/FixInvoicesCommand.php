<?php

namespace Tigris\ShopBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\ShopBundle\Entity\OrderStatus;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\Repository\OrderRepository;

#[AsCommand(name: 'tigris:shop:fix-invoice', description: 'Fix order without invoice')]
class FixInvoicesCommand extends Command
{
    public function __construct(private readonly OrderRepository $orderRepository, private readonly EntityManagerInterface $entityManager, private readonly ShopManager $shopManager)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $orders = $this->orderRepository->createQueryBuilder('o')
            ->where('SIZE(o.orderInvoices) = 0')
            ->join('o.paymentStatus', 'ps')
            ->andWhere('ps.status = :status')
            ->setParameter(':status', OrderStatus::PAID)
            ->getQuery()
            ->getResult()
        ;

        $orderCount = is_countable($orders) ? count($orders) : 0;

        foreach ($orders as $order) {
            $invoice = $this->shopManager->createInvoiceFromOrder($order);
            $invoice->setPaymentStatus(Invoice::STATUS_PAID);
            $invoice->setStatus(Invoice::STATUS_FINISHED);
            $this->entityManager->persist($invoice);
            $order->addInvoice($invoice);

            $this->entityManager->flush();

            if ($output->isVerbose()) {
                $invoiceNum = $invoice->getNumberStr();
                $output->writeln("<comment>Create invoice $invoiceNum</comment>");
            }
        }

        $output->writeln("<info>$orderCount invoice(s) created!</info>");

        return Command::SUCCESS;
    }
}
