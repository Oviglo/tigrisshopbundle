<?php

namespace Tigris\ShopBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Form\Type\AddressType;

#[IsGranted('ROLE_USER')]
#[Route(path: '/address')]
class AddressController extends BaseController
{
    use FormTrait;

    #[Route(path: '/new', options: ['expose' => true], methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        TranslatorInterface $translator,
        EntityManagerInterface $em
    ): Response {
        $entity = (new Address())
            ->setUser($this->getUser())
        ;

        $form = $this->createForm(AddressType::class, $entity, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('tigris_shop_address_new'),
        ]);
        $this->addFormActions($form);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('shop.address.add.success'),
                ]);
            }

            $this->addFlash('success', $translator->trans('shop.address.add.success'));

            return $this->redirectToRoute('tigris_shop_shop_index');
        }

        return $this->render('@TigrisShop/address/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}/remove', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => true])]
    public function remove(
        Request $request,
        Address $entity,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ): Response {
        $this->denyAccessUnlessGranted('edit', $entity);

        $form = $this
            ->createFormBuilder(
                null,
                [
                    'method' => 'DELETE',
                    'action' => $this->generateUrl('tigris_shop_address_remove', ['id' => $entity->getId()]),
                    'attr' => ['class' => 'no-ajax'],
                ]
            )
            ->getForm()
        ;
        $this->addFormActions($form, '');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('shop.address.delete.success'),
                ]);
            }

            $this->addFlash('success', $translator->trans('shop.address.delete.success'));

            return $this->redirectToRoute('tigris_shop_purchase_index');
        }

        return $this->render('@TigrisShop/address/remove.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
