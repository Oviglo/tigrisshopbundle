<?php

namespace Tigris\ShopBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\TransportService\TransportServiceInterface;
use Tigris\ShopBundle\TransportService\TransportServices;

#[Route(path: '/shop/transport')]
class TransportController extends BaseController
{
    #[Route(path: '/tracing/{id}', methods: ['GET'], requirements: ['id' => '\d+'], options: ['expose' => true])]
    #[IsGranted('ROLE_USER')]
    public function tracing(Order $order, TransportServices $transportServices): Response
    {
        $this->denyAccessUnlessGranted('view', $order);

        $transport = $order->getTransport();

        if (!$transport instanceof Transport) {
            throw new \Exception('There are no transport', 1);
        }

        $serviceName = $transport->getService();
        $service = $transportServices->getService($serviceName);

        if (!$service instanceof TransportServiceInterface) {
            return $this->render('@TigrisShop/transport/tracing.html.twig', [
                'order' => $order,
                'transport' => $transport,
                'tracing' => [],
            ]);
        }

        if ($service->getTracingType() === TransportServiceInterface::TRACING_TYPE_URL) {
            return $this->redirect($service->getTracingUrl($order->getTrackingNumber()));
        }

        $tracing = $service->getTracingInfos($order->getTrackingNumber());

        return $this->render('@TigrisShop/transport/tracing.html.twig', [
            'order' => $order,
            'transport' => $transport,
            'tracing' => $tracing,
        ]);
    }
}
