<?php

namespace Tigris\ShopBundle\Controller;

use Tigris\ShopBundle\Entity\Transport;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\TransportService\TransportServices;

#[Route(path: '/shop/order')]
#[IsGranted('ROLE_USER')]
class OrderController extends BaseController
{
    #[Route(path: '/list.{_format}', defaults: ['_format' => 'html'], options: ['expose' => true])]
    public function index(Request $request, ShopManager $shopManager, $_format): Response
    {
        if ('json' == $_format) {
            $criteria = $this->getCriteria($request);

            $entities = $shopManager->findOrderByUser($this->getUser(), $criteria);

            return $this->paginatorToJsonResponse($entities);
        }

        return $this->render('@TigrisShop/order/index.html.twig');
    }

    #[Route(path: '/{id}', options: ['expose' => true])]
    public function show(Order $entity, TransportServices $transportServices): Response
    {
        $this->denyAccessUnlessGranted('view', $entity);

        $transport = $entity->getTransport();

        $transportService = null;
        if ($transport instanceof Transport) {
            $serviceName = $transport->getService();
            $transportService = $transportServices->getService($serviceName);
        }

        return $this->render(
            '@TigrisShop/order/show.html.twig',
            ['entity' => $entity, 'transportService' => $transportService]
        );
    }
}
