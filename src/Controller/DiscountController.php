<?php

namespace Tigris\ShopBundle\Controller;

use Tigris\ShopBundle\Entity\Basket;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Manager\DiscountManager;
use Tigris\ShopBundle\Manager\ShopManager;

#[Route(path: 'shop/discount')]
class DiscountController extends BaseController
{
    #[Route(path: '/check-code/{code}', methods: ['GET'], options: ['expose' => true])]
    #[IsGranted('ROLE_USER')]
    public function checkCode(
        string $code,
        DiscountManager $discountManager,
        ShopManager $shopManager,
        TranslatorInterface $translator
    ): JsonResponse
    {
        $basket = $shopManager->getBasketByUser($this->getUser());
        $reason = $translator->trans('shop.discount.unvalid_code');
        if (!$basket instanceof Basket) {
            return new JsonResponse(['result' => false, 'reason' => '']);
        }

        $canValid = $discountManager->canActivateCode($code, $this->getUser());

        if ($canValid) {
            $reason = $translator->trans('shop.discount.valid_code');
            $discountManager->activateCode($code, $this->getUser());
        }

        return new JsonResponse(['result' => $canValid, 'reason' => $reason]);
    }
}
