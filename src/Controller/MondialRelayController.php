<?php

namespace Tigris\ShopBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Form\Type\MondialRelayType;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\TransportService\MondialRelay;

#[Route('/shop/mondial-relay')]
#[IsGranted('ROLE_USER')]
class MondialRelayController extends BaseController
{
    #[Route('/choose', methods: ['GET', 'POST'])]
    public function chooseRelay(
        Request $request,
        MondialRelay $mondialRelay,
        ShopManager $shopManager,
        EntityManagerInterface $entityManager
    ): Response
    {
        $postal = $request->get('postal', null);
        $city = $request->get('city', null);
        $country = $request->get('country', 'FR');
        $basket = $shopManager->getBasketByUser($this->getUser());
        $order = $basket->getOrder();

        $data = [
            'postal_city' => ['postal' => $postal, 'city' => $city, 'country' => $country],
        ];

        $form = $this->createForm(
            MondialRelayType::class,
            $data,
            ['method' => 'POST', 'action' => $this->generateUrl('tigris_shop_mondialrelay_chooserelay', 
            ['postal' => $postal, 'city' => $city, 'country' => $country])]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $point = $mondialRelay->getDeliveryPoint($data['relay'], $country);
            $order->setTransportServiceData(get_object_vars($point));

            $entityManager->persist($order);
            $entityManager->flush();

            return $this->redirectToRoute('tigris_shop_purchase_summary');
        }

        return $this->render('@TigrisShop/purchase/transport/mondial_relay.html.twig', [
            'address' => $order->getShippingAddress(),
            'form' => $form,
        ]);
    }

    #[Route('/search', methods: ['GET'], options: ['expose' => true])]
    public function search(Request $request, MondialRelay $mondialRelay): JsonResponse
    {
        $postal = $request->get('postal', null);
        $city = $request->get('city', null);
        $country = $request->get('country', 'FR');
        $results = $mondialRelay->getDeliveryChoice($postal, $city, $country);

        return new JsonResponse(['status' => 'success', 'message' => '', 'results' => $results]);
    }
}
