<?php

namespace Tigris\ShopBundle\Controller;

use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Event\ProductEvent;
use Tigris\ShopBundle\Event\ShopEvents;
use Tigris\ShopBundle\Repository\ProductRepository;

#[Route(path: '/shop/product')]
class ProductController extends BaseController
{
    #[Route(path: '/data.{_format}', defaults: ['_format' => 'json'], methods: ['GET'], options: ['expose' => true])]
    public function data(Request $request, ProductRepository $productRepository): Response
    {
        $criteria = $this->getCriteria($request);
        $criteria['categories'] = explode(',', (string) $request->get('categories', ''));
        $criteria['search'] = $request->get('search', $request->get('s', ''));
        $criteria['public'] = 1;
        $data = $productRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/{slug}', requirements: ['slug' => '[a-zA-Z0-9-]+'])]
    public function show(
        #[MapEntity(expr: 'repository.findOneBySlug(slug)')]
        Product $entity,
        EventDispatcherInterface $eventDispatcher
    ): Response {
        $this->generateBreadcrumbs([
            'shop.menu.eshop' => ['route' => 'tigris_shop_shop_index'],
            $entity->getName() => ['route' => 'tigris_shop_product_show', 'params' => ['slug' => $entity->getSlug()]],
        ]);

        $this->denyAccessUnlessGranted('view', $entity);

        $event = new ProductEvent($entity);
        $eventDispatcher->dispatch($event, ShopEvents::SHOW_PRODUCT);

        return $this->render('@TigrisShop/product/show.html.twig', \array_merge([
            'entity' => $entity,
        ], $event->getViewParams()));
    }
}
