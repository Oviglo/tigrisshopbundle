<?php

namespace Tigris\ShopBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Event\BasketEvent;
use Tigris\ShopBundle\Event\ShopEvents;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\Repository\TaxRepository;
use Tigris\ShopBundle\Security\BasketProductVoter;
use Tigris\ShopBundle\Security\Voter\ProductVoter;

#[IsGranted('ROLE_USER')]
#[Route(path: '/shop/basket')]
class BasketController extends BaseController
{
    #[Route(path: '/{id}/add')]
    public function add(
        Request $request,
        Product $product,
        ShopManager $shopManager,
        TranslatorInterface $translator,
        EventDispatcherInterface $eventDispatcher,
        EntityManagerInterface $em
    ): Response {
        if (!$this->isGranted(ProductVoter::BUY, $product)) {
            $this->addFlash('error', 'shop.basket.error.cannot_buy');

            return $this->redirectToRoute('tigris_shop_basket_index');
        }

        $user = $this->getUser();
        if ($user instanceof User) {
            $quantity = $request->query->get('quantity', 1);
            $basket = $shopManager->getBasketByUser($user);
            $basket->addProduct($product, $quantity);
            // Test if user can add product (quantity)
            if (
                null != $basket->getBasketProduct($product)
                && !$this->isGranted(BasketProductVoter::ADD_PRODUCT, $basket->getBasketProduct($product))
            ) {
                $this->addFlash('error', 'shop.basket.error.cannot_add_to_basket');

                return $this->redirectToRoute('tigris_shop_basket_index');
            }

            $event = new BasketEvent($basket, $product, 1);
            $eventDispatcher->dispatch($event, ShopEvents::ADD_TO_BASKET);

            $em->persist($basket);
            $em->flush();
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['status' => 'success', 'message' => 'shop.basket.product_added']);
        }

        $this->addFlash('success', 'shop.basket.product_added');

        return $this->redirectToRoute('tigris_shop_basket_index');
    }

    #[Route(path: '/', options: ['expose' => true])]
    public function index(ShopManager $shopManager, ConfigManager $configManager): Response
    {
        $user = $this->getUser();
        $basket = null;
        if ($user instanceof User) {
            $basket = $shopManager->getBasketByUser($user);
        }

        return $this->render('@TigrisShop/basket/index.html.twig', [
            'basket' => $basket,
            'giftOptions' => $configManager->getGroupValue('TigrisShopBundle.giftoptions'),
        ]);
    }

    #[Route(path: '/{id}/change-quantity', methods: ['POST'])]
    public function changeQuantity(
        Request $request,
        Product $product,
        EntityManagerInterface $entityManager,
        ShopManager $shopManager,
        EventDispatcherInterface $eventDispatcher
    ): Response {
        if ($this->isCsrfTokenValid('change-quantity'.$product->getId(), $request->request->get('_token'))) {
            $user = $this->getUser();
            $basket = null;
            if ($user instanceof User) {
                $quantity = (int) $request->request->get('quantity');
                $basket = $shopManager->getBasketByUser($user);
                $basket->changeQuantity($product, $quantity);

                // Test if user can add product (quantity)
                if (
                    !$this->isGranted(BasketProductVoter::ADD_PRODUCT, $basket->getBasketProduct($product))
                    && $quantity > 0
                ) {
                    $this->addFlash('error', 'shop.basket.error.cannot_add_to_basket');

                    return $this->redirectToRoute('tigris_shop_basket_index');
                }

                $entityManager->persist($basket);
                $entityManager->flush();

                $event = new BasketEvent($basket, $product, $quantity);
                $eventDispatcher->dispatch($event, ShopEvents::CHANGE_BASKET_QUANTITY);

                if ($quantity > 0) {
                    $this->addFlash('success', 'shop.basket.change_quantity.success');
                } else {
                    $this->addFlash('success', 'shop.basket.delete.success');
                }
            }
        } else {
            // TODO: show error flash message
        }

        return $this->redirectToRoute('tigris_shop_basket_index');
    }

    #[Route(path: '/{id}/gift-wrapping-option', requirements: ['id' => '\d+'], methods: ['POST'], options: ['expose' => true])]
    public function giftWrappingOption(
        Product $product,
        ConfigManager $configManager,
        Request $request,
        ShopManager $shopManager,
        TaxRepository $taxRepository,
        EntityManagerInterface $em
    ): JsonResponse {
        if (!$this->getParameter('tigris_shop.enable_gift_options')) {
            return new JsonResponse(['status' => 'error', 'message' => 'option not evailable']);
        }

        if ($this->isCsrfTokenValid('gift-wrapping-option-'.$product->getId(), $request->request->get('_token'))) {
            $user = $this->getUser();
            $basket = null;
            if ($user instanceof User) {
                $basket = $shopManager->getBasketByUser($user);

                $giftOptions = $configManager->getGroupValue('TigrisShopBundle.giftoptions');
                $giftTax = null;
                if ($giftOptions['tax']) {
                    $giftTax = $taxRepository->findOneById((int) $giftOptions['tax']);
                }
                $giftPrice = (float) $giftOptions['price'] ?? 0;
                $basket->changeGiftWrappingOption($product, (bool) $request->request->get('enable'), $giftPrice, $giftTax);

                $em->flush();

                return new JsonResponse(['status' => 'success', 'message' => '']);
            }
        }

        return new JsonResponse(['status' => 'error', 'message' => '']);
    }
}
