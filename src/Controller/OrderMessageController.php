<?php

namespace Tigris\ShopBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderMessage;
use Tigris\ShopBundle\Manager\OrderMessageManager;
use Tigris\ShopBundle\Repository\OrderMessageRepository;

#[IsGranted('ROLE_USER')]
#[Route('/shop/order/message')]
class OrderMessageController extends BaseController
{
    use FormTrait;

    #[Route('/{id<\d+>}/data', options: ['expose' => true], methods: ['GET'])]
    public function data(Order $order, Request $request, OrderMessageRepository $orderMessageRepository): JsonResponse
    {
        $this->denyAccessUnlessGranted('view', $order);

        $criteria = $this->getCriteria($request);

        $entities = $orderMessageRepository->findDataByOrder($order, $criteria);

        return $this->paginatorToJsonResponse($entities);
    }

    #[Route('/{id<\d+>}/new', options: ['expose' => true], methods: ['POST'])]
    public function new(
        Order $order,
        Request $request,
        OrderMessageManager $orderMessageManager,
        TranslatorInterface $translator
    ): Response {
        $this->denyAccessUnlessGranted('view', $order);

        $entity = (new OrderMessage())
            ->setOrder($order)
            ->setAuthor($this->getUser())
            ->setContent($request->get('content', ''))
        ;

        if ($this->isCsrfTokenValid('order-message-new', $request->get('_token'))) {
            $orderMessageManager->add($entity);

            return new JsonResponse([
                'status' => 'success',
                'message' => $translator->trans('shop.order_message.add.success'),
            ]);
        }

        return new JsonResponse([
            'status' => 'error',
            'message' => $translator->trans('shop.order_message.add.error'),
        ]);
    }

    #[Route('/{id<\d+>}/remove', options: ['expose' => true], methods: ['GET', 'DELETE'])]
    public function remove(
        OrderMessage $entity,
        Request $request,
        TranslatorInterface $translator,
        EntityManagerInterface $em
    ): Response {
        $this->denyAccessUnlessGranted('view', $entity->getOrder());

        $form = $this->createFormBuilder($entity, [
            'method' => Request::METHOD_DELETE,
            'action' => $this->generateUrl('tigris_shop_ordermessage_remove', ['id' => $entity->getId()]),
        ])->getForm();

        $this->addFormActions(
            $form,
            $this->generateUrl('tigris_shop_order_show', ['id' => $entity->getOrder()->getId()])
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            return new JsonResponse([
                'status' => 'success',
                'message' => $translator->trans('show.order_message.delete.success'),
            ]);
        }

        return $this->render('@TigrisShop/order_message/remove.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
