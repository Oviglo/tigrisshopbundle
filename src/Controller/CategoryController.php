<?php

namespace Tigris\ShopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Repository\CategoryRepository;

#[Route(path: 'shop/category')]
class CategoryController extends BaseController
{
    public function _list(
        Request $request,
        CategoryRepository $categoryRepository,
        int $count = 10,
        bool $parent = true
    ): Response
    {
        $criteria = $this->getCriteria($request);
        $criteria['public'] = true;
        $criteria['count'] = $count;
        $criteria['parent'] = $parent;
        $entities = $categoryRepository->findData($criteria);

        return $this->render('@TigrisShop/category/_list.html.twig', [
            'entities' => $entities,
        ]);
    }
}
