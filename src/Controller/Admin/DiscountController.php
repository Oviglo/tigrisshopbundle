<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Form\Type\DiscountType;
use Tigris\ShopBundle\Repository\DiscountRepository;

#[Route('/admin/shop/discount')]
#[IsGranted('ROLE_ADMIN')]
class DiscountController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'shop.menu.shop' => null,
            'shop.menu.discounts' => ['route' => 'tigris_shop_admin_discount_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisShop/admin/discount/index.html.twig');
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => 'admin'])]
    public function data(Request $request, DiscountRepository $discountRepository): JsonResponse
    {
        $criteria = $this->getCriteria($request);
        $criteria['search'] = $request->get('search');
        $criteria['itemType'] = $request->get('itemType', '');
        $criteria['validity'] = $request->get('validity', '');
        $data = $discountRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', methods: ['GET', 'POST'], options: ['expose' => 'admin'])]
    public function new(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $entity = new Discount();

        $form = $this->createForm(DiscountType::class, $entity, [
            'method' => 'POST',
            'action' => $this->generateUrl('tigris_shop_admin_discount_new'),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_discount_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.discount.add.success')]);
            }

            $this->addFlash('success', $translator->trans('shop.discount.add.success'));

            return $this->redirectToRoute('tigris_shop_admin_discount_index');
        }

        return $this->render('@TigrisShop/admin/discount/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function edit(Request $request, Discount $entity, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $form = $this->createForm(DiscountType::class, $entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_shop_admin_discount_edit', ['id' => $entity->getId()]),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_discount_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.discount.edit.success')]);
            }

            $this->addFlash('success', $translator->trans('shop.discount.edit.success'));

            return $this->redirectToRoute('tigris_shop_admin_discount_index');
        }

        return $this->render('@TigrisShop/admin/discount/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function remove(Request $request, Discount $entity, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $form = $this->createFormBuilder(
            $entity,
            [
                'method' => 'DELETE',
                'action' => $this->generateUrl('tigris_shop_admin_discount_remove', ['id' => $entity->getId()]),
            ]
        )->getForm();

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_discount_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.discount.remove.success')]);
            }

            $this->addFlash('success', $translator->trans('shop.discount.remove.success'));

            return $this->redirectToRoute('tigris_shop_admin_discount_index');
        }

        return $this->render('@TigrisShop/admin/discount/remove.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/print-code', requirements: ['id' => '\d+'], methods: ['GET'], options: ['expose' => 'admin'])]
    public function printCode(Discount $entity): Response
    {
        $dompdf = new Dompdf(['enable_remote' => true]);
        $dompdf->loadHtml($this->renderView('@TigrisShop/admin/discount/print_code.pdf.twig', [
            'entity' => $entity,
        ]));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        // $dompdf->stream($entity->getName().'.pdf');

        return new Response(content: $dompdf->output(), headers: [
            'Content-Type' => 'application/pdf',
        ]);
    }
}
