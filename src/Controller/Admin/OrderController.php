<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Controller\ChartControllerTrait;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Notification\Notification;
use Tigris\BaseBundle\Notification\NotificationButton;
use Tigris\BaseBundle\Notification\NotificationData;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderStatus;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Form\Type\ShippType;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\Repository\BasketRepository;
use Tigris\ShopBundle\Repository\OrderRepository;
use Tigris\ShopBundle\Repository\TransportRepository;
use Tigris\ShopBundle\TransportService\TransportServices;

#[Route('/admin/shop/order')]
#[IsGranted('ROLE_ADMIN')]
class OrderController extends BaseController
{
    use ChartControllerTrait;
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'shop.menu.shop' => null,
            'shop.menu.orders' => ['route' => 'tigris_shop_admin_order_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(TransportRepository $transportRepository): Response
    {
        $this->generateBreadcrumbs();

        $transports = [];
        if ($this->getParameter('tigris_shop')['enable_transport']) {
            $transports = $transportRepository->findAll();
        }

        return $this->render('@TigrisShop/admin/order/index.html.twig', [
            'transports' => $transports,
        ]);
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => 'admin'])]
    public function data(ShopManager $shopManager, Request $request): Response
    {
        $criteria = $this->getCriteria($request);
        $criteria['status'] = $request->query->get('status');
        $criteria['paymentStatus'] = $request->query->get('paymentStatus');
        $criteria['deliveryStatus'] = $request->query->get('deliveryStatus');
        $criteria['transport'] = $request->query->get('transport', null);

        if ('true' == $request->query->get('showCanceled', false)) {
            $criteria['status'] = 'canceled';
        }

        $data = $shopManager->findOrderData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/{id}/show', requirements: ['id' => '\d+'], methods: ['GET'], options: ['expose' => 'admin'])]
    public function show(Order $entity, TransportServices $transportServices): Response
    {
        $this->generateBreadcrumbs([
            'shop.order.order' => ['route' => 'tigris_shop_admin_order_show', 'params' => ['id' => $entity->getId()]],
        ]);

        $transport = $entity->getTransport();

        $transportService = null;
        if ($transport instanceof Transport) {
            $serviceName = $transport->getService();
            $transportService = $transportServices->getService($serviceName);
        }

        return $this->render('@TigrisShop/admin/order/show.html.twig', ['entity' => $entity, 'transportService' => $transportService]);
    }

    #[Route('/chart', methods: ['GET'], options: ['expose' => 'admin'])]
    public function chart(Request $request, TranslatorInterface $translator, OrderRepository $orderRepository): JsonResponse
    {
        $criteria = $this->getDateCriteria($request);

        $board = $this->getStatsBoard(
            $criteria['startDate'],
            $criteria['endDate'],
            false
        );
        $labels = $board['labels'];
        $stats = $board['stats'];
        $dateStart = $board['dates']['start'];
        $dateEnd = $board['dates']['end'];

        $entities = $orderRepository->findChartByDates($dateStart, $dateEnd);

        foreach ($entities as $entity) {
            $stats[(int) $entity['year'].'-'.(int) $entity['month']] = (int) $entity['nb'];
        }

        return new JsonResponse([
            'labels' => $labels,
            'stats' => [
                [
                'label' => $translator->trans('shop.order.orders'),
                'data' => array_values($stats),
                ],
            ],
        ]);
    }

    #[Route('/{id}/shipp', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function shipp(
        Request $request,
        Order $entity,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        ConfigManager $configManager,
        MailerInterface $mailer,
        Notification $notification
    ): Response {
        $this->denyAccessUnlessGranted('edit', $entity);

        $form = $this->createForm(ShippType::class, $entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_shop_admin_order_shipp', ['id' => $entity->getId()]),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_order_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setDeliveryStatus(OrderStatus::SHIPPED);
            $entityManager->flush();

            // Notification
            $notificationData = (new NotificationData())
                ->setTitle('email.shipped.title')
                ->setMessage('email.shipped.content')
                ->setMessageParams(['%n%' => str_pad($entity->getId(), 5, '0')])
                ->addButton(new NotificationButton('email.shipped.button', 'tigris_shop_order_show', ['id' => $entity->getId()]))
            ;
            $notification->send($entity->getUser(), $notificationData, null, $entity, false);

            // Emails
            try {
                $siteTitle = $configManager->getvalue('TigrisBaseBundle.title', '');
                $notificationMail = $configManager->getvalue('TigrisBaseBundle.noreply_mail', '');
                $email = (new TemplatedEmail())
                    ->from(new Address($notificationMail, $siteTitle))
                    ->to(new Address($entity->getUser()->getEmail()))
                    ->subject($translator->trans('email.shipped.title'))
                    ->htmlTemplate('@TigrisShop/email/shipped.html.twig')
                    ->context([
                        'entity' => $entity,
                    ])
                ;
                $mailer->send($email);
            } catch (\Exception) {
            }

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisShop/admin/order/shipp.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/available', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function available(
        Request $request,
        Order $entity,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        ConfigManager $configManager,
        MailerInterface $mailer,
        Notification $notification
    ): Response {
        $this->denyAccessUnlessGranted('edit', $entity);

        $form = $this->createFormBuilder($entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_shop_admin_order_available', ['id' => $entity->getId()]),
        ])->getForm();
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_order_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setDeliveryStatus(OrderStatus::AVAILABLE);
            $entityManager->flush();

            // Notification
            $notificationData = (new NotificationData())
                ->setTitle('email.available.title')
                ->setMessage('email.available.content')
                ->setMessageParams(['%n%' => str_pad($entity->getId(), 5, '0')])
                ->addButton(new NotificationButton('email.available.button', 'tigris_shop_order_show', ['id' => $entity->getId()]))
            ;
            $notification->send($entity->getUser(), $notificationData, null, $entity, false);

            // Emails
            try {
                $siteTitle = $configManager->getvalue('TigrisBaseBundle.title', '');
                $notificationMail = $configManager->getvalue('TigrisBaseBundle.noreply_mail', '');
                $email = (new TemplatedEmail())
                    ->from(new Address($notificationMail, $siteTitle))
                    ->to(new Address($entity->getUser()->getEmail()))
                    ->subject($translator->trans('email.available.title'))
                    ->htmlTemplate('@TigrisShop/email/available.html.twig')
                    ->context([
                        'entity' => $entity,
                        'config' => $configManager->getGroupValue('TigrisShopBundle.clickcollect'),
                    ])
                ;
                $mailer->send($email);
            } catch (\Exception) {
            }

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisShop/admin/order/available.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/paid', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function paid(Request $request, Order $entity, ShopManager $shopManager, EntityManagerInterface $entityManager)
    {
        $this->denyAccessUnlessGranted('edit', $entity);

        $form = $this->createFormBuilder($entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_shop_admin_order_paid', ['id' => $entity->getId()]),
        ])
        ->add('status_paid', CheckboxType::class, [
            'label' => 'shop.order.paid.check',
            'required' => false,
            'mapped' => false,
        ])
        ->add('payment_method', ChoiceType::class, [
            'label' => 'shop.order.paid.method',
            'required' => false,
            'choices' => [
                'shop.order.paid.credit_card' => Order::METHOD_CREDIT_CARD,
                'shop.order.paid.cash' => Order::METHOD_CASH,
                'shop.order.paid.bank_check' => Order::METHOD_BANK_CHECK,
            ],
        ])
        ->add('createInvoice', CheckboxType::class, [
            'label' => 'shop.order.paid.create_invoice',
            'required' => false,
            'mapped' => false,
        ])
        ->getForm();
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_order_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('status_paid')->getData()) {
                if ($form->get('createInvoice')->getData()) {
                    $invoices = $entity->getOrderInvoices();
                    if ($invoices[0] instanceof Invoice) {
                        $invoice = $invoices[0]->getInvoice();
                    } else {
                        $invoice = $shopManager->createInvoiceFromOrder($entity);
                        $entity->addInvoice($invoice);
                    }

                    $entityManager->persist($invoice);

                    $invoice->setPaymentStatus(Invoice::STATUS_PAID);
                }

                $entity->setPaymentStatus(OrderStatus::PAID);
            }

            $entityManager->flush();

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisShop/admin/order/paid.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/cancel', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function cancel(Request $request, Order $entity, EntityManagerInterface $entityManager, BasketRepository $basketRepository): Response
    {
        $this->denyAccessUnlessGranted('cancel', $entity);

        $form = $this->
            createFormBuilder($entity, [
                'method' => 'PUT',
                'action' => $this->generateUrl('tigris_shop_admin_order_cancel', ['id' => $entity->getId()]),
            ])
            ->getForm()
        ;
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_order_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Delete basket
            $basket = $basketRepository->findOneByOrder($entity);
            if ($basket instanceof Basket) {
                $basket->setOrder(null);
            }
            $entity->setStatus(OrderStatus::CANCELED);
            $entityManager->flush();

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisShop/admin/order/cancel.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/valid', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function valid(Request $request, Order $entity, EntityManagerInterface $entityManager, BasketRepository $basketRepository): Response
    {
        $form = $this->
            createFormBuilder($entity, [
                'method' => 'PUT',
                'action' => $this->generateUrl('tigris_shop_admin_order_valid', ['id' => $entity->getId()]),
            ])
            ->getForm()
        ;
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_order_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $basket = $basketRepository->findOneByOrder($entity);
            if ($basket instanceof Basket) {
                $basket->setOrder(null);
            }
            $entity->setStatus(OrderStatus::COMPLETED);
            $entityManager->flush();

            return new JsonResponse(['status' => 'success', 'message' => '']);
        }

        return $this->render('@TigrisShop/admin/order/valid.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/statuses', requirements: ['id' => '\d+'], methods: ['GET'], options: ['expose' => 'admin'])]
    public function statuses(Order $entity): Response
    {
        return $this->render('@TigrisShop/admin/order/statuses.html.twig', [
            'entity' => $entity,
        ]);
    }
}
