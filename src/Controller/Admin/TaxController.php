<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ShopBundle\Entity\Tax;
use Tigris\ShopBundle\Form\Type\TaxType;
use Tigris\ShopBundle\Repository\TaxRepository;

#[Route('/admin/shop/tax')]
#[IsGranted('ROLE_ADMIN')]
class TaxController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'shop.menu.shop' => null,
            'shop.menu.taxes' => ['route' => 'tigris_shop_admin_tax_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisShop/admin/tax/index.html.twig');
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => 'admin'])]
    public function data(Request $request, TaxRepository $taxRepository): JsonResponse
    {
        $data = $taxRepository->findData($this->getCriteria($request));

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', methods: ['GET', 'POST'], options: ['expose' => 'admin'])]
    public function new(Request $request, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $entity = new Tax();
        $form = $this->createForm(TaxType::class, $entity, [
            'action' => $this->generateUrl('tigris_shop_admin_tax_new'),
            'method' => Request::METHOD_POST,
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_tax_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('shop.tax.add.success'),
                ]);
            } else {
                $this->addFlash('success', $translator->trans('shop.tax.add.success'));

                return $this->redirectToRoute('tigris_shop_admin_tax_index');
            }
        }

        return $this->render('@TigrisShop/admin/tax/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function edit(
        Tax $entity,
        Request $request,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ): Response {
        $form = $this->createForm(TaxType::class, $entity, [
            'action' => $this->generateUrl('tigris_shop_admin_tax_edit', ['id' => $entity->getId()]),
            'method' => Request::METHOD_PUT,
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_tax_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('shop.tax.edit.success'),
                ]);
            } else {
                $this->addFlash('success', $translator->trans('shop.tax.edit.success'));

                return $this->redirectToRoute('tigris_shop_admin_tax_index');
            }
        }

        return $this->render('@TigrisShop/admin/tax/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function remove(
        Tax $entity,
        Request $request,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ): Response {
        $form = $this->getDeleteForm($entity, 'tigris_shop_admin_tax_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('shop.tax.remove.success'),
                ]);
            } else {
                $this->addFlash('success', $translator->trans('shop.tax.remove.success'));

                return $this->redirectToRoute('tigris_shop_admin_tax_index');
            }
        }

        return $this->render('@TigrisShop/admin/tax/remove.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
