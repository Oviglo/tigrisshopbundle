<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Form\Type\CategoryType;
use Tigris\ShopBundle\Repository\CategoryRepository;

#[Route('/admin/shop/category')]
#[IsGranted('ROLE_ADMIN')]
class CategoryController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'shop.menu.shop' => null,
            'shop.menu.categories' => ['route' => 'tigris_shop_admin_category_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisShop/admin/category/index.html.twig', []);
    }

    #[Route('/data', options: ['expose' => 'admin'], methods: ['GET'])]
    public function data(Request $request, CategoryRepository $categoryRepository): JsonResponse
    {
        $data = $categoryRepository->findData($this->getCriteria($request));

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs();
        $entity = new Category();
        $form = $this->createForm(CategoryType::class, $entity, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('tigris_shop_admin_category_new'),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_category_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new Jsonresponse(['status' => 'success', 'message' => $translator->trans('shop.category.add.success')]);
            } else {
                $this->addFlash('success', $translator->trans('shop.category.add.success'));

                return $this->redirectToRoute('tigris_shop_admin_category_index');
            }
        }

        return $this->render('@TigrisShop/admin/category/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(Category $entity, Request $request, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs();
        $form = $this->createForm(CategoryType::class, $entity, [
            'method' => Request::METHOD_PUT,
            'action' => $this->generateUrl('tigris_shop_admin_category_edit', ['id' => $entity->getId()]),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_category_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new Jsonresponse(['status' => 'success', 'message' => $translator->trans('shop.category.edit.success')]);
            } else {
                $this->addFlash('success', $translator->trans('shop.category.edit.success'));

                return $this->redirectToRoute('tigris_shop_admin_category_index');
            }
        }

        return $this->render('@TigrisShop/admin/category/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(Category $entity, Request $request, EntityManagerInterface $em, TranslatorInterface $translator): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_shop_admin_category_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();
            if ($request->isXmlHttpRequest()) {
                return new Jsonresponse(['status' => 'success', 'message' => $translator->trans('shop.category.remove.success')]);
            } else {
                $this->addFlash('success', $translator->trans('shop.category.remove.success'));

                return $this->redirectToRoute('tigris_shop_admin_category_index');
            }
        }

        return $this->render('@TigrisShop/admin/category/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route('/sort', options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function sort(Request $request, CategoryRepository $categoryRepository, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this
            ->createFormBuilder(null, [
                'attr' => [
                    'novalidate' => 'novalidate',
                ],
            ])
            ->setMethod(Request::METHOD_PUT)
            ->setAction($this->generateUrl('tigris_shop_admin_category_sort'))
            ->getForm()
        ;

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_category_index'));

        $form->handleRequest($request);

        $entities = $categoryRepository->getTree();

        if ($form->isSubmitted() && $form->isValid()) {
            $allEntities = $categoryRepository->findAll();
            $positions = $request->request->all('position');
            foreach ($positions as $id => $position) {
                foreach ($allEntities as $entity) {
                    if ($entity->getId() === $id) {
                        $entity->setPosition($position);
                        $em->persist($entity);

                        break;
                    }
                }
            }

            $em->flush();

            // reorder
            $categoryRepository->reorderAll('position');

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.category.sort.success')]);
            } else {
                $this->addFlash('success', 'shop.category.sort.success');

                return $this->redirectToRoute('tigris_shop_admin_category_index');
            }
        }

        return $this->render('@TigrisShop\admin\category\sort.html.twig', [
            'form' => $form,
            'entities' => $entities,
        ]);
    }
}
