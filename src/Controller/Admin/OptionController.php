<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\Admin\AbstractCrudController;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Crud\Crud;

#[Route('/admin/shop/option')]
#[IsGranted('ROLE_ADMIN')]
class OptionController extends AbstractCrudController
{
    protected function generateCrud(): Crud
    {
        return new Crud([]);
    }
}
