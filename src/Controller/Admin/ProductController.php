<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Form\Type\ProductCategoryType;
use Tigris\ShopBundle\Form\Type\ProductType;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\Repository\CategoryRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

#[Route('/admin/shop/product')]
#[IsGranted('ROLE_ADMIN')]
class ProductController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'shop.menu.shop' => null,
            'shop.menu.products' => ['route' => 'tigris_shop_admin_product_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(Request $request, CategoryRepository $categoryRepository): Response
    {
        $this->generateBreadcrumbs();

        $categories = $categoryRepository->findData();
        $criteria = $this->getCriteria($request, 'adminProductCriteria');

        return $this->render('@TigrisShop/admin/product/index.html.twig', [
            'categories' => $categories,
            'criteria' => $criteria,
        ]);
    }

    #[Route('/data', options: ['expose' => 'admin'], methods: ['GET'])]
    public function data(Request $request, ShopManager $shopManager): JsonResponse
    {
        $criteria = $this->getCriteria($request, 'adminProductCriteria');
        $criteria['categories'] = explode(',', (string) $request->get('categories', ''));
        $criteria['search'] = $request->get('search');
        $criteria['public'] = $request->get('public', '');
        $data = $shopManager->findProductData($criteria);

        return $this->paginatorToJsonResponse($data, ['Default', 'Admin']);
    }

    #[Route('/new', methods: ['GET', 'POST'], options: ['expose' => true])]
    public function new(Request $request, ShopManager $shopManager, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs([
            'shop.product.add.title' => ['route' => 'tigris_shop_admin_product_new'],
        ]);

        $entity = new Product();
        $form = $this->createForm(ProductType::class, $entity, [
            'action' => $this->generateUrl('tigris_shop_admin_product_new'),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_product_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shopManager->saveProduct($entity);
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.product.add.success')]);
            }
            $this->addFlash('success', 'shop.product.add.success');

            return $this->redirectToRoute('tigris_shop_admin_product_index');
        }

        return $this->render('@TigrisShop/admin/product/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/copy', requirements: ['id' => '\d+'], methods: ['GET', 'POST'], options: ['expose' => 'admin'])]
    public function copy(Product $entity, Request $request, EntityManagerInterface $entityManager)
    {
        $this->generateBreadcrumbs([
            'shop.product.copy.title' => ['route' => 'tigris_shop_admin_product_copy', 'params' => ['id' => $entity->getId()]],
        ]);

        $form = $this->createFormBuilder(null, ['attr' => ['class' => 'no-ajax']])
            ->setAction($this->generateUrl('tigris_shop_admin_product_copy', ['id' => $entity->getId()]))
            ->getForm()
        ;

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_product_index'));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $newProduct = clone $entity;
            $entityManager->persist($newProduct);
            $entityManager->flush();

            return $this->redirectToRoute('tigris_shop_admin_product_edit', ['id' => $newProduct->getId()]);
        }

        return $this->render('@TigrisShop/admin/product/copy.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], methods: ['GET', 'POST'], options: ['expose' => 'admin'])]
    public function edit(Product $entity, Request $request, ShopManager $shopManager, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs([
            'shop.product.edit.title' => ['route' => 'tigris_shop_admin_product_edit', 'params' => ['id' => $entity->getId()]],
        ]);

        $form = $this->createForm(ProductType::class, $entity, [
            'action' => $this->generateUrl('tigris_shop_admin_product_edit', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_product_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shopManager->saveProduct($entity);
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.product.edit.success')]);
            }
            $this->addFlash('success', 'shop.product.edit.success');

            return $this->redirectToRoute('tigris_shop_admin_product_index');
        }

        return $this->render('@TigrisShop/admin/product/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function remove(Product $entity, Request $request, ShopManager $shopManager, TranslatorInterface $translator): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_shop_admin_product_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shopManager->removeProduct($entity);
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.product.remove.success')]);
            }
            $this->addFlash('success', 'shop.product.remove.success');

            return $this->redirectToRoute('tigris_shop_admin_product_index');
        }

        return $this->render('@TigrisShop/admin/product/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route('/remove-group', methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function removeGroup(Request $request, ProductRepository $productRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $ids = $request->get('ids');
        $products = $productRepository->findByIds($ids);

        $form = $this->createFormBuilder(null, [
            'action' => $this->generateUrl('tigris_shop_admin_product_removegroup', ['ids' => $ids]),
            'method' => Request::METHOD_DELETE,
        ])->getForm();

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_product_index'), 'button.delete');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($products as $product) {
                $entityManager->remove($product);
            }

            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.product.change_quantity.success')]);
            }

            $this->addFlash('success', 'shop.product.change_quantity.success');

            return $this->redirectToRoute('tigris_shop_admin_product_index');
        }

        return $this->render('@TigrisShop/admin/product/remove_group.html.twig', [
            'products' => $products,
            'form' => $form,
        ]);
    }

    #[Route('/change-category', methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function changeCategory(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator, ProductRepository $productRepository): Response
    {
        $ids = $request->get('ids');
        $products = $productRepository->findByIds($ids);
        $form = $this->createForm(ProductCategoryType::class, null, ['method' => 'PUT', 'action' => $this->generateUrl('tigris_shop_admin_product_changecategory', ['ids' => $ids])]);
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_product_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categories = $form->getData()['categories'];
            foreach ($products as $product) {
                $product->setCategories($categories);

                $entityManager->persist($product);
            }

            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.product.change_category.success')]);
            }

            $this->addFlash('success', $translator->trans('shop.product.change_category.success'));

            return $this->redirectToRoute('tigris_shop_admin_product_index');
        }

        return $this->render('@TigrisShop/admin/product/change_category.html.twig', [
            'form' => $form,
            'products' => $products,
        ]);
    }

    #[Route('/{id}/change-part', requirements: ['id' => '\d+'], methods: ['PUT'], options: ['expose' => 'admin'])]
    public function changePart(Request $request, Product $entity, EntityManagerInterface $em, TranslatorInterface $translator): JsonResponse
    {
        $this->denyAccessUnlessGranted('edit', $entity);

        $data = $request->request->get('data', null);
        $value = $request->request->get('value', null);

        if (null !== $data && null !== $value) {
            $method = 'set'.ucfirst($data);

            if (method_exists($entity, $method)) {
                $entity->{$method}(Utils::formatValue($value));

                $em->persist($entity);
                $em->flush();

                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('shop.product.change_part.success')]);
            }
        }

        return new JsonResponse(['status' => 'error', 'message' => '']);
    }
}
