<?php

namespace Tigris\ShopBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Form\Type\TransportType;
use Tigris\ShopBundle\Repository\TransportRepository;

#[Route('admin/shop/transport')]
#[IsGranted('ROLE_ADMIN')]
class TransportController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'shop.menu.shop' => null,
            'shop.menu.transports' => ['route' => 'tigris_shop_admin_transport_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisShop/admin/transport/index.html.twig');
    }

    #[Route('/data', methods: ['GET'], options: ['expose' => 'admin'])]
    public function data(Request $request, TransportRepository $ransportRepository): Response
    {
        $data = $ransportRepository->findData($this->getCriteria($request));

        return $this->paginatorToJsonResponse($data);
    }

    #[Route('/new', methods: ['GET', 'POST'], options: ['expose' => 'admin'])]
    public function new(Request $request, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs();

        $entity = new Transport();
        $form = $this->createForm(TransportType::class, $entity, [
            'action' => $this->generateUrl('tigris_shop_admin_transport_new'),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_transport_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['type' => 'success', 'message' => $translator->trans('shop.transport.add.success')]);
            }

            $this->addFlash('success', 'shop.transport.add.success');

            return $this->redirectToRoute('tigris_shop_admin_transport_index');
        }

        return $this->render('@TigrisShop/admin/transport/new.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/edit', requirements: ['id' => '\d+'], methods: ['GET', 'PUT'], options: ['expose' => 'admin'])]
    public function edit(Request $request, Transport $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs();

        $form = $this->createForm(TransportType::class, $entity, [
            'action' => $this->generateUrl('tigris_shop_admin_transport_edit', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_transport_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['type' => 'success', 'message' => $translator->trans('shop.transport.edit.success')]);
            }

            $this->addFlash('success', 'shop.transport.edit.success');

            return $this->redirectToRoute('tigris_shop_admin_transport_index');
        }

        return $this->render('@TigrisShop/admin/transport/edit.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route('/{id}/remove', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function remove(Request $request, Transport $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('tigris_shop_admin_transport_remove', ['id' => $entity->getId()]))
            ->setMethod(Request::METHOD_DELETE)
            ->getForm()
        ;

        $this->addFormActions($form, $this->generateUrl('tigris_shop_admin_transport_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($entity);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['type' => 'success', 'message' => $translator->trans('shop.transport.remove.success')]);
            }

            $this->addFlash('success', 'shop.transport.remove.success');

            return $this->redirectToRoute('tigris_shop_admin_transport_index');
        }

        return $this->render('@TigrisShop/admin/transport/remove.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }
}
