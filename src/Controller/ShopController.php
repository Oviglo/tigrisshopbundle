<?php

namespace Tigris\ShopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Event\ProductListEvent;
use Tigris\ShopBundle\Event\ShopEvents;
use Tigris\ShopBundle\Form\Type\ProductFilterType;
use Tigris\ShopBundle\Repository\CategoryRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

#[Route(path: '/shop')]
class ShopController extends BaseController
{
    #[Route(path: '/{slug}', defaults: ['slug' => null])]
    public function index(
        Request $request,
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        EventDispatcherInterface $eventDispatcher,
        Category $category = null
    ): Response {
        $this->generateBreadcrumbs([
            'shop.menu.eshop' => ['route' => 'tigris_shop_shop_index', 'params' => ['slug' => $category?->getSlug()]],
        ]);

        $minMaxPrice = $productRepository->getMinMaxPrice();
        $maxPrice = $minMaxPrice['maxPrice'];
        $minPrice = $minMaxPrice['minPrice'];

        $criteria = $this->getCriteria($request);
        $criteria['public'] = true;
        $criteria['count'] = 24;
        if (null === $request->query->get('minPrice', null)) {
            $request->query->set('minPrice', $minPrice);
        }
        if (null === $request->query->get('maxPrice', null)) {
            $request->query->set('maxPrice', $maxPrice);
        }
        $page = (int) $request->query->get('p', 1);
        if ($page < 1) {
            throw $this->createNotFoundException();
        }
        $criteria['begin'] = ($page - 1) * $criteria['count'];

        // Sort
        $order = explode(',', $request->query->get('sortBy', 'id,desc'));
        unset($criteria['order']);
        if (isset($order[0]) && isset($order[1]) && '' !== trim($order[0]) && '' !== trim($order[1])) {
            $criteria['orders'] = [
                $order[0] => strtoupper($order[1]),
            ];
        }

        $form = $this->createForm(
            ProductFilterType::class,
            ['minPrice' => 0, 'category' => $category],
            ['method' => Request::METHOD_GET, 'attr' => ['id' => 'product-filters-form']]
        );

        $form->handleRequest($request);
        if (null !== $form->getData()) {
            $criteria = array_merge($criteria, $form->getData());
        }

        if ($category instanceof Category) {
            $criteria['root_category'] = $category;
        }

        $products = $productRepository->findData($criteria);
        $pages = ceil($products->count() / $criteria['count']);

        $rootCategories = $categoryRepository->getRootNodes();

        $productIterator = $products->getIterator();

        if ($productIterator instanceof \ArrayIterator) {
            $event = new ProductListEvent($productIterator->getArrayCopy());
            $eventDispatcher->dispatch($event, ShopEvents::LOAD_PRODUCT_LIST);
        }

        return $this->render('@TigrisShop/shop/index.html.twig', array_merge([
            'products' => $products,
            'criteria' => $criteria,
            'pages' => $pages,
            'page' => $page,
            'rootCategories' => $rootCategories,
            'form' => $form->createView(),
        ], $event->getViewParams()));
    }
}
