<?php

namespace Tigris\ShopBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderStatus;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Event\PaymentEvent;
use Tigris\ShopBundle\Event\ShopEvents;
use Tigris\ShopBundle\Form\Type\ChooseAddressType;
use Tigris\ShopBundle\Form\Type\TransportEntityType;
use Tigris\ShopBundle\Manager\DiscountManager;
use Tigris\ShopBundle\Manager\ShopManager;
use Tigris\ShopBundle\Payment\PaymentService;
use Tigris\ShopBundle\Repository\OrderRepository;
use Tigris\ShopBundle\Repository\TaxRepository;
use Tigris\ShopBundle\Repository\TransportRepository;
use Tigris\ShopBundle\TransportService\AbstractTransportService;
use Tigris\ShopBundle\TransportService\TransportServices;

#[Route(path: '/shop/purchase')]
class PurchaseController extends BaseController
{
    use FormTrait;

    #[Route(path: '/')]
    #[IsGranted('ROLE_USER')]
    public function index(Request $request, ShopManager $shopManager, EntityManagerInterface $entityManager): Response
    {
        $addresses = $shopManager->getAddressByUser($this->getUser());
        $basket = $shopManager->getBasketByUser($this->getUser());

        if (null == $basket || !$basket->getProductCount()) {
            $this->addFlash('error', 'shop.purchase.empty_basket');

            return $this->redirectToRoute('tigris_shop_basket_index');
        }

        $form = $this->createForm(ChooseAddressType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $address = $data['addresses'] ?? $data['address'];

            $address->setUser($this->getUser());
            $address->setIsDefault(true);
            $shopManager->saveAddress($address);
            $order = $basket->getOrder();

            if (!$order instanceof Order) {
                // Create new order
                $order = (new Order())
                    ->setUser($this->getUser())
                ;

                $basket->setOrder($order);
            }

            $order->setOrderAddress($address);
            $order->refreshFromBasket($basket);

            $entityManager->persist($order);
            $entityManager->flush();

            if ($basket->hasDelivery()) { // Go to transport page
                return $this->redirectToRoute('tigris_shop_purchase_delivery');
            }

            return $this->redirectToRoute('tigris_shop_purchase_summary');
        }

        return $this->render('@TigrisShop/purchase/index.html.twig', [
            'addresses' => $addresses,
            'form' => $form,
        ]);
    }

    #[Route(path: '/delivery')]
    #[IsGranted('ROLE_USER')]
    public function delivery(
        Request $request,
        ShopManager $shopManager,
        TransportRepository $transportRepository,
        TransportServices $transportServices,
        EntityManagerInterface $em
    ): Response {
        $basket = $shopManager->getBasketByUser($this->getUser());

        if (null == $basket || !$basket->getProductCount()) {
            $this->addFlash('error', 'shop.purchase.empty_basket');

            return $this->redirectToRoute('tigris_shop_basket_index');
        }

        $address = $basket->getShippingAddress();

        $excludeTransports = $basket->getExcludeTransports();

        if (null == $address) {
            $this->addFlash('error', 'shop.purchase.empty_address');

            return $this->redirectToRoute('tigris_shop_purchase_index');
        }

        $transports = $transportRepository->findByWeightAndAddress(
            $basket->getWeight(),
            $address,
            $basket->getPrice(),
            $excludeTransports
        );

        $form = $this->createFormBuilder(null, [
            'method' => Request::METHOD_POST,
            'action' => $this->generateUrl('tigris_shop_purchase_delivery'),
        ])
            ->add('transport', TransportEntityType::class, [
                'label' => false,
                'order' => $basket->getOrder(),
                'choices' => $transports,
                'class' => Transport::class,
                'expanded' => true,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->getForm()
        ;

        $this->addFormActions($form, false, 'button.next');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $transport = $data['transport'];
            $order = $basket->getOrder();
            $order->setTransport($transport);

            // Transport service
            $service = $transportServices->getService($transport->getService());
            $routeName = 'tigris_shop_purchase_summary';
            $routeParams = [];
            if ($service instanceof AbstractTransportService) {
                $serviceRoute = $service->getRoute($address);

                if (null !== $serviceRoute) {
                    $routeName = $serviceRoute['name'];
                    $routeParams = $serviceRoute['params'];
                }
            }

            $em->flush();

            return $this->redirectToRoute($routeName, $routeParams);
        }

        return $this->render('@TigrisShop/purchase/delivery.html.twig', [
            'entities' => $transports,
            'form' => $form,
        ]);
    }

    #[Route(path: '/summary')]
    #[IsGranted('ROLE_USER')]
    public function summary(
        ShopManager $shopManager,
        DiscountManager $discountManager,
        EntityManagerInterface $em,
        ConfigManager $configManager,
        TaxRepository $taxRepository
    ): Response {
        $basket = $shopManager->getBasketByUser($this->getUser());
        $order = $basket->getOrder();

        if ($order instanceof Order) {
            $discountManager->applyDiscountOnOrder($order);
            $giftOptions = $configManager->getGroupValue('TigrisShopBundle.giftoptions');
            if (isset($giftOptions['tax']) && null !== $giftOptions['tax']) {
                $giftOptions['tax'] = $taxRepository->findOneById((int) $giftOptions['tax']);
            }
            $order->refreshFromBasket($basket, $giftOptions);

            $em->persist($order);
            $em->flush();
        }

        if (!$basket instanceof Basket || !$basket->getProductCount()) {
            $this->addFlash('error', 'shop.purchase.empty_basket');

            return $this->redirectToRoute('tigris_shop_basket_index');
        }
        $paymentMethods = $this->getParameter('tigris_shop')['payment_system'];

        return $this->render('@TigrisShop/purchase/summary.html.twig', [
            'basket' => $basket,
            'paymentMethods' => $paymentMethods,
        ]);
    }

    #[Route(path: '/payment/{type}', requirements: ['type' => '[a-z]*'])]
    #[IsGranted('ROLE_USER')]
    public function payment(
        string $type,
        PaymentService $paymentService,
        ShopManager $shopManager,
        EntityManagerInterface $entityManager
    ): Response {
        $basket = $shopManager->getBasketByUser($this->getUser());
        $order = $basket->getOrder();
        $order->refresh();
        $entityManager->flush();

        if (null == $basket || !$basket->getProductCount()) {
            $this->addFlash('error', 'shop.purchase.empty_basket');

            return $this->redirectToRoute('tigris_shop_basket_index');
        }

        $payment = $paymentService->getPaymentService($type);
        $responseUrl = $this->generateUrl('tigris_shop_purchase_result', ['type' => $type], UrlGeneratorInterface::ABSOLUTE_URL);
        $successUrl = $this->generateUrl('tigris_shop_purchase_finished', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $cancelUrl = $this->generateUrl('tigris_shop_purchase_canceled', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $result = $payment->sendOrderRequest($order, $responseUrl, $successUrl, $cancelUrl);

        if (is_string($result)) {
            $result = ['url' => $result, 'fields' => []];
        }

        return $this->render('@TigrisShop/purchase/payment.html.twig', $result);
    }

    #[Route(path: '/result/{type}', requirements: ['type' => '[a-z]*'])]
    public function result(
        Request $request,
        PaymentService $paymentService,
        string $type,
        ShopManager $shopManager,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        MailerInterface $mailer,
        TranslatorInterface $translator,
        ConfigManager $configManager,
        UserRepository $userRepository,
        OrderRepository $orderRepository,
        LoggerInterface $log
    ): Response {
        $payment = $paymentService->getPaymentService($type);
        $paymentResponse = $payment->getOrderResponse($request);

        $basket = null;
        // Get order by payment response
        if (!empty($paymentResponse->getOrderId())) {
            $order = $orderRepository->findOneById($paymentResponse->getOrderId());
            if (null !== $order) {
                $basket = $shopManager->getBasketByUser($order->getUser());
            }
        } else {
            $this->denyAccessUnlessGranted('ROLE_USER');
            $basket = $shopManager->getBasketByUser($this->getUser());
            $order = $basket->getOrder();
            if (null == $order) {
                $order = $orderRepository->findLastByUser($this->getUser());
            }
        }

        if (null === $order) {
            throw new \Exception('Order not found', 1);
        }

        $order->setApiResponse($paymentResponse->getApiResponse());

        if ('true' == $request->query->get('success')) {
            if ($paymentResponse->isApproved()) {
                if ($basket instanceof Basket) {
                    $entityManager->remove($basket);
                }
                // Create invoice only for real payment
                if (!$paymentResponse->isTest()) {
                    $invoice = $shopManager->createInvoiceFromOrder($order);
                    $invoice->setPaymentStatus(Invoice::STATUS_PAID);
                    $invoice->setStatus(Invoice::STATUS_FINISHED);
                    $entityManager->persist($invoice);
                    $order->addInvoice($invoice);
                }

                $order->useDiscount();
                $order->setPaymentStatus(OrderStatus::PAID);

                if ($basket->hasDelivery()) {
                    $order->setDeliveryStatus(OrderStatus::SHIPPING);
                    $order->setStatus(OrderStatus::CONFIRMED);
                } else {
                    $order->setDeliveryStatus(OrderStatus::SHIPPED);
                    $order->setStatus(OrderStatus::COMPLETED);
                }

                $order->setPaymentMethod($type);

                // Update products restock
                $shopManager->updateStock($order);
                $entityManager->flush();
                // Emails
                $this->sendOrderMail($configManager, $mailer, $order, $translator, $userRepository, $log);

                $event = new PaymentEvent($order, $paymentResponse->getApiResponse());
                $eventDispatcher->dispatch($event, ShopEvents::PAYMENT_SUCCESS);
            } else {
                if (!empty($paymentResponse->getReturnValue())) {
                    return new Response($paymentResponse->getReturnValue());
                }

                return $this->redirectToRoute('tigris_shop_purchase_canceled');
            }

            if (!empty($paymentResponse->getReturnValue())) {
                return new Response($paymentResponse->getReturnValue());
            }

            return $this->redirectToRoute('tigris_shop_purchase_finished');
        }

        $order->setStatus(Order::STATUS_CANCELED);

        $event = new PaymentEvent($order, $paymentResponse->getApiResponse());
        $eventDispatcher->dispatch($event, ShopEvents::PAYMENT_FAILED);
        $entityManager->flush();

        return $this->render('@TigrisShop/purchase/error.html.twig');
    }

    #[Route(path: '/paid-at-shop')]
    #[IsGranted('ROLE_USER')]
    public function paidAtShop(
        Request $request,
        ShopManager $shopManager,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
        ConfigManager $configManager,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        LoggerInterface $log
    ): Response {
        $basket = $shopManager->getBasketByUser($this->getUser());
        $order = $basket->getOrder();
        if (null == $order) {
            return $this->redirectToRoute('tigris_shop_purchase_index');
        }

        $transport = $order->getTransport();
        if (!$transport instanceof Transport || Transport::TYPE_COLLECT !== $transport->getType()) {
            $this->createAccessDeniedException();
        }

        $form = $this->createFormBuilder(null, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_shop_purchase_paidatshop'),
        ])->getForm();
        $this->addFormActions($form, false, 'button.confirm');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($basket);
            $shopManager->updateStock($order);
            $order->setStatus(OrderStatus::CONFIRMED);
            $order->useDiscount();
            $entityManager->flush();

            // Emails
            $this->sendOrderMail($configManager, $mailer, $order, $translator, $userRepository, $log);

            return $this->redirectToRoute('tigris_shop_purchase_finished');
        }

        return $this->render('@TigrisShop/purchase/paidatshop.html.twig', [
            'form' => $form,
            'order' => $order,
        ]);
    }

    #[Route(path: '/finished')]
    #[IsGranted('ROLE_USER')]
    public function finished(ShopManager $shopManager): Response
    {
        $order = $shopManager->findLastOrderByUser($this->getUser());

        return $this->render('@TigrisShop/purchase/finished.html.twig', [
            'order' => $order,
        ]);
    }

    #[Route(path: '/canceled')]
    #[IsGranted('ROLE_USER')]
    public function canceled(): Response
    {
        return $this->render('@TigrisShop/purchase/canceled.html.twig');
    }

    private function sendOrderMail(
        ConfigManager $configManager,
        Mailer $mailer,
        Order $order,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        LoggerInterface $log
    ): void {
        try {
            $siteTitle = $configManager->getvalue('TigrisBaseBundle.title', '');
            $notificationMail = $configManager->getvalue('TigrisBaseBundle.noreply_mail', '');
            $email = (new TemplatedEmail())
                ->from(new Address($notificationMail, $siteTitle))
                ->to(new Address($order->getUser()->getEmail()))
                ->subject($translator->trans('email.order.title'))
                ->htmlTemplate('@TigrisShop/email/order.html.twig')
                ->context([
                    'order' => $order,
                ])
            ;
            $mailer->send($email);

            $admins = $userRepository->findAdmins();
            foreach ($admins as $admin) {
                $email = (new TemplatedEmail())
                    ->from(new Address($notificationMail, $siteTitle))
                    ->to(new Address($admin->getEmail()))
                    ->subject($translator->trans('email.admin_order.title'))
                    ->htmlTemplate('@TigrisShop/email/admin_order.html.twig')
                    ->context([
                        'order' => $order,
                    ])
                ;
                $mailer->send($email);
            }
        } catch (\Exception $e) {
            $log->error('[Send Order Mail] '.$e->getMessage());
        }
    }
}
