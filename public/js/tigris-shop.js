import MondialRelaySelect from '../vue/Transport/MondialRelaySelect'
import OrderMessagePost from '../vue/Order/OrderMessagePost'
import TigrisEvents from '../../../tigris-base-bundle/assets/js/tigris-events';

vueApp.component('mondial-relay-select', MondialRelaySelect);
vueApp.component('order-message-post', OrderMessagePost);

// ========= Product form ===============
function productActiveMaxQuantity(value) {
    let maxQuantityField = document.querySelector('#product_maxQuantity');
    maxQuantityField.readOnly = true;
    if (value) {
        maxQuantityField.readOnly = false;
    }
}

function productActiveDelivery(value) {
    let deliveryFieldset = document.querySelector('#product-delivery fieldset');
    deliveryFieldset.disabled = false;
    if (value == 2) {
        deliveryFieldset.disabled = true;
    }
}

function productAvailabilityChange(value) {
    let availableDateField = document.querySelector('#product_availableDate').closest('div');
    let preOrderDateField = document.querySelector('#product_preOrderDate').closest('div');

    switch (value) {
        case 'available':
            availableDateField.classList.add('d-none');
            preOrderDateField.classList.add('d-none');
            break;
        case 'unavailable':
            availableDateField.classList.add('d-none');
            preOrderDateField.classList.add('d-none');
            break;
        case 'available_at':
            availableDateField.classList.remove('d-none');
            preOrderDateField.classList.add('d-none');
            break;
        case 'pre_order':
            availableDateField.classList.remove('d-none');
            preOrderDateField.classList.remove('d-none');
            break;
    }
}

document.addEventListener(TigrisEvents.FORM_LOADED, function () {
    let maxQuantityCheck = document.querySelector('#product_enableMaxQuantity');
    if (null == maxQuantityCheck) {
        return;
    }
    productActiveMaxQuantity(maxQuantityCheck.checked);

    maxQuantityCheck.addEventListener('change', function () {
        productActiveMaxQuantity(maxQuantityCheck.checked);
    });

    // Delivery
    let productTypeRadios = document.querySelectorAll('[name="product[type]"]');

    productTypeRadios.forEach((element) => {
        if (element.checked) {
            productActiveDelivery(element.value);
        }
        element.addEventListener('change', function () {
            if (element.checked) {
                productActiveDelivery(element.value);
            }
        });
    });

    // Availability
    let availableStatusField = document.querySelector('#product_availableStatus');
    productAvailabilityChange(availableStatusField.value);
    availableStatusField.addEventListener('change', (e) => {
        productAvailabilityChange(e.target.value);
    });

    let form = document.querySelector('form[name="product"]');
    form.addEventListener('submit', function () {
        let disebledFields = form.querySelectorAll('[disabled]');
        for (const disabledField of disebledFields) {
            disabledField.disabled = false;
        }
    });


});

window.addEventListener('load', () => {
    setTimeout(() => {
        // Discount item type
        let discountItemTypeSelect = document.querySelector('#discount_itemType');
        if (null !== discountItemTypeSelect) {
            discountItemTypeSelect.addEventListener('change', () => {
                if (discountItemTypeSelect.value === 'products') {
                    document.querySelector('#discount_products').parentNode.classList.remove('d-none');
                    document.querySelector('#discount_categories').parentNode.classList.add('d-none');
                } else if (discountItemTypeSelect.value === 'categories') {
                    document.querySelector('#discount_products').parentNode.classList.add('d-none');
                    document.querySelector('#discount_categories').parentNode.classList.remove('d-none');
                } else if (discountItemTypeSelect.value === 'all_products') {
                    document.querySelector('#discount_products').parentNode.classList.add('d-none');
                    document.querySelector('#discount_categories').parentNode.classList.add('d-none');
                } else {
                    document.querySelector('#discount_products').parentNode.classList.add('d-none');
                    document.querySelector('#discount_categories').parentNode.classList.add('d-none');
                }
            });

            discountItemTypeSelect.dispatchEvent(new Event('change'));
        }
    }, 1200)

});

/* ============ Address autocompletion ==============*/
window.addEventListener('load', function () {
    let searchAddressInput = document.querySelector('.address-autocomplete');
    if (searchAddressInput) {
        import('../../../tigris-base-bundle/assets/js/tigris-autocomplete').then((module) => {
            let autocomplete = new module.TigrisAutocomplete('.address-autocomplete', 'tigris_base_geocoding_search');
            autocomplete.onCall((json) => {
                let result = [];
                for (const addr of json.addresses) {
                    result.push({
                        label: addr.line1 + ' ' + addr.zipCode + ' ' + addr.city,
                        value: addr.line1 + ' ' + addr.zipCode + ' ' + addr.city,
                        data: addr,
                    });
                }

                return result;
            });

            autocomplete.onClickItem((item) => {
                let addr = item.data;

                if (typeof addr !== 'undefined') {
                    let postal = document.querySelector('input[id$="_postal"]');
                    if (postal !== null) {
                        postal.value = addr.zipCode;
                    }

                    let city = document.querySelector('input[id$="_city"]');
                    if (city !== null) {
                        city.value = addr.city;
                    }

                    let line1 = document.querySelector('input[id$="_line1"]');
                    if (line1 !== null) {
                        line1.value = addr.line1;
                    }

                    let lat = document.querySelector('input[id$="_lat"]');
                    if (lat !== null) {
                        lat.value = addr.lat;
                    }

                    let lng = document.querySelector('input[id$="_lng"]');

                    if (lng !== null) {
                        lng.value = addr.lng;
                    }
                }
            });
        });
    }
});

/* ============== FILTER range price ==================*/
window.addEventListener('load', () => {
    let form = document.querySelector('#product-filters-form');
    if (null !== form) {
        let minPriceInput = document.getElementById('minPrice');
        let maxPriceInput = document.getElementById('maxPrice');
        let minValue = document.createElement('span');
        minValue.classList.add('min-value');
        minValue.innerHTML = minPriceInput.value;
        minPriceInput.parentNode.querySelector('label').append(minValue);

        let maxValue = document.createElement('span');
        maxValue.classList.add('max-value');
        maxValue.innerHTML = maxPriceInput.value;
        maxPriceInput.parentNode.querySelector('label').append(maxValue);

        minPriceInput.addEventListener('input', (e) => {
            minValue.innerHTML = e.target.value;
            if (parseInt(e.target.value) > parseInt(maxPriceInput.value)) {
                maxPriceInput.value = e.target.value;
                maxValue.innerHTML = e.target.value;
            }
        });

        maxPriceInput.addEventListener('input', (e) => {
            maxValue.innerHTML = e.target.value;
            if (parseInt(e.target.value) < parseInt(minPriceInput.value)) {
                minPriceInput.value = e.target.value;
                minValue.innerHTML = e.target.value;
            }
        });
    }
});
