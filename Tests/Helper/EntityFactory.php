<?php

namespace Tigris\ShopBundle\Tests\Helper;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderDiscount;
use Tigris\ShopBundle\Entity\OrderProduct;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Entity\Tax;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Entity\TransportSlot;

class EntityFactory
{
    public static function createOrder(): Order
    {
        $o = new Order();
        $o->setUser(new User());
        $reflectionProperty = new \ReflectionProperty(Order::class, 'id');
        $reflectionProperty->setValue($o, 1);

        return $o;
    }

    public static function createOrderProduct(Order $order, float $price, int $quantity, float $tax = 0, ?Discount $discount = null): OrderProduct
    {
        $op = new OrderProduct();
        $product = new Product();
        $product->setName('Product');
        $product->setPrice($price);

        if ($tax > 0) {
            $product->setTax(static::createTax($tax));
        }

        if ($discount instanceof Discount) {
            $discount->setItemType(Discount::ITEM_TYPE_PRODUCTS);
            $discount->addProduct($product);
            $product->setDiscounts(new ArrayCollection([$discount]));
        }

        $op->setOrder($order);
        $op->setQuantity($quantity);
        $op->setProduct($product);

        return $op;
    }

    public static function createTransport(float $price): Transport
    {
        $t = new Transport();
        $s = new TransportSlot();
        $s->setPrice($price);
        $t->setName('Transport');

        $t->addSlot($s);

        return $t;
    }

    public static function createTax(float $value, string $name = 'TAX'): Tax
    {
        $t = new Tax();
        $t->setName($name);
        $t->setValue($value);

        return $t;
    }

    public static function createDiscount(string $type, float $value): Discount
    {
        return (new Discount())
            ->setValue($value)
            ->setType($type)
            ->setName(uniqid('DISCOUNT_'));
    }

    public static function createOrderDiscount(Discount $discount, Order $order): OrderDiscount
    {
        return (new OrderDiscount())
            ->setDiscount($discount)
            ->setOrder($order);
    }
}
