<?php

namespace Tigris\ShopBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\ShopBundle\Entity\Option;

#[CoversClass(Option::class)]
class OptionTest extends KernelTestCase
{
    use EntityTestTrait;

    private function getEntity(): Option
    {
        return (new Option())
            ->setPrice(10.50)
            ->setName('Option test')
            ->setType(Option::TYPE_ORDER)
        ;
    }

    #[Test]
    public function testValid(): void
    {
        $entity = $this->getEntity();

        $this->validateEntity($entity);

        self::assertNull($entity->getId());
        self::assertEquals('Option test', $entity->getName());
        self::assertEquals(10.50, $entity->getPrice());
        self::assertEquals(Option::TYPE_ORDER, $entity->getType());
    }
}
