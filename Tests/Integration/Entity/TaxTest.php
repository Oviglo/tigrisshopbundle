<?php

namespace Tigris\ShopBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Tigris\ShopBundle\Entity\Tax;

#[CoversClass(Tax::class)]
class TaxTest extends KernelTestCase
{
    private function getEntity(): Tax
    {
        return (new Tax())
            ->setName('Tax Test')
            ->setValue(6.49)
        ;
    }

    #[Test]
    public function testValidEntity(): void
    {
        self::bootKernel();

        $entity = $this->getEntity();
        $errors = self::getContainer()->get(ValidatorInterface::class)->validate($entity);
        self::assertCount(0, $errors);

        self::assertEquals('TAX TEST (6.49%)', (string) $entity);
        self::assertArrayHasKey('id', $entity->toArray());
        self::assertArrayHasKey('value', $entity->toArray());
        self::assertArrayHasKey('name', $entity->toArray());
    }
}
