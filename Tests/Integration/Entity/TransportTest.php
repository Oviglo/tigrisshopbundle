<?php

namespace Tigris\ShopBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Entity\TransportSlot;

#[CoversClass(Transport::class)]
class TransportTest extends KernelTestCase
{
    use EntityTestTrait;

    private Order&MockObject $order;

    public function setUp(): void
    {
        $this->order = $this->createMock(Order::class);
    }

    private static function getEntity(): Transport
    {
        return (new Transport())
            ->setName('Transport test')
            ->setDescription('Transport description test')
            ->setCountries(['FR', 'DE', 'IT', 'ES'])
            ->setMaxDistance(10)
            ->setFreeAtPrice(200)
            ->setType(Transport::TYPE_SHIPPING)
            ->setMinPrice(0)
            ->setMaxDepth(1000)
            ->setMaxWidth(1000)
            ->setMaxHeight(1000)
            ->setMaxWeight(1000)
        ;
    }

    #[Test]
    public function testValid(): void
    {
        $entity = self::getEntity();

        $this->validateEntity($entity);
    }

    #[Test]
    public function testInvalidName(): void
    {
        $entity = self::getEntity();
        $entity->setName('');

        $this->validateEntity($entity, 1);
    }

    #[Test]
    public function testInvalidMaxDistance(): void
    {
        $this->validateEntity(self::getEntity()->setMaxDistance(3000), 1);
        $this->validateEntity(self::getEntity()->setMaxDistance(-100), 1);
    }

    #[Test]
    public function testSlot(): void
    {
        $entity = self::getEntity();
        $slot1 = (new TransportSlot())
            ->setMaxWeight(10)
            ->setPrice(10.50)
        ;
        $entity->addSlot($slot1);

        self::assertCount(1, $entity->getSlots());

        $slot2 = (new TransportSlot())
            ->setMaxWeight(50)
            ->setPrice(12)
        ;
        $entity->addSlot($slot2);

        self::assertCount(2, $entity->getSlots());

        $entity->removeSlot($slot1);

        self::assertCount(1, $entity->getSlots());
    }

    #[Test]
    #[DataProvider('priceProvider')]
    public function testPrice(Transport $entity, float $orderWeight, float $orderPrice, float $expectedPrice): void
    {
        $this->order->method('getWeight')->willReturn($orderWeight);
        $this->order->method('getPrice')->willReturn($orderPrice);
        $entity->setOrder($this->order);

        self::assertEquals($expectedPrice, $entity->getPrice());
    }

    public static function priceProvider(): \Generator
    {
        yield [self::getEntity(), 0, 0, 0];

        $entity = self::getEntity();
        $slot1 = (new TransportSlot())
            ->setMaxWeight(10)
            ->setPrice(10.50)
        ;
        $entity->addSlot($slot1);
        $slot2 = (new TransportSlot())
            ->setMaxWeight(50)
            ->setPrice(25)
        ;
        $entity->addSlot($slot2);
        $slot3 = (new TransportSlot())
            ->setMaxWeight(100)
            ->setPrice(42.99)
        ;
        $entity->addSlot($slot3);

        yield [$entity, 0, 0, 10.50];
        yield [$entity, 22, 10, 25];
        yield [$entity, 80, 10, 42.99];
        yield [$entity, 80, 400, 0];
    }
}
