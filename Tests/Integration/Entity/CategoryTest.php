<?php

namespace Tigris\ShopBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\ShopBundle\Entity\Category;

#[CoversClass(Category::class)]
class CategoryTest extends KernelTestCase
{
    use EntityTestTrait;

    private function getEntity(): Category
    {
        return (new Category())
            ->setName('Category test')
            ->setPublic(true)
            ->setPosition(0)
            ->setAccessAuthenticated(false)
        ;
    }

    #[Test]
    public function testValid(): void
    {
        $this->validateEntity($this->getEntity());
    }
}
