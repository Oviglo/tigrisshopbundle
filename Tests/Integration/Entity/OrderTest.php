<?php

namespace Tigris\ShopBunlde\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\BaseBundle\Tests\Helper\ReflectionTestTrait;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;

#[CoversClass(Order::class)]
class OrderTest extends KernelTestCase
{
    use EntityTestTrait;
    use ReflectionTestTrait;

    private function getEntity(): Order
    {
        $user = new User();
        self::setProperty($user, 'id', 123);
        $order = new Order();
        $order
            ->setUser($user)
            ->setFirstname('John')
            ->setName('DOE')
        ;

        $product1 = (new Product())
            ->setName('Product 1')
            ->setPrice(15.99)
            ->setQuantity(10)
        ;
        self::setProperty($product1, 'id', 1);

        $order->addProduct($product1, 1);

        $product2 = (new Product())
            ->setName('Product 2')
            ->setPrice(10)
            ->setQuantity(10)
        ;
        self::setProperty($product2, 'id', 2);

        $order->addProduct($product2, 2);

        return $order;
    }

    #[Test]
    public function testValid(): void
    {
        $entity = $this->getEntity();
        $this->validateEntity($entity);

        self::assertEquals(35.99, $entity->getPrice());
    }

    #[Test]
    public function testAddInvoice(): void
    {
        $invoice = new Invoice();
        $entity = $this->getEntity();
        $entity->addInvoice($invoice);

        $invoice = $entity->getMainInvoice();

        self::assertEquals(35.99, $invoice->getAmountIT());
    }
}
