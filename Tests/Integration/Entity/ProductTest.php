<?php

namespace Tigris\ShopBundle\Tests\Integration\Entity;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Tigris\BaseBundle\Tests\Helper\EntityTestTrait;
use Tigris\ShopBundle\Entity\Product;

#[CoversClass(Product::class)]
class ProductTest extends KernelTestCase
{
    use EntityTestTrait;

    private static function getEntity(): Product
    {
        $product = new Product();
        $product
            ->setName('Test product')
            ->setPublic(true)
            ->setQuantity(10)
            ->setPrice(12.99)
        ;

        return $product;
    }

    #[Test]
    public function testValidEntity(): void
    {
        $entity = self::getEntity();

        $errors = self::getContainer()->get(ValidatorInterface::class)->validate($entity);

        self::assertCount(0, $errors);
    }

    #[Test]
    public function testInvalidQuantityEntity(): void
    {
        $entity = self::getEntity();
        $entity->setQuantity(-100);

        $this->validateEntity($entity, 1);
    }

    #[Test]
    public function testInvalidMaxQuantityEntity(): void
    {
        $entity = self::getEntity();
        $entity->setMaxQuantity(-100);

        $this->validateEntity($entity, 1);
    }

    #[Test]
    public function testInvalidPriceEntity(): void
    {
        $this->validateEntity(self::getEntity()->setPrice(0), 0);
        $this->validateEntity(self::getEntity()->setPrice(-1), 1);
    }

    #[Test]
    public function testInvalidNameEntity(): void
    {
        $this->validateEntity(self::getEntity()->setName(''), 1);
        $this->validateEntity(self::getEntity()->setName(random_bytes(200)), 1);
    }

    #[Test]
    #[DataProvider('preOrderProvider')]
    public function testPreOrder(Product $entity, bool $expected): void
    {
        self::assertEquals($entity->isPreOrder(), $expected);
    }

    public static function preOrderProvider(): \Generator
    {
        yield [self::getEntity(), false];

        $entity = self::getEntity();
        $entity->setAvailableStatus(Product::STATUS_PRE_ORDER);
        yield [$entity, false];

        $entity = self::getEntity();
        $entity->setAvailableStatus(Product::STATUS_PRE_ORDER);
        $entity->setPreOrderDate(new \DateTime('5 minutes ago'));
        yield [$entity, false];

        // La précommande est valide seulement si la date de disponibilité est dans le futur et la date de précommande est passée
        $entity = self::getEntity();
        $entity->setAvailableStatus(Product::STATUS_PRE_ORDER);
        $entity->setPreOrderDate(new \DateTime('5 minutes ago'));
        $entity->setAvailableDate(new \DateTime('+ 1 week'));
        yield [$entity, true];
    }
}
