<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Repository\CategoryRepository;

#[CoversClass(CategoryRepository::class)]
class CategoryRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var CategoryRepository */
        $repository = $em->getRepository(Category::class);

        $data = $repository->findData(['public' => true, 'parent' => true]);

        self::assertGreaterThan(0, $data->count());
    }

    #[Test]
    public function getTree(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var CategoryRepository */
        $repository = $em->getRepository(Category::class);

        $data = $repository->getTree();

        self::assertGreaterThan(0, count($data));
    }
}
