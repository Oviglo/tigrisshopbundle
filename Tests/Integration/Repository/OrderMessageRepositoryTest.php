<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderMessage;
use Tigris\ShopBundle\Repository\OrderMessageRepository;
use Tigris\ShopBundle\Repository\OrderRepository;

#[CoversClass(OrderMessageRepository::class)]
class OrderMessageRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findDataByOrder(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OrderMessageRepository */
        $repository = $em->getRepository(OrderMessage::class);

        /** @var Order */
        $order = self::getContainer()->get(OrderRepository::class)->findOneBy([]);

        $data = $repository->findDataByOrder($order, []);

        self::assertGreaterThan(0, $data->count());
    }
}
