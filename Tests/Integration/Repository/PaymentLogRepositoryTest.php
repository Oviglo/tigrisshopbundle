<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\PaymentLog;
use Tigris\ShopBundle\Repository\PaymentLogRepository;

#[CoversClass(PaymentLogRepository::class)]
class PaymentLogRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var PaymentLogRepository */
        $repository = $em->getRepository(PaymentLog::class);

        $data = $repository->findData([]);

        self::assertGreaterThanOrEqual(0, $data->count());
    }
}
