<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\Tax;
use Tigris\ShopBundle\Repository\TaxRepository;

#[CoversClass(TaxRepository::class)]
class TaxRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var TaxRepository */
        $repository = $em->getRepository(Tax::class);

        $data = $repository->findData([]);

        self::assertGreaterThan(0, $data->count());
    }
}
