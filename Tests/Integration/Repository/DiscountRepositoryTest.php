<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Repository\DiscountRepository;

#[CoversClass(DiscountRepository::class)]
class DiscountRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var DiscountRepository */
        $repository = $em->getRepository(Discount::class);

        $data = $repository->findData(['validity' => 'valid', 'search' => 'Discount', 'itemType' => Discount::DISCOUNT_TYPE_VALUE]);

        self::assertGreaterThanOrEqual(0, $data->count());
    }
}
