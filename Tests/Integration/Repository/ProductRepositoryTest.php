<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Repository\CategoryRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

#[CoversClass(ProductRepository::class)]
class ProductRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);

        $categories = static::getContainer()->get(CategoryRepository::class)->findAll();

        $data = $repository->findData(['public' => true, 'search' => 'Mario', 'minPrice' => 0, 'maxPrice' => 500, 'categories' => $categories]);

        self::assertGreaterThan(0, $data->count());
    }

    #[Test]
    public function findMaxPrice(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);

        $data = $repository->findMaxPrice();

        self::assertEquals(99.49, $data);
    }

    #[Test]
    public function findMinPrice(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);

        $data = $repository->findMinPrice();

        self::assertEquals(2.99, $data);
    }

    #[Test]
    public function getMinMaxPrice(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);

        $data = $repository->getMinMaxPrice();

        self::assertEquals(2.99, $data['minPrice']);
        self::assertEquals(99.49, $data['maxPrice']);
    }

    #[Test]
    public function findOneBySlug(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);

        $data = $repository->findOneBySlug('mario-party-superstars');

        self::assertNotNull($data);
    }

    #[Test]
    public function search(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var ProductRepository */
        $repository = $em->getRepository(Product::class);

        $data = $repository->search('Mario');

        self::assertCount(5, $data);
    }
}
