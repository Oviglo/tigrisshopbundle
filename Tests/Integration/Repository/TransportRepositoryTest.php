<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Repository\TransportRepository;

#[CoversClass(TransportRepository::class)]
class TransportRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var TransportRepository */
        $repository = $em->getRepository(Transport::class);

        $data = $repository->findData([]);

        self::assertGreaterThan(0, $data->count());
    }

    #[Test]
    public function findByWeightAndCountry(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var TransportRepository */
        $repository = $em->getRepository(Transport::class);

        $data = $repository->findByWeightAndCountry(0, 'FR');

        self::assertGreaterThan(0, count($data));
    }
}
