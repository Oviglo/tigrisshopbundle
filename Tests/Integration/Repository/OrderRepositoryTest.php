<?php

namespace Tigris\ShopBundle\Tests\Integration\Repository;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\OrderStatus;
use Tigris\ShopBundle\Repository\OrderRepository;

#[CoversClass(OrderRepository::class)]
class OrderRepositoryTest extends KernelTestCase
{
    #[Test]
    public function findData(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OrderRepository */
        $repository = $em->getRepository(Order::class);

        $data = $repository->findData(['status' => 'valid', 'paymentStatus' => OrderStatus::PAID]);

        self::assertGreaterThanOrEqual(0, $data->count());
    }

    #[Test]
    public function testCount(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OrderRepository */
        $repository = $em->getRepository(Order::class);

        $data = $repository->count(['status' => 'valid', 'paymentStatus' => OrderStatus::PAID]);

        self::assertGreaterThanOrEqual(0, $data);
    }

    #[Test]
    public function findByUser(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OrderRepository */
        $repository = $em->getRepository(Order::class);

        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => AbstractLoginWebTestCase::USER_EMAIL]);

        $data = $repository->findByUser($user, []);

        self::assertGreaterThanOrEqual(0, $data->count());
    }

    #[Test]
    public function getValidTotalPrice(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OrderRepository */
        $repository = $em->getRepository(Order::class);

        $data = $repository->getValidTotalPrice();

        self::assertGreaterThanOrEqual(0, $data);
    }

    #[Test]
    public function findChartByDates(): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);
        /** @var OrderRepository */
        $repository = $em->getRepository(Order::class);

        $data = $repository->findChartByDates(new \DateTime('-1 year'), new \DateTime('next month'));

        self::assertGreaterThanOrEqual(0, count($data));
    }
}
