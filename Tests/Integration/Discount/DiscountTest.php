<?php

namespace Tigris\ShopBundle\Tests\Integration\Discount;

use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Manager\DiscountManager;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class DiscountTest extends AbstractLoginWebTestCase
{
    private DiscountManager $discountManager;

    protected function setUp(): void
    {
        parent::setUp();

        $this->discountManager = $this->container->get(DiscountManager::class);
    }

    #[DataProvider('getProductTests')]
    public function testProductDiscount(string $name, bool $useDiscount, float $expectedPrice, string $message = ''): void
    {
        $product = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => $name])
        ;

        $this->assertSame($expectedPrice, $product->getPrice($useDiscount));
    }

    public static function getProductTests(): \Iterator
    {
        yield ["Super Mario Odyssey", false, 44.90, ''];
        yield ["Super Mario Odyssey", true, 21.43, ''];
        yield ["Astérix le gaulois", false, 9.99, ''];
        yield ["Astérix le gaulois", true, 7.99, 'One discount need to set only one time'];
    }

    /* DISCOUNT CODE */

    public function testDiscountCodeValidation(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $canValid = $this->discountManager->canActivateCode('HAPPYCODE', $user);

        $this->assertSame(true, $canValid);
    }

    public function testDeprecatedDiscountCodeValidation(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $canValid = $this->discountManager->canActivateCode('DEPRECATED', $user);

        $this->assertSame(false, $canValid);
    }

    public function testUnactivateCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $product = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'The Legend Of Zelda Breath Of The Wild'])
        ;

        $this->assertSame(51.9, $product->getPrice(true, $user));
    }

    public function testActivateCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $canValid = $this->discountManager->activateCode('HAPPYCODE', $user);

        $this->assertSame(true, $canValid);

        /**
         * @var Product
         */
        $product = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'The Legend Of Zelda Breath Of The Wild'])
        ;

        $this->assertSame(5.0, $product->getPrice(true, $user));
    }

    public function testActivateOrderCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $this->discountManager->activateCode('ORDERCODE', $user);

        $product1 = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'The Legend Of Zelda Breath Of The Wild'])
        ;

        $product2 = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'Kaamelott, Tome 1 : L\'Armée Du Nécromant'])
        ;

        $order = (new Order())
            ->setUser($user)
            ->addProduct($product1, 1)
            ->addProduct($product2, 2)
        ;

        // 51.90 + (13.95 - 2) * 2 = 75.8
        $this->assertSame(75.8, $order->getPrice(false, true, $user));

        $this->discountManager->applyDiscountOnOrder($order);

        // 75.8 - 15
        $this->assertSame(60.8, $order->getPrice(false, true, $user));
    }

    public function testDeprecatedOrderCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $this->discountManager->activateCode('DEPRECATEDORDERCODE', $user);

        $product1 = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'The Legend Of Zelda Breath Of The Wild'])
        ;

        $product2 = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'Kaamelott, Tome 1 : L\'Armée Du Nécromant'])
        ;

        $order = (new Order())
            ->setUser($user)
            ->addProduct($product1, 1)
            ->addProduct($product2, 2)
        ;

        // 51.90 + (13.95 - 2) * 2 = 75.8
        $this->assertSame(75.8, $order->getPrice(false, true, $user));

        $this->discountManager->applyDiscountOnOrder($order);

        // same price because invalid code
        $this->assertSame(75.8, $order->getPrice(false, true, $user));
    }

    public function testMinPriceOrderCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $this->discountManager->activateCode('MINPRICEORDERCODE', $user);

        $product = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'Kaamelott, Tome 1 : L\'Armée Du Nécromant'])
        ;

        $order = (new Order())
            ->setUser($user)
            ->addProduct($product, 1)
        ;

        // 13.95 - 2
        $this->assertSame(11.95, $order->getPrice(false, true, $user));

        $this->discountManager->applyDiscountOnOrder($order);

        $this->assertSame(11.95, $order->getPrice(false, true, $user));
    }

    public function testValidMinPriceOrderCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $this->discountManager->activateCode('MINPRICEORDERCODE', $user);

        $product = $this->em
            ->getRepository(Product::class)
            ->findOneBy(['name' => 'Kaamelott, Tome 1 : L\'Armée Du Nécromant'])
        ;

        $order = (new Order())
            ->setUser($user)
            ->addProduct($product, 2)
        ;

        // 13.95 - 2 * 2
        $this->assertSame(23.9, $order->getPrice(false, true, $user));

        $this->discountManager->applyDiscountOnOrder($order);

        $this->assertSame(8.9, $order->getPrice(false, true, $user));
    }

    public function testUserCode(): void
    {
        $user = $this->getUser('asterix@yopmail.com');
        $this->discountManager->unactivateAllCodes($user);

        $canValid = $this->discountManager->activateCode('CODEFORASTERIX', $user);
        $this->assertSame(true, $canValid);

        // Code invalid for obelix
        $user = $this->getUser('obelix@yopmail.com');
        $canValid = $this->discountManager->activateCode('CODEFORASTERIX', $user);
        $this->assertSame(false, $canValid);
    }
}
