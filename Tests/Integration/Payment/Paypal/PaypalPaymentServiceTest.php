<?php

namespace Tigris\ShopBundle\Tests\Integration\Payment\Paypal;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Payment\PaymentService;
use Tigris\ShopBundle\Payment\PaymentServiceInterface;
use Tigris\ShopBundle\Payment\Paypal\PaypalPaymentService;
use Tigris\ShopBundle\Tests\Helper\EntityFactory;

#[CoversClass(PaypalPaymentService::class)]
class PaypalPaymentServiceTest extends KernelTestCase
{
    private PaypalPaymentService $paypalPayment;

    public function setUp(): void
    {
        $paymentService = self::getContainer()->get(PaymentService::class);
        $this->paypalPayment = $paymentService->getPaymentService('paypal');
    }

    #[Test]
    public function getRedirectionType(): void
    {
        self::assertEquals(PaymentServiceInterface::REDIRECTION_AUTO, $this->paypalPayment->getRedirectionType());
    }

    #[Test]
    #[DataProvider('sendOrderRequestProvider')]
    public function sendOrderRequest(Order $order, string $responseUrl, string $finishedUrl, string $canceledUrl, string $expected): void
    {
        $response = $this->paypalPayment->sendOrderRequest($order, $responseUrl, $finishedUrl, $canceledUrl);

        self::assertStringContainsString($expected, $response);

        $urlComponents = parse_url((string) $response);
        parse_str($urlComponents['query'], $params);

        $request = $this->createMock(Request::class);
        $request->method('get')->willReturn($params['token']);

        $paymentResponse = $this->paypalPayment->getOrderResponse($request);

        self::assertFalse($paymentResponse->isApproved());
    }

    public static function sendOrderRequestProvider(): \Generator
    {
        $shippingAddress = new Address();
        $shippingAddress
            ->setCity('City')
            ->setCountry('FR')
            ->setPostal('88210')
            ->setComplement('')
        ;

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 100, 3, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 68.54, 4, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 34.99, 6, 5.5));
        $transport = EntityFactory::createTransport(10.65);
        $order->setTransport($transport);
        $order->setShippingAddress($shippingAddress);

        yield [$order, 'http://localhost', 'http://localhost', 'http://localhost', 'token='];
    }
}
