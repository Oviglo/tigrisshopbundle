<?php

namespace Tigris\ShopBundle\Tests\Integration\Form\Type;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\FormFactoryInterface;
use Tigris\ShopBundle\Entity\Tax;
use Tigris\ShopBundle\Form\Type\TaxType;

#[CoversClass(TaxType::class)]
class TaxTypeTest extends KernelTestCase
{
    #[Test]
    public function testSendData(): void
    {
        /** @var FormFactoryInterface */
        $formFactory = static::getContainer()->get(FormFactoryInterface::class);

        $model = new Tax();

        $form = $formFactory->create(TaxType::class, $model);

        $data = [
            'name' => 'Tax Test',
            'value' => 20.00,
        ];

        $form->submit($data);

        $expected = new Tax();
        $expected
            ->setName($data['name'])
            ->setValue($data['value']);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($expected, $model);
    }
}
