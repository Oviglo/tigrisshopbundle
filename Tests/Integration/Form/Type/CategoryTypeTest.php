<?php

namespace Tigris\ShopBundle\Tests\Integration\Form\Type;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\FormFactoryInterface;
use Tigris\ShopBundle\Entity\Category;
use Tigris\ShopBundle\Form\Type\CategoryType;

#[CoversClass(CategoryType::class)]
class CategoryTypeTest extends KernelTestCase
{
    #[Test]
    public function testSendData(): void
    {
        /** @var FormFactoryInterface */
        $formFactory = static::getContainer()->get(FormFactoryInterface::class);

        $model = new Category();

        $form = $formFactory->create(CategoryType::class, $model);

        $data = [
            'name' => 'Category Test',
            'public' => true,
        ];

        $form->submit($data);

        $expected = new Category();
        $expected
            ->setName($data['name'])
            ->setPublic($data['public']);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($expected, $model);
    }
}
