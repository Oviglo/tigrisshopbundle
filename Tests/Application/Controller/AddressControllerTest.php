<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\AddressController;
use Tigris\ShopBundle\Repository\AddressRepository;

#[CoversClass(AddressController::class)]
class AddressControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function new(): void
    {
        $this->loginUser();

        $crawler = $this->client->request(Request::METHOD_GET, '/address/new');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('address_actions_save')->form();
        $this->client->submit($form, [
            'address[postal]' => '88100',
            'address[city]' => 'Saint-Dié-des-Vosges',
            'address[firstname]' => 'Jean',
            'address[name]' => 'Dujardin',
            'address[phone]' => '0600000000',
            'address[line1]' => 'Chez moi',
        ]);

        self::assertResponseRedirects();
    }

    #[Test]
    public function remove(): void
    {
        $this->loginUser();

        $repository = self::getContainer()->get(AddressRepository::class);

        $count = $repository->count([]);
        $entity = $repository->findAll()[0];

        $crawler = $this->client->request(Request::METHOD_GET, '/address/'.$entity->getId().'/remove');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();
        $this->client->submit($form, []);

        self::assertResponseRedirects();

        self::assertEquals($count - 1, $repository->count([]));
    }
}
