<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\ShopController;

#[CoversClass(ShopController::class)]
class ShopControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->client->request(Request::METHOD_GET, '/shop');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function indexPagination(): void
    {
        $this->client->request(Request::METHOD_GET, '/shop', ['p' => 2]);

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function indexWrongPagination(): void
    {
        $this->client->request(Request::METHOD_GET, '/shop', ['p' => 'wrong']);

        self::assertResponseStatusCodeSame(404);
    }
}
