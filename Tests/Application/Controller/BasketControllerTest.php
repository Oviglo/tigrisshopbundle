<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Repository\ProductRepository;

class BasketControllerTest extends AbstractLoginWebTestCase
{
    private ProductRepository $productRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productRepository = $this->container->get(ProductRepository::class);
    }

    public function testAdd(): void
    {
        $this->login('asterix@yopmail.com');
        $product = $this->productRepository->findOneBy(['reference' => 'SMO']);

        $this->client->request(Request::METHOD_GET, '/shop/basket/'.$product->getId().'/add');
        $this->assertResponseRedirects();

        $this->client->followRedirect();

        $this->assertSelectorTextContains('.product-title', $product->getName());
    }

    public function testChangeQuantity(): void
    {
        $this->login('asterix@yopmail.com');
        $product = $this->productRepository->findOneBy(['reference' => 'SMO']);

        $this->client->request(Request::METHOD_GET, '/shop/basket/'.$product->getId().'/add');

        $this->client->followRedirect();

        $this->client->submitForm('change-quantity-1', [
            'quantity' => 2,
        ]);

        $this->assertResponseRedirects();

        $this->client->followRedirect();

        $this->assertSelectorTextContains('.product-title', $product->getName());
    }

    public function testDelete(): void
    {
        $this->login('asterix@yopmail.com');
        $product = $this->productRepository->findOneBy(['reference' => 'SMO']);

        $this->client->request(Request::METHOD_GET, '/shop/basket/'.$product->getId().'/add');

        $this->client->followRedirect();

        $this->client->submitForm('change-quantity-1', [
            'quantity' => 0,
        ]);

        $this->assertResponseRedirects();

        $this->client->followRedirect();

        $this->assertSelectorTextContains('.basket-items', "Il n'y a aucun produit dans votre panier");
    }
}
