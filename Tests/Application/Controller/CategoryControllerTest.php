<?php

namespace Tigris\ShopBunlde\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\CategoryController;
use Tigris\ShopBundle\Repository\CategoryRepository;

#[CoversClass(CategoryController::class)]
class CategoryControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function list(): void
    {
        /** @var CategoryController */
        $controller = self::getContainer()->get(CategoryController::class);
        $request = new Request();
        $categoryRepository = $this->createMock(CategoryRepository::class);

        $response = $controller->_list($request, $categoryRepository);

        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
