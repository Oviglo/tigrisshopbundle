<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Entity\User\ShopUserInterface;
use Tigris\ShopBundle\Repository\ProductRepository;
use Tigris\ShopBundle\Repository\TransportRepository;

class PurchaseControllerTest extends AbstractLoginWebTestCase
{
    private ProductRepository $productRepository;
    private TransportRepository $transportRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productRepository = $this->container->get(ProductRepository::class);
        $this->transportRepository = $this->container->get(TransportRepository::class);
    }

    public function testPurchase(): void
    {
        $this->login('asterix@yopmail.com');
        $product = $this->productRepository->findOneBy(['reference' => 'SMO']);

        $this->client->request(Request::METHOD_GET, '/shop/basket/'.$product->getId().'/add');

        $this->client->followRedirect();

        $this->client->request(Request::METHOD_GET, '/shop/purchase/');

        $this->assertResponseIsSuccessful();

        $this->client->submitForm('Enregistrer', [
            'choose_address[address][civility]' => 'mr',
            'choose_address[address][name]' => 'Peuplu',
            'choose_address[address][firstname]' => 'Jean',
            'choose_address[address][phone]' => '0600000000',
            'choose_address[address][country]' => 'FR',
            'choose_address[address][postal]' => '88210',
            'choose_address[address][city]' => 'Vieux-Moulin',
            'choose_address[address][line1]' => '17 rue du Senneçon',
        ]);

        $this->assertResponseRedirects();

        $this->client->followRedirect();

        $transport = $this->transportRepository->findOneBy(['name' => 'Mondial Relay']);

        $this->assertSelectorTextContains('.transport-name', $transport->getName());

        $this->client->submitForm('form_actions_save', [
            'form[transport]' => $transport->getId(),
        ]);

        $this->assertResponseRedirects();

        $this->client->followRedirect();

        $this->assertSelectorTextContains('label', 'LE RELAX');
    }

    #[Test]
    public function canceled(): void
    {
        $this->loginUser();

        $this->client->request(Request::METHOD_GET, '/shop/purchase/canceled');
        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function finished(): void
    {
        $this->loginUser();

        $this->client->request(Request::METHOD_GET, '/shop/purchase/finished');
        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function summaryWithoutBasket(): void
    {
        $this->loginUser();

        $this->client->request(Request::METHOD_GET, '/shop/purchase/summary');

        self::assertResponseRedirects('/shop/basket/');
    }

    #[Test]
    public function summaryWithBasket(): void
    {
        /** @var ShopUserInterface&User */
        $user = $this->loginUser();
        $products = self::getContainer()->get(ProductRepository::class)->findBy(['public' => true]);
        $basket = (new Basket())
            ->addProduct($products[0], 1)
            ->addProduct($products[1], 2)
            ->setOrder($user->getOrders()->first())
        ;

        $basket->setUser($user);

        $this->em->persist($basket);
        $this->em->flush();

        $this->client->request(Request::METHOD_GET, '/shop/purchase/summary');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function payAtShop(): void
    {
        /** @var ShopUserInterface&User */
        $user = $this->loginUser();
        /** @var Order */
        $order = $user->getOrders()->first();

        $transport = (new Transport())
            ->setName('Test')
            ->setType(Transport::TYPE_COLLECT)
        ;

        $order->setTransport($transport);

        $products = self::getContainer()->get(ProductRepository::class)->findBy(['public' => true]);
        $basket = (new Basket())
            ->addProduct($products[0], 1)
            ->addProduct($products[1], 2)
            ->setOrder($order)
        ;

        $basket->setUser($user);

        $this->em->persist($basket);
        $this->em->flush();

        $this->client->request(Request::METHOD_GET, '/shop/purchase/paid-at-shop');
        self::assertResponseIsSuccessful();

        $this->client->submitForm('form_actions_save');

        self::assertResponseRedirects();
    }
}
