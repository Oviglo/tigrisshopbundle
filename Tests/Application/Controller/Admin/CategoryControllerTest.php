<?php

namespace Tigris\ShopBunlde\Tests\Application\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\Admin\CategoryController;
use Tigris\ShopBundle\Repository\CategoryRepository;

#[CoversClass(CategoryController::class)]
class CategoryControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/category/');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function data(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/category/data');

        $this->assertResponseIsSuccessful();
    }

    public function testAdd(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/category/new');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('category_actions_save')->form();

        $this->client->submit($form, [
            'category[name]' => 'Catégorie test',
            'category[description]' => 'Ceci est une catégorie de test',
        ]);

        $this->assertResponseRedirects();

        $oldCategory = $this->container->get(CategoryRepository::class)->findOneBy(['slug' => 'categorie-test']);

        $this->assertNotNull($oldCategory);
    }

    public function testEdit(): void
    {
        $this->loginAdmin();

        $category = $this->container->get(CategoryRepository::class)->findOneBy(['slug' => 'livre']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/category/'.$category->getId().'/edit');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('category_actions_save')->form();

        $this->client->submit($form, [
            'category[name]' => 'Livres',
            'category[description]' => 'Plein de livres',
        ]);

        $this->assertResponseRedirects();

        $oldCategory = $this->container->get(CategoryRepository::class)->findOneBy(['slug' => 'livre']);

        $this->assertNull($oldCategory);
    }

    public function testDelete(): void
    {
        $this->loginAdmin();

        $category = $this->container->get(CategoryRepository::class)->findOneBy(['slug' => 'jeux-video']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/category/'.$category->getId().'/remove');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();

        $this->client->submit($form);

        $this->assertResponseRedirects();

        $oldCategory = $this->container->get(CategoryRepository::class)->findOneBy(['slug' => 'jeux-video']);

        $this->assertNull($oldCategory);
    }

    #[Test]
    public function sort(): void
    {
        $this->loginAdmin();
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/category/sort');
        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();
        $this->client->submit($form);

        $this->assertResponseRedirects();
    }
}
