<?php

namespace Tigris\ShopBundle\Tests\Application\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\Admin\TaxController;
use Tigris\ShopBundle\Repository\TaxRepository;

#[CoversClass(TaxController::class)]
class TaxControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/tax/');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function anonymousAccess(): void
    {
        $this->client->request(Request::METHOD_GET, '/admin/shop/tax/');

        $this->assertResponseStatusCodeSame(302);
    }

    #[Test]
    public function data(): void
    {
        $this->loginAdmin();
        $this->client->request(Request::METHOD_GET, '/admin/shop/tax/data');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function new(): void
    {
        $this->loginAdmin();
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/tax/new');
        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('tax_actions_save')->form();

        $this->client->submit($form, [
            'tax[name]' => 'Tax test',
            'tax[value]' => 33.33,
        ]);

        self::assertResponseRedirects();

        $newCategory = $this->container->get(TaxRepository::class)->findOneBy(['name' => 'Tax test']);

        self::assertNotNull($newCategory);
    }

    #[Test]
    public function edit(): void
    {
        $this->loginAdmin();
        $tax = $this->container->get(TaxRepository::class)->findOneBy(['name' => 'TVA 20']);
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/tax/'.$tax->getId().'/edit');
        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('tax_actions_save')->form();

        $this->client->submit($form, [
            'tax[name]' => 'TVA 6',
            'tax[value]' => 6.00,
        ]);

        self::assertResponseRedirects();

        $oldCategory = $this->container->get(TaxRepository::class)->findOneBy(['name' => 'TVA 6']);

        self::assertNotNull($oldCategory);
    }

    #[Test]
    public function remove(): void
    {
        $this->loginAdmin();
        $tax = $this->container->get(TaxRepository::class)->findOneBy(['name' => 'TVA 20']);
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/tax/'.$tax->getId().'/remove');
        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();

        $this->client->submit($form, []);

        self::assertResponseRedirects();

        $deletedCategory = $this->container->get(TaxRepository::class)->findOneBy(['name' => 'TVA 20']);

        self::assertNull($deletedCategory);
    }
}
