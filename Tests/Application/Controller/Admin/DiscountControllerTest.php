<?php

namespace Tigris\ShopBundle\Tests\Application\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\Admin\DiscountController;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Repository\DiscountRepository;

#[CoversClass(DiscountController::class)]
class DiscountControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/discount/');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function data(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/discount/data');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function new(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/discount/new');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('discount_actions_save')->form();

        $this->client->submit($form, [
            'discount[name]' => 'Discount test',
            'discount[code]' => 'DISCOUNT_TEST',
            'discount[itemType]' => Discount::ITEM_TYPE_ORDER,
            'discount[type]' => Discount::DISCOUNT_TYPE_VALUE,
            'discount[value]' => 5,
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(DiscountRepository::class)->findOneBy(['name' => 'Discount test']);

        self::assertNotNull($entity);
    }

    #[Test]
    public function edit(): void
    {
        $this->loginAdmin();

        $entity = self::getContainer()->get(DiscountRepository::class)->findOneBy(['name' => 'Super Discount']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/discount/'.$entity->getId().'/edit');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('discount_actions_save')->form();

        $this->client->submit($form, [
            'discount[name]' => 'Super Discount Test',
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(DiscountRepository::class)->findOneBy(['name' => 'Super Discount Test']);

        self::assertNotNull($entity);
    }

    #[Test]
    public function remove(): void
    {
        $this->loginAdmin();

        $entity = self::getContainer()->get(DiscountRepository::class)->findOneBy(['name' => 'Super Discount']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/discount/'.$entity->getId().'/remove');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();

        $this->client->submit($form, []);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(DiscountRepository::class)->findOneBy(['name' => 'Super Discount']);

        self::assertNull($entity);
    }

    #[Test]
    public function printCode(): void
    {
        $this->loginAdmin();

        $entity = self::getContainer()->get(DiscountRepository::class)->findOneBy(['name' => 'Happy Code']);

        $this->client->request(Request::METHOD_GET, '/admin/shop/discount/'.$entity->getId().'/print-code');

        $this->assertResponseIsSuccessful();
    }
}
