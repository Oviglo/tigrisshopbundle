<?php

namespace Tigris\ShopBundle\Tests\Application\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\Admin\ProductController;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Repository\CategoryRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

#[CoversClass(ProductController::class)]
class ProductControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function testIndex(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/product/');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function testAnonymousAccess(): void
    {
        $this->client->request(Request::METHOD_GET, '/admin/shop/product/');

        $this->assertResponseStatusCodeSame(302);
    }

    #[Test]
    public function testData(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/product/data');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function testNew(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/product/new');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('product_actions_save')->form();
        $this->client->submit($form, [
            'product[name]' => 'Product test',
            'product[public]' => true,
            'product[type]' => Product::TYPE_STANDARD,
            'product[description]' => 'Product test description',
            'product[price]' => 15.99,
            'product[quantity]' => 10,
        ]);

        self::assertResponseRedirects();

        $newProduct = $this->container->get(ProductRepository::class)->findOneBy(['name' => 'Product test']);

        self::assertNotNull($newProduct);
    }

    #[Test]
    public function testEdit(): void
    {
        $this->loginAdmin();

        $product = $this->container->get(ProductRepository::class)->findOneBy(['reference' => 'SMO']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/product/'.$product->getId().'/edit');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('product_actions_save')->form();
        $this->client->submit($form, [
            'product[name]' => 'Product edited',
        ]);

        self::assertResponseRedirects();

        $newProduct = $this->container->get(ProductRepository::class)->findOneBy(['name' => 'Product edited']);

        self::assertNotNull($newProduct);
    }

    #[Test]
    public function testRemove(): void
    {
        $this->loginAdmin();

        $product = $this->container->get(ProductRepository::class)->findOneBy(['reference' => 'SMO']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/product/'.$product->getId().'/remove');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();
        $this->client->submit($form, []);

        self::assertResponseRedirects();

        $deletedProduct = $this->container->get(ProductRepository::class)->findOneBy(['reference' => 'SMO']);

        self::assertNull($deletedProduct);
    }

    #[Test]
    public function testDuplicate(): void
    {
        $this->loginAdmin();

        $product = $this->container->get(ProductRepository::class)->findOneBy(['reference' => 'SMO']);

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/product/'.$product->getId().'/copy');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();
        $this->client->submit($form, []);

        self::assertResponseRedirects();

        $newProduct = $this->container->get(ProductRepository::class)->findOneBy(['reference' => 'SMO2']);

        self::assertNotNull($newProduct);
    }

    #[Test]
    public function testRemoveGroup(): void
    {
        $this->loginAdmin();

        /** @var ProductRepository */
        $productRepository = $this->container->get(ProductRepository::class);
        $count = $productRepository->count([]);
        $products = $productRepository->findBy([], ['id' => 'ASC'], 5);
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getId();
        }

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/product/remove-group?'.http_build_query(['ids' => $ids]));

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();
        $this->client->submit($form, []);

        self::assertResponseRedirects();

        self::assertEquals($count - 5, $productRepository->count([]));
    }

    #[Test]
    public function testChangeCategory(): void
    {
        $this->loginAdmin();

        /** @var ProductRepository */
        $productRepository = $this->container->get(ProductRepository::class);
        $products = $productRepository->findBy([], ['id' => 'ASC'], 5);
        $ids = [];
        foreach ($products as $product) {
            $ids[] = $product->getId();
        }

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/product/change-category?'.\http_build_query(['ids' => $ids]));
        $category = self::getContainer()->get(CategoryRepository::class)->findOneBy(['name' => 'Livre']);

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('product_category_actions_save')->form();
        $this->client->submit($form, [
            'product_category[categories]' => [$category->getId()],
        ]);

        self::assertResponseRedirects();

        $products = $productRepository->findBy([], ['id' => 'ASC'], 5);
        foreach ($products as $product) {
            self::assertEquals('Livre', $product->getCategories()->first()->getName());
        }
    }

    #[Test]
    public function testChangePart(): void
    {
        $this->loginAdmin();
        /** @var ProductRepository */
        $productRepository = $this->container->get(ProductRepository::class);
        $product = $productRepository->findOneBy(['reference' => 'SMO']);

        $this->client->request(Request::METHOD_PUT, '/admin/shop/product/'.$product->getId().'/change-part', [
            'data' => 'price',
            'value' => 88,
        ]);

        $this->assertResponseIsSuccessful();

        $editedProduct = $productRepository->findOneBy(['reference' => 'SMO']);

        self::assertEquals(88, $editedProduct->getPrice());
    }
}
