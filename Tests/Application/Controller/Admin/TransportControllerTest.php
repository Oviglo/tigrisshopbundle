<?php

namespace Tigris\ShopBundle\Tests\Application\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Repository\TransportRepository;

class TransportControllerTest extends AbstractLoginWebTestCase
{
    public function testIndex(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/transport/');

        $this->assertResponseIsSuccessful();
    }

    public function testAdd(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/transport/new');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('transport_actions_save')->form();

        $this->client->submit($form, [
            'transport[name]' => 'Transport de test',
            'transport[description]' => 'Description de transport',
            'transport[countries]' => ['FR'],
        ]);

        $this->assertResponseRedirects();

        $transport = $this->container->get(TransportRepository::class)->findLast();

        $this->assertEquals($transport->getName(), 'Transport de test');
    }

    public function testEdit(): void
    {
        $this->loginAdmin();

        $transport = $this->container->get(TransportRepository::class)->findLast();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/transport/'.$transport->getId().'/edit');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('transport_actions_save')->form();

        $this->client->submit($form, [
            'transport[name]' => 'Transport modifié',
        ]);

        $this->assertResponseRedirects();

        $transport = $this->container->get(TransportRepository::class)->findLast();

        $this->assertEquals($transport->getName(), 'Transport modifié');
    }

    public function testDelete(): void
    {
        $this->loginAdmin();

        $transport = $this->container->get(TransportRepository::class)->findLast();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/transport/'.$transport->getId().'/remove');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();

        $this->client->submit($form);

        $this->assertResponseRedirects();

        $removedTransport = $this->container->get(TransportRepository::class)->findOneById($transport->getId());

        $this->assertNull($removedTransport);
    }
}
