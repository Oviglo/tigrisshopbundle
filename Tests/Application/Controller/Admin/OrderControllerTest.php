<?php

namespace Tigris\ShopBundle\Tests\Application\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\Admin\OrderController;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Repository\OrderRepository;

#[CoversClass(OrderController::class)]
class OrderControllerTest extends AbstractLoginWebTestCase
{
    public function testIndexAccessDenied(): void
    {
        $this->client->request(Request::METHOD_GET, '/admin/shop/order/');

        $this->assertResponseStatusCodeSame(302);
    }

    public function testIndex(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/order/');

        $this->assertResponseIsSuccessful();
    }

    public function testData(): void
    {
        $this->loginAdmin();

        $this->client->request(Request::METHOD_GET, '/admin/shop/order/data');

        $this->assertResponseIsSuccessful();
    }

    public function testShow(): void
    {
        $this->loginAdmin();

        $orderRepository = $this->container->get(OrderRepository::class);
        $order = $orderRepository->findLast();

        $this->client->request(Request::METHOD_GET, '/admin/shop/order/'.$order->getId().'/show');

        $this->assertResponseIsSuccessful();
    }

    public function testStatuses(): void
    {
        $this->loginAdmin();

        $orderRepository = $this->container->get(OrderRepository::class);
        $order = $orderRepository->findLast();
        $this->client->request(Request::METHOD_GET, '/admin/shop/order/'.$order->getId().'/statuses');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    #[DataProvider('statusProvider')]
    public function testStatusChange(Order $order, string $status, string $actionButton, array $formData = []): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request(Request::METHOD_GET, '/admin/shop/order/'.$order->getId().'/'.$status);
        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton($actionButton)->form();
        $this->client->submit($form, $formData);

        $this->assertResponseIsSuccessful();
    }

    public static function statusProvider(): \Generator
    {
        $orderRepository = self::getContainer()->get(OrderRepository::class);
        $order = $orderRepository->findLast();

        yield [$order, 'shipp', 'shipp_actions_save', ['shipp[trackingNumber]' => '123']];
        yield [$order, 'available', 'form_actions_save', []];
        yield [$order, 'paid', 'form_actions_save', ['form[status_paid]' => true, 'form[payment_method]' => Order::METHOD_CREDIT_CARD, 'form[createInvoice]' => true]];
        yield [$order, 'cancel', 'form_actions_save', []];
        yield [$order, 'valid', 'form_actions_save', []];
    }

    #[Test]
    public function testChart(): void
    {
        $this->loginAdmin();
        $this->client->request(Request::METHOD_GET, '/admin/shop/order/chart');

        $this->assertResponseIsSuccessful();
    }
}
