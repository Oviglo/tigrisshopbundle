<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tigris\ShopBundle\Controller\ProductController;
use Tigris\ShopBundle\Repository\ProductRepository;

#[CoversClass(ProductController::class)]
class ProductControllerTest extends WebTestCase
{
    #[Test]
    public function data(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/shop/product/data');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function show(): void
    {
        $client = static::createClient();

        $product = self::getContainer()->get(ProductRepository::class)->findOneBy(['public' => true]);

        $client->request(Request::METHOD_GET, '/shop/product/'.$product->getSlug());

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function showNotFound(): void
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/shop/product/not-found');

        self::assertResponseStatusCodeSame(404);
    }
}
