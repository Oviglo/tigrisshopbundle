<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\TransportController;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\Transport;
use Tigris\ShopBundle\Entity\User\ShopUserInterface;

#[CoversClass(TransportController::class)]
class TransportControllerTest extends AbstractLoginWebTestCase
{
    public const LAPOSTE_TRACING = '861011301731382';

    #[Test]
    public function tracing(): void
    {
        /** @var ShopUserInterface */
        $user = $this->loginUser();
        /** @var Order */
        $order = $user->getOrders()[0];
        $transport = (new Transport())
            ->setName('Test')
            ->setService(Transport::SERVICE_COLISSIMO)
        ;

        $order->setTransport($transport);
        $order->setTrackingNumber(self::LAPOSTE_TRACING);

        $this->client->request(Request::METHOD_GET, 'shop/transport/tracing/'.$order->getId());

        self::assertResponseIsSuccessful();
    }
}
