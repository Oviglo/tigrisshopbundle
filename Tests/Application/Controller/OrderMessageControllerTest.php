<?php

namespace Tigris\ShopBundle\Tests\Application\Controller;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\OrderMessageController;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Entity\User\ShopUserInterface;
use Tigris\ShopBundle\Repository\OrderMessageRepository;

#[CoversClass(OrderMessageController::class)]
class OrderMessageControllerTest extends AbstractLoginWebTestCase
{
    private function getOrder(): null|Order
    {
        /** @var ShopUserInterface */
        $user = $this->loginUser();

        return \method_exists($user, 'getOrders') ? $user->getOrders()[0] : null;
    }

    private function getCsrfToken(): string
    {
        // self::getContainer()->get(RequestStack::class)->push($this->client->getRequest());
        /*$request = new Request();
          $request->setSession(self::getContainer()->get('session.factory')->createSession());
          self::getContainer()->get(RequestStack::class)->push($request);*/
        return 'bad';
    }

    #[Test]
    public function data(): void
    {
        $order = $this->getOrder();

        $this->client->request(Request::METHOD_GET, '/shop/order/message/'.$order->getId().'/data');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function new(): void
    {
        $order = $this->getOrder();
        $this->client->disableReboot();

        $token = $this->getCsrfToken();

        $this->client->xmlHttpRequest('POST', '/shop/order/message/'.$order->getId().'/new', [
            'content' => 'Message',
            '_token' => $token,
        ]);

        $response = \json_decode($this->client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();
        // $this->client->followRedirect();

        self::assertEquals('error', $response['status']);
    }

    #[Test]
    public function newBadToken(): void
    {
        $order = $this->getOrder();

        $this->client->xmlHttpRequest('POST', '/shop/order/message/'.$order->getId().'/new', [
            'content' => 'Message',
            '_token' => 'bad',
        ]);

        $response = \json_decode($this->client->getResponse()->getContent(), true);
        $this->assertResponseIsSuccessful();

        self::assertEquals('error', $response['status']);
    }

    #[Test]
    public function remove(): void
    {
        $this->loginAdmin();
        $message = self::getContainer()->get(OrderMessageRepository::class)->findOneBy([]);

        $this->client->request(Request::METHOD_GET, '/shop/order/message/'.$message->getId().'/remove');

        $this->assertResponseIsSuccessful();

        $this->client->submitForm('form_actions_save');

        $this->assertResponseIsSuccessful();
    }
}
