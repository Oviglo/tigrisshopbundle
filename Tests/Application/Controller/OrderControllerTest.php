<?php

namespace Tigris\Shopbundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\Response;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\OrderController;
use Tigris\ShopBundle\Repository\OrderRepository;

#[CoversClass(OrderController::class)]
class OrderControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginUser();

        $this->client->request(Request::METHOD_GET, '/shop/order/list');

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function show(): void
    {
        $this->loginUser();

        $user = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => self::USER_EMAIL]);
        $order = self::getContainer()->get(OrderRepository::class)->findOneBy(['user' => $user]);

        $this->client->request(Request::METHOD_GET, '/shop/order/'.$order->getId());

        $this->assertResponseIsSuccessful();
    }

    #[Test]
    public function showDenied(): void
    {
        $this->loginUser();

        $wrongUser = self::getContainer()->get(UserRepository::class)->findOneBy(['email' => 'obelix@yopmail.com']);
        $order = self::getContainer()->get(OrderRepository::class)->findOneBy(['user' => $wrongUser]);

        $this->client->request(Request::METHOD_GET, '/shop/order/'.$order->getId());

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}
