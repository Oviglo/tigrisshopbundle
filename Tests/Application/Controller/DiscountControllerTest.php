<?php

namespace tigris\ShopBundle\Tests\Application\Controller;

use Symfony\Component\HttpFoundation\Request;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\ShopBundle\Controller\DiscountController;
use Tigris\ShopBundle\Repository\DiscountRepository;

#[CoversClass(DiscountController::class)]
class DiscountControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function checkCode(): void
    {
        $this->loginUser();

        // $discount = self::getContainer()->get(DiscountRepository::class)->findOneBy(['code' => 'CODEFORASTERIX']);

        $this->client->request(Request::METHOD_GET, '/shop/discount/check-code/CODEFORASTERIX');

        self::assertResponseIsSuccessful();
    }
}
