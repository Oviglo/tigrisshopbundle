<?php

namespace Tigris\ShopBundle\Tests\Unit\Payment;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Payment\PaymentRequest;
use Tigris\ShopBundle\Tests\Helper\EntityFactory;

#[CoversClass(PaymentRequest::class)]
class PaymentRequestTest extends TestCase
{
    #[DataProvider('paymentRequestProvider')]
    public function testPaymentRequest(Order $order, float $expectedTotalPrice, float $expectedPriceWithoutTransport, float $expectedItemsPrice): void
    {
        $request = new PaymentRequest($order, '');
        static::assertEquals($expectedTotalPrice, $request->getTotalPrice());
        static::assertEquals($expectedPriceWithoutTransport, $request->getPriceWhithoutTransport());
        static::assertEquals($expectedItemsPrice, $request->getItemsPrice());
    }

    public static function paymentRequestProvider(): \Iterator
    {
        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 12.90, 2, 20));
        $transport = EntityFactory::createTransport(6.90);
        $order->setTransport($transport);

        yield [$order, 32.70, 25.80, 21.50];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 10, 2, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 18.60, 1, 5.5));
        $transport = EntityFactory::createTransport(3.50);
        $order->setTransport($transport);

        yield [$order, 42.10, 38.60, 34.29];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 100, 3, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 68.54, 4, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 34.99, 6, 5.5));
        $transport = EntityFactory::createTransport(10.65);
        $order->setTransport($transport);

        yield [$order, 794.75, 784.1, 677.49];
    }
}
