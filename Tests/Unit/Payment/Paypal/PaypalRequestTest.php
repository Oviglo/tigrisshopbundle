<?php

namespace Tigris\ShopBundle\Tests\Unit\Payment\Paypal;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Entity\Discount;
use Tigris\ShopBundle\Payment\PaymentRequest;
use Tigris\ShopBundle\Payment\Paypal\PaypalRequest;
use Tigris\ShopBundle\Tests\Helper\EntityFactory;

#[CoversClass(PaypalRequest::class)]
class PaypalRequestTest extends TestCase
{
    #[DataProvider('paypalRequestProvider')]
    public function testPaypalRequest(PaymentRequest $request, float $expectedTotalPrice, float $expectedTotalTax): void
    {
        $paypalRequest = new PaypalRequest($request);
        $nativeRequest = $paypalRequest->getRequest();

        $totalPrice = $nativeRequest['purchase_units'][0]['amount']['value'];
        $totalTax = $nativeRequest['purchase_units'][0]['amount']['breakdown']['tax_total']['value'];

        $shippingPrice = 0;
        if (isset($nativeRequest['purchase_units'][0]['amount']['breakdown']['shipping'])) {
            $shippingPrice = $nativeRequest['purchase_units'][0]['amount']['breakdown']['shipping']['value'];
        }

        $discountPrice = 0;
        if (isset($nativeRequest['purchase_units'][0]['amount']['breakdown']['discount'])) {
            $discountPrice = $nativeRequest['purchase_units'][0]['amount']['breakdown']['discount']['value'];
        }
        $items = $nativeRequest['purchase_units'][0]['items'];
        static::assertEquals($expectedTotalPrice, $totalPrice);
        static::assertEquals($expectedTotalTax, $totalTax);

        $totalItemTax = 0;
        $totalItemPrice = 0;
        foreach ($items as $item) {
            $totalItemTax += round((float) $item['tax']['value'], 2) * (int) $item['quantity'];
            $totalItemPrice += round((float) $item['unit_amount']['value'], 2) * (int) $item['quantity'];
        }

        static::assertEquals(round($totalItemTax, 2), $totalTax);
        static::assertEquals(round($totalItemPrice + $totalItemTax + $shippingPrice - $discountPrice, 2), $totalPrice);
    }

    public static function paypalRequestProvider(): \Iterator
    {
        $shippingAddress = new Address();
        $shippingAddress
            ->setCity('City')
            ->setCountry('FR')
            ->setPostal('88210')
            ->setComplement('')
        ;

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 100, 3, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 68.54, 4, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 34.99, 6, 5.5));
        $transport = EntityFactory::createTransport(10.65);
        $order->setTransport($transport);
        $order->setShippingAddress($shippingAddress);
        $request = new PaymentRequest($order, '');

        yield [$request, 794.75, 106.61];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 100, 2, 20));
        $transport = EntityFactory::createTransport(6.50);
        $order->setTransport($transport);
        $order->addDiscount(EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 10));
        $order->setShippingAddress($shippingAddress);

        $request = new PaymentRequest($order, '');

        yield [$request, 186.50, 33.34];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 54.90, 3, 20));
        $transport = EntityFactory::createTransport(6.50);
        $order->setTransport($transport);
        $order->addDiscount(EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 25));
        $order->setShippingAddress($shippingAddress);

        $request = new PaymentRequest($order, '');

        yield [$request, 130.03, 27.45];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 58.90, 2, 20));
        $transport = EntityFactory::createTransport(6.50);
        $order->setTransport($transport);
        $order->addDiscount(EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 24));
        $order->setShippingAddress($shippingAddress);

        $request = new PaymentRequest($order, '');

        yield [$request, 96.03, 19.64];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 58.90, 2, 20));
        $transport = EntityFactory::createTransport(6.50);
        $order->setTransport($transport);
        $order->addDiscount(EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 5));
        $order->addDiscount(EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 5));
        $order->setShippingAddress($shippingAddress);

        $request = new PaymentRequest($order, '');

        yield [$request, 112.52, 19.64];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 28.96, 2, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 12.46, 1, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 43.32, 2, 20));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 1.23, 2, 20));
        $transport = EntityFactory::createTransport(0.55);
        $order->setTransport($transport);

        $order->setShippingAddress($shippingAddress);

        $order->addDiscount(EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 33));

        $request = new PaymentRequest($order, '');

        yield [$request, 107.40, 26.58];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 100.99, 2, 20, EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 25)));
        $order->setOrderAddress($shippingAddress);
        $request = new PaymentRequest($order, '');

        yield [$request, 151.48, 25.24];

        $order = EntityFactory::createOrder();
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 100.99, 2, 20, EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 25)));
        $order->addOrderProduct(EntityFactory::createOrderProduct($order, 50.49, 3, 20, EntityFactory::createDiscount(Discount::DISCOUNT_TYPE_PERCENT, 33)));
        $order->setOrderAddress($shippingAddress);
        $request = new PaymentRequest($order, '');

        yield [$request, 252.97, 42.16];
    }
}
