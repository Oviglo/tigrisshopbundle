<?php

namespace Tigris\ShopBundle\Tests\Unit\Security;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\BasketProduct;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Security\BasketProductVoter;

#[CoversClass(BasketProductVoter::class)]
class BasketProductVoterTest extends TestCase
{
    protected TokenInterface $token;

    protected function setUp(): void
    {
        $this->token = $this->createMock(TokenInterface::class);
    }

    public static function getTests(): \Iterator
    {
        $basketProduct = new BasketProduct();
        $basketProduct->setBasket(new Basket());
        $basketProduct->setProduct((new Product())->setQuantity(0));
        yield [$basketProduct, [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_DENIED];

        $basketProduct = new BasketProduct();
        $basketProduct->setBasket(new Basket());
        $basketProduct->setProduct((new Product())->setQuantity(5)->setEnableMaxQuantity(true)->setMaxQuantity(1));
        $basketProduct->setQuantity(2);
        yield [$basketProduct, [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_DENIED];

        yield [new Product(), [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_ABSTAIN];

        $basketProduct = new BasketProduct();
        $basketProduct->setBasket(new Basket());
        $basketProduct->setProduct((new Product())->setQuantity(5)->setEnableMaxQuantity(true)->setMaxQuantity(10));
        $basketProduct->setQuantity(0);
        yield [$basketProduct, [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_GRANTED];

        $basketProduct = new BasketProduct();
        $basketProduct->setBasket(new Basket());
        $basketProduct->setProduct((new Product())->setQuantity(5)->setEnableMaxQuantity(false)->setMaxQuantity(1));
        $basketProduct->setQuantity(2);
        yield [$basketProduct, [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_GRANTED];

        $basketProduct = new BasketProduct();
        $basketProduct->setBasket(new Basket());
        $basketProduct->setProduct((new Product())->setQuantity(0)->setEnableMaxQuantity(true)->setMaxQuantity(1));
        $basketProduct->setQuantity(2);
        yield [$basketProduct, [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_DENIED];

        $basketProduct = new BasketProduct();
        $basketProduct->setBasket(new Basket());
        $basketProduct->setProduct((new Product())->setQuantity(0)->setType(Product::TYPE_VIRTUAL));
        $basketProduct->setQuantity(1);
        yield [$basketProduct, [BasketProductVoter::ADD_PRODUCT], Voter::ACCESS_GRANTED];
    }

    #[DataProvider('getTests')]
    public function testVote(object $subject, array $attributes, int $expectedVote): void
    {
        $voter = new BasketProductVoter();
        self::assertEquals($expectedVote, $voter->vote($this->token, $subject, $attributes));
    }
}
