<?php

namespace Tigris\ShopBundle\Tests\Unit\Security\Voter;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\Security\Voter\ProductVoter;

#[CoversClass(ProductVoter::class)]
class ProductVoterTest extends TestCase
{
    protected TokenInterface $token;

    protected function setUp(): void
    {
        $this->token = $this->createMock(TokenInterface::class);
    }

    public static function getTests(): \Iterator
    {
        $product = (new Product())->setPublic(false)->setQuantity(10)->setAvailableStatus(Product::STATUS_AVAILABLE);
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_DENIED, 'ACCESS DENIED for unpublished product'];
        yield [$product, [ProductVoter::VIEW], Voter::ACCESS_DENIED, 'ACCESS DENIED pour voir un produit qui n\'est pas publié'];

        $product = (new Product())->setPublic(true)->setQuantity(0)->setAvailableStatus(Product::STATUS_AVAILABLE);
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_DENIED, 'ACCESS DENIED for out of stock product'];
        yield [$product, [ProductVoter::EDIT], Voter::ACCESS_DENIED, 'ACCESS DENIED pour modifier le produit'];

        $product = (new Product())->setPublic(true)->setQuantity(10)->setAvailableStatus(Product::STATUS_UNAVAILABLE);
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_DENIED, 'ACCESS DENIED for unavailable product'];
        yield [$product, [ProductVoter::VIEW], Voter::ACCESS_GRANTED, 'ACCESS GRANTED pour un produit avec statut indisponible'];

        $product = (new Product())->setPublic(true)->setQuantity(10)->setAvailableStatus(Product::STATUS_AVAILABLE_AT)->setAvailableDate(new \DateTime('+ 2 days'));
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_DENIED, 'ACCESS DENIED for not available yet product'];

        $product = (new Product())->setPublic(true)->setQuantity(10)->setAvailableStatus(Product::STATUS_AVAILABLE_AT)->setAvailableDate(new \DateTime('- 2 days'));
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_GRANTED, 'ACCESS GRANTED for past available date product'];
        yield [$product, [ProductVoter::VIEW], Voter::ACCESS_GRANTED, 'ACCESS GRANTED pour voir un produit qui est disponible'];
        yield [$product, [ProductVoter::EDIT], Voter::ACCESS_DENIED, 'ACCESS DENIED pour modifier le produit'];

        $product = (new Product())->setPublic(true)->setQuantity(10)->setAvailableStatus(Product::STATUS_AVAILABLE);
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_GRANTED, 'ACCESS GRANTED for available product'];

        $product = (new Product())->setPublic(true)->setQuantity(10)->setAvailableStatus(Product::STATUS_PRE_ORDER)->setPreOrderDate(new \DateTime('- 1 day'));
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_GRANTED, 'ACCESS GRANTED for available pre order product'];

        $product = (new Product())->setPublic(true)->setQuantity(10)->setAvailableStatus(Product::STATUS_PRE_ORDER)->setPreOrderDate(new \DateTime('+ 1 day'));
        yield [$product, [ProductVoter::BUY], Voter::ACCESS_DENIED, 'ACCESS DENIED before pre order date'];
    }

    #[DataProvider('getTests')]
    public function testVote(object $subject, array $attributes, int $expectedVote, string $message): void
    {
        $voter = new ProductVoter();
        self::assertEquals($expectedVote, $voter->vote($this->token, $subject, $attributes), $message);
    }
}
