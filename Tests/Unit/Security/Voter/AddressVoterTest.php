<?php

namespace Tigris\ShopBundle\Tests\Unit\Security\Voter;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Tests\Helper\ReflectionTestTrait;
use Tigris\BaseBundle\Tests\Helper\VoterTestTrait;
use Tigris\ShopBundle\Entity\Address;
use Tigris\ShopBundle\Security\Voter\AddressVoter;

#[CoversClass(AddressVoter::class)]
class AddressVoterTest extends TestCase
{
    use VoterTestTrait;
    use ReflectionTestTrait;

    protected function setUp(): void
    {
        $this->createVoterMocks();

        parent::setUp();
    }

    #[Test]
    #[DataProvider('voteProvider')]
    public function vote(object $subject, array $attributes, null|User $user, int $expectedVote): void
    {
        $voter = new AddressVoter($this->authorizationChecker);
        $this->setAuthenticatedUser($user);

        self::assertEquals($expectedVote, $voter->vote($this->token, $subject, $attributes));
    }

    public static function voteProvider(): \Generator
    {
        $address = new Address();
        $address->setUser(new User());

        yield [$address, [AddressVoter::VIEW], null, Voter::ACCESS_DENIED];
        yield [$address, [AddressVoter::EDIT], null, Voter::ACCESS_DENIED];

        $user = new User();
        self::setProperty($user, 'id', '123');

        yield [$address, [AddressVoter::VIEW], $user, Voter::ACCESS_DENIED];
        yield [$address, [AddressVoter::EDIT], $user, Voter::ACCESS_DENIED];

        $admin = new User();
        $admin->setRoles(['ROLE_ADMIN']);
        self::setProperty($admin, 'id', '888');
        yield [$address, [AddressVoter::VIEW], $admin, Voter::ACCESS_GRANTED];
        yield [$address, [AddressVoter::EDIT], $admin, Voter::ACCESS_GRANTED];

        $address = new Address();
        $user = new User();
        self::setProperty($user, 'id', '123');
        $address->setUser($user);
        yield [$address, [AddressVoter::VIEW], $user, Voter::ACCESS_GRANTED];
        yield [$address, [AddressVoter::EDIT], $user, Voter::ACCESS_GRANTED];
    }
}
