<?php

namespace Tigris\ShopBundle\Tests\Unit\Security\Voter;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Tests\Helper\ReflectionTestTrait;
use Tigris\BaseBundle\Tests\Helper\VoterTestTrait;
use Tigris\InvoiceBundle\Entity\Invoice;
use Tigris\ShopBundle\Entity\Order;
use Tigris\ShopBundle\Security\Voter\OrderVoter;

/**
 * @internal
 */
#[CoversClass(OrderVoter::class)]
class OrderVoterTest extends TestCase
{
    use VoterTestTrait;
    use ReflectionTestTrait;

    public function setUp(): void
    {
        $this->createVoterMocks();

        parent::setUp();
    }

    #[Test]
    #[DataProvider('voteProvider')]
    public function vote(Order $subject, array $attributes, ?User $user, int $expectedVote): void
    {
        $this->setAuthenticatedUser($user);

        $voter = new OrderVoter($this->authorizationChecker);
        self::assertEquals($expectedVote, $voter->vote($this->token, $subject, $attributes));
    }

    public static function voteProvider(): \Generator
    {
        $user = new User();
        self::setProperty($user, 'id', 123);
        $order = new Order();
        $order->setUser($user);

        yield [$order, [OrderVoter::VIEW], null, Voter::ACCESS_DENIED];

        yield [$order, [OrderVoter::EDIT], null, Voter::ACCESS_DENIED];

        yield [$order, [OrderVoter::CANCEL], null, Voter::ACCESS_DENIED];

        $admin = new User();
        self::setProperty($user, 'id', 888);
        $admin->setRoles(['ROLE_ADMIN']);

        yield [$order, ['OTHER'], $admin, Voter::ACCESS_ABSTAIN];

        yield [$order, [OrderVoter::VIEW], $admin, Voter::ACCESS_GRANTED];

        yield [$order, [OrderVoter::EDIT], $admin, Voter::ACCESS_GRANTED];

        yield [$order, [OrderVoter::CANCEL], $admin, Voter::ACCESS_GRANTED];

        $invoiceOrder = new Order();
        $invoiceOrder->setUser($admin);
        $invoiceOrder->addInvoice(new Invoice());

        yield [$invoiceOrder, [OrderVoter::VIEW], $admin, Voter::ACCESS_GRANTED];

        yield [$invoiceOrder, [OrderVoter::EDIT], $admin, Voter::ACCESS_GRANTED];

        yield [$invoiceOrder, [OrderVoter::CANCEL], $admin, Voter::ACCESS_GRANTED];

        $invoiceOrder = new Order();
        $invoiceOrder->setUser($user);
        $invoiceOrder->addInvoice(new Invoice());

        yield [$invoiceOrder, [OrderVoter::VIEW], $user, Voter::ACCESS_GRANTED];

        yield [$invoiceOrder, [OrderVoter::EDIT], $user, Voter::ACCESS_GRANTED];

        yield [$invoiceOrder, [OrderVoter::CANCEL], $user, Voter::ACCESS_DENIED];
    }
}
