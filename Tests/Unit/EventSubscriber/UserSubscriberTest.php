<?php

namespace Tigris\ShopBundle\Tests\Unit\EventSubscriber;

use Knp\Menu\MenuFactory;
use Knp\Menu\MenuItem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;
use Tigris\ShopBundle\EventSubscriber\UserSubscriber;

#[CoversClass(UserSubscriber::class)]
class UserSubscriberTest extends TestCase
{
    #[Test]
    public function getSubscribedEvents(): void
    {
        self::assertArrayHasKey(Events::LOAD_MY_ACCOUNT_MENU, UserSubscriber::getSubscribedEvents());
    }

    #[Test]
    public function onLoadMyAccount(): void
    {
        $subscriber = new UserSubscriber();
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber($subscriber);

        $menu = new MenuItem('root', new MenuFactory());
        $event = new MenuEvent($menu);

        $dispatcher->dispatch($event, Events::LOAD_MY_ACCOUNT_MENU);
        
        $editedMenu = $event->getMenu();
        $child = $editedMenu->getChild('shop.menu.orders');

        self::assertNotNull($child);
    }
}
