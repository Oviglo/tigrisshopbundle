<?php

namespace Tigris\ShopBundle\Tests\Unit\EventSubscriber;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tigris\BaseBundle\Event\Events;
use Tigris\ShopBundle\EventSubscriber\MenuSubscriber;

#[CoversClass(MenuSubscriber::class)]
class MenuSubscriberTest extends TestCase
{
    #[Test]
    public function getSubscribedEvents(): void
    {
        self::assertArrayHasKey(Events::LOAD_ADMIN_MENU, MenuSubscriber::getSubscribedEvents());
    }
}
