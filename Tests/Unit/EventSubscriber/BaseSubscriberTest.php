<?php

namespace Tigris\ShopBundle\Tests\Unit\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\Form;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Event\DashboardEvent;
use Tigris\BaseBundle\Event\Event;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Event\SearchEvent;
use Tigris\BaseBundle\Event\StatsEvent;
use Tigris\ShopBundle\Entity\Basket;
use Tigris\ShopBundle\Entity\Product;
use Tigris\ShopBundle\EventSubscriber\BaseSubscriber;
use Tigris\ShopBundle\Repository\BasketRepository;
use Tigris\ShopBundle\Repository\OrderRepository;
use Tigris\ShopBundle\Repository\ProductRepository;

#[CoversClass(BaseSubscriber::class)]
class BaseSubscriberTest extends TestCase
{
    protected BasketRepository&MockObject $basketRepository;
    protected Basket&MockObject $basket;
    protected TokenInterface&MockObject $token;
    protected TokenStorageInterface&MockObject $tokenStorage;
    protected ProductRepository&MockObject $productRepository;
    protected OrderRepository&MockObject $orderRepository;

    protected BaseSubscriber $subscriber;
    protected EventDispatcher $dispatcher;

    protected function setUp(): void
    {
        $this->basketRepository = $this->createMock(BasketRepository::class);
        $this->basket = $this->createMock(Basket::class);
        $this->basket->method('getProductsQuantity')->willReturn(3);
        $this->basketRepository->method('findOneByUser')->willReturn($this->basket);

        $this->tokenStorage = $this->createMock(TokenStorageInterface::class);
        $this->token = $this->createMock(TokenInterface::class);
        $this->token->method('getUser')->willreturn(new User());
        $this->tokenStorage->method('getToken')->willReturn($this->token);

        $this->productRepository = $this->createMock(ProductRepository::class);
        /** @var Product&MockObject */
        $product = $this->createMock(Product::class);
        $product->method('getName')->willReturn('Product 1');
        $product->method('getDescription')->willReturn('Product 1 description');
        $product->method('getSlug')->willReturn('product-1');
        $product->method('getSaleCount')->willReturn(120);
        $this->productRepository->method('search')->willReturn([$product]);

        $this->orderRepository = $this->createMock(OrderRepository::class);

        $this->orderRepository->method('count')->willReturn(6);
        $this->orderRepository->method('getValidTotalPrice')->willReturn(430.90);

        /** @var Paginator&MockObject */
        $paginator = $this->createMock(Paginator::class);
        $iterator = new \ArrayIterator($product);
        $paginator->method('getIterator')->willReturn($iterator);
        $this->productRepository->method('findData')->willReturn($paginator);

        $this->subscriber = new BaseSubscriber($this->basketRepository, $this->tokenStorage, $this->productRepository, $this->orderRepository);

        $this->dispatcher = new EventDispatcher();

        $this->dispatcher->addSubscriber($this->subscriber);

        parent::setUp();
    }

    #[Test]
    public function getSubscribedEvents(): void
    {
        self::assertArrayHasKey(Events::AJAX_CRON, BaseSubscriber::getSubscribedEvents());
        self::assertArrayHasKey(Events::LOAD_STATS, BaseSubscriber::getSubscribedEvents());
        self::assertArrayHasKey(Events::LOAD_CONFIGS, BaseSubscriber::getSubscribedEvents());
        self::assertArrayHasKey(Events::SEARCH, BaseSubscriber::getSubscribedEvents());
    }

    #[Test]
    public function onAjaxCron(): void
    {
        $event = new Event();
        $this->dispatcher->dispatch($event, Events::AJAX_CRON);

        self::assertEquals(3, $event->getArgument('shopBasketCounter'));
    }

    #[Test]
    public function onLoadStats(): void
    {
        $event = new StatsEvent();
        $this->dispatcher->dispatch($event, Events::LOAD_STATS);

        self::assertCount(1, $event->getStats());
    }

    #[Test]
    public function onLoadConfig(): void
    {
        $form = $this->createMock(Form::class);
        $event = new FormEvent($form, []);

        $this->dispatcher->dispatch($event, Events::LOAD_CONFIGS);

        self::assertNotNull($event->getForm()->get('TigrisShopBundle'));
    }

    #[Test]
    public function onSearch(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $router = $this->createMock(RouterInterface::class);
        $event = new SearchEvent('product', ['shop-product'], $em, $router);

        $this->dispatcher->dispatch($event, Events::SEARCH);

        self::assertCount(1, $event->getResults());

        $results = $event->getResults();
        self::assertEquals('Product 1', $results[0]['title']);
    }
}
